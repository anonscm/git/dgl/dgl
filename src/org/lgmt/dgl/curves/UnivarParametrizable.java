/*
** UnivarParametrizable.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.curves;

/**
 * Une interface commune pour les objets unidimensionnels dont la
 * paramétrisation est facultative.
 * 
 * <p>
 * Certains objets tels que les lignes droites, les cercles et d'autres formes
 * géométriques unidimmensionnelles peuvent être considérés comme de simples
 * objets définis par leur caractéristiques géométriques (points, distances,
 * etc) ou comme des objets définis paramétriquement. Cette interface permet
 * d'associer une paramétrisation à de tels objets. Si ces objets sont modifiés,
 * la paramétrisation associée doit également être modifiée ; d'où la nécessité
 * de maintenir cette paramétrisation synchronisée avec les caractéristiques
 * géométriques de l'objet. Selon les objets, la gestion de cette
 * synchronisation peut être automatique ou laissée à la discrétion de
 * l'utilisateur. La méthode {@link #isParamSync()} doit permettre de vérifier si
 * la paramétrisation courante d'un objet est synchronisée avec sa définition
 * géométrique.
 * 
 * @author redonnet
 * 
 */
public interface UnivarParametrizable {
	/**
	 * Définit la paramétrisation de l'objet
	 *
	 */
	public void syncParam();

	/**
	 * 
	 * @return true si la paramétrisation courante de l'objet est synchronisée
	 *         avec sa définition géométrique, false dans le cas contraire
	 */
	public boolean isParamSync();
}
