/*
** SurfaceIsoCurve.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.curves;

import org.lgmt.dgl.functions.UnivarEvaluable;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.Matrix4d;

/**
 * Une courbe isoparamétrique.
 * 
 * <p>
 * ATTENTION : le paramètre de la courbe est toujours appelé u, même si c'est le
 * paramètre u de la surface qui est constant (et donc le v qui varie)
 * 
 * @navassoc - - - Surface
 * @navassoc - - - SurfaceParam
 * 
 * @author redonnet
 * 
 */
public class SurfaceIsoCurve extends Curve {
	private Surface surface;
	private Surface.Param param_name;
	/** nom du paramètre constant */
	private double isoValue;

	/** valeur du paramètre constant */

	/**
	 * Définit une nouvelle courbe isoparamétrique à partir de la surface support,
	 * et du nom et de la valeur du paramètre constant.
	 * 
	 * <p>
	 * Les limites paramétrique de la courbes sont fixées aux valeurs limites du
	 * paramètre varaible de la surface.
	 * 
	 * @param surf  la surface support de la courbe isoparamétrique
	 * @param param le nom du paramètre constant (Surface.PARAM.U ou
	 *              Surface.PARAM.V)
	 * @param value la valeur du paramètre constant
	 */
	public SurfaceIsoCurve(Surface surf, Surface.Param param, double value) {
		this.surface = surf;
		this.param_name = param;
		this.isoValue = value;

		if (param == Surface.Param.U) {
			this.umin = surface.getVmin();
			this.umax = surface.getVmax();
			createIsoU();
		}

		if (param == Surface.Param.V) {
			this.umin = surface.getUmin();
			this.umax = surface.getUmax();
			createIsoV();
		}
	}

	private void createIsoU() {
		this.v3f = functionFactory.newVec3Function(new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				return surface.evalX(isoValue, u);
			}
		}, new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				return surface.evalY(isoValue, u);
			}
		}, new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				return surface.evalZ(isoValue, u);
			}
		});
	}

	private void createIsoV() {
		this.v3f = functionFactory.newVec3Function(new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				return surface.evalX(u, isoValue);
			}
		}, new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				return surface.evalY(u, isoValue);
			}
		}, new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				return surface.evalZ(u, isoValue);
			}
		});
	}

	public Surface getSurface() {
		return surface;
	}

	public Surface.Param getParam_name() {
		return param_name;
	}

	public double getValue() {
		return isoValue;
	}

	/**
	 * Déplace la courbe par une matrice de translation/rotation
	 *
	 * <p>
	 * En pratique cette méthode déplace la surface supportant la courbe et garde
	 * les paramètres définissant l'isoparamétrique inchangés.
	 * 
	 * @param m la matrice de transformation
	 */
	@Override
	public void transform(Matrix4d m) {
		this.surface.transform(m);
	}

}
