package org.lgmt.dgl.curves;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.lgmt.dgl.commons.Interval;
import org.lgmt.dgl.commons.ParameterOutOfBoundsException;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.UnivarEvaluable;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;

/**
 * A BSpline curve.
 *
 * Most commonly used B-Splines curves. End points of the knots sequence must
 * have a degree+1 multiplicity. Internally is represented as a set of Bezier
 * curves to improve calculations time.
 * 
 * @navassoc - bezMap * BezierCurve
 * 
 * @author redonnet
 *
 */
public class BSplineCurve extends Curve {
	private int npu;
	private int dgu;
	private int nku;
	private Point3d[] points;
	private double[] knotsu;
	private LinkedHashMap<Interval, BezierCurve> bezMap;

	/**
	 * Builds a new BSpline curve using a set of points and a knots sequence. The
	 * first and the last values of the knot sequence must be repeated (degree + 1)
	 * times. This ensures the curve starts at the first control point and end at
	 * the last one.
	 * 
	 * @param points : the control points
	 * @param knots  : the knot sequence
	 */
	public BSplineCurve(final Point3d[] points, double[] knots) {
		this.type = C_TYPE.C_BSPLINE;
		this.points = points;
		this.knotsu = knots;
		this.npu = Array.getLength(points);
		this.nku = Array.getLength(knots);
		this.dgu = nku - npu - 1;
		this.umin = knots[0];
		this.umax = knots[nku - 1];

		this.checkMultiplicity();

		this.buildBezMap();

		setV3f();
	}

	/**
	 * Builds a new BSpline curve using a set of points and a knots sequence. The
	 * first and the last values of the knot sequence must be repeated (degree + 1)
	 * times. This ensures the curve starts at the first control point and end at
	 * the last one.
	 * 
	 * @param points : the control points
	 * @param knots  : the knot sequence
	 */
	public BSplineCurve(ArrayList<Point3d> points, double[] knots) {
		this(points.toArray(new Point3d[points.size()]), knots);
	}

	/**
	 * Construit une nouvelle courbe de B-Spline, clone de la courbe passée en
	 * paramètre.
	 * 
	 * NOTE : Deep copy
	 * 
	 * @param curve
	 */
	public BSplineCurve(BSplineCurve curve) {
		this.type = C_TYPE.C_BSPLINE;
		points = new Point3d[curve.npu];
		knotsu = new double[curve.nku];
		Arrays.setAll(points, i -> new Point3d(curve.points[i]));
		Arrays.setAll(knotsu, i -> curve.knotsu[i]);
		this.npu = Array.getLength(points);
		this.nku = Array.getLength(knotsu);
		this.dgu = nku - npu - 1;
		this.umin = knotsu[0];
		this.umax = knotsu[nku - 1];

		this.checkMultiplicity();

		this.buildBezMap();

		setV3f();
	}

	private void setV3f() {
		this.v3f = functionFactory.newVec3Function(new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				double val = 0.0;
				for (Interval i : bezMap.keySet()) {
					if (i.includes(u)) {
						val = bezMap.get(i).getV3f().getFx()
								.eval((u - i.getLeftValue()) / (i.getRightValue() - i.getLeftValue()));
						break;
					}
				}
				return val;
			}
		}, new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				double val = 0.0;
				for (Interval i : bezMap.keySet()) {
					if (i.includes(u)) {
						val = bezMap.get(i).getV3f().getFy()
								.eval((u - i.getLeftValue()) / (i.getRightValue() - i.getLeftValue()));
						break;
					}
				}
				return val;
			}
		}, new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				double val = 0.0;
				for (Interval i : bezMap.keySet()) {
					if (i.includes(u)) {
						val = bezMap.get(i).getV3f().getFz()
								.eval((u - i.getLeftValue()) / (i.getRightValue() - i.getLeftValue()));
						break;
					}
				}
				return val;
			}
		});
	}

	/**
	 * Check if knots sequence ends multiplicity are OK.
	 * 
	 * @throws UnhandledCurveTypeException si les multiplicité sont erronées
	 */
	private void checkMultiplicity() {
		for (int i = 0; i <= dgu; i++) {
			if (Math.abs(knotsu[i] - umin) > Util.PREC12)
				throw new UnhandledCurveTypeException(this,
						new String("Multiplicity error at the begining of the nodal sequence"));
		}
		for (int i = nku - 1; i >= nku - dgu; i--) {
			if (Math.abs(knotsu[i] - umax) > Util.PREC12)
				throw new UnhandledCurveTypeException(this,
						new String("Multiplicity error at the end of the nodal sequence"));
		}
	}

	/**
	 * Builds a Bezier curves set mapped to parameter intervals.
	 * 
	 * <p>
	 * The B-Spline curve is converted to a set of Bezier curves using the following
	 * algorithm:
	 * <ol>
	 * <li>First each value of the knot sequence is duplicated until it reaches a
	 * (degree+1) multiplicity. The corresponding control points are replaced
	 * accordingly. This procedure is achieved using the Boehm knot insertion
	 * algorithm.</li>
	 * <li>Then for each pair of different consecutive knots a Bezier curve is
	 * created and stored with the corresponding interval parameter.</li>
	 * </ol>
	 * </p>
	 */
	private void buildBezMap() {
		// on initialise une seq nodale de travail
		ArrayList<Double> seq = new ArrayList<Double>();
		for (int i = 0; i < knotsu.length; i++) {
			seq.add(i, knotsu[i]);
		}

		// on initialise une liste de pts de ctrl de travail
		ArrayList<Point3d> pts = new ArrayList<Point3d>();
		for (int i = 0; i < points.length; i++) {
			pts.add(i, new Point3d(points[i]));
		}

		// Pour chaque point de la sequence nodale
		// on insere des noeuds -> multiplicité = dgu+1
		// On ne gère pas les extrémités de la séquence nodale pour
		// lesquelles on suppose qu'on a déjà une multiplicité = dgu+1 (sans
		// quoi la spline ne passe pas par les extrémités, ce qu'on ne
		// rencontre jamais)
		// TODO : Gérer cette exception au cas où...
		int k; // indice courant de la séquence nodale
		int m; // multiplicité du noeud courant
		int j;
		for (k = 0; k <= seq.size() - dgu - 1; k = k + m) {
			// on calcule la multiplicité du noeud
			m = 1;
			j = 1;
			if (k + j < seq.size())
				while (Math.abs(seq.get(k + j) - seq.get(k)) < Util.PREC12)
					if (k + j < seq.size()) {
						m++;
						j++;
						if (k + j == seq.size())
							break;
					}
			if (k == 0) {
				if (m < dgu + 1)
					System.err.println("Multiplicité du premier noeud non gérée");
				continue;
			}
			if (k == seq.size() - dgu - 1) {
				if (m < dgu + 1)
					System.err.println("Multiplicité du dernier noeud non gérée");
				continue;
			}

			while (m <= dgu) {
				// la sequence de points à insérer
				List<Point3d> q = new ArrayList<Point3d>();
				// on calcule les points à insérer
				Point3d pt = new Point3d();
				double t = seq.get(k);
				double a;
				int i = k - dgu + 1;
				while (i < k) {
					a = (t - seq.get(i)) / (seq.get(i + dgu) - seq.get(i));
					pt.interpolate(pts.get(i - 1), pts.get(i), a);
					q.add(new Point3d(pt));
					i++;
				}
				// on supprime les anciens points
				for (j = 0; j < q.size() - 1; j++)
					pts.remove(k - dgu + 1);
				// on ajoute les nouveaux points
				pts.addAll(k - dgu + 1, q);
				// on ajoute le nouveau noeud
				seq.add(k, t);

				k++;
				m++;
			}
			m = 1;
		}

		// On crée la map de BezierCurves
		// nb de courbes à créer : seq.size() / (dgu+1) - 1
		bezMap = new LinkedHashMap<Interval, BezierCurve>();
		for (k = 0; k < seq.size() / (dgu + 1) - 1; k++) {
			j = k * (dgu + 1);
			ArrayList<Point3d> kBezPts = new ArrayList<Point3d>();
			for (int i = j; i < j + dgu + 1; i++) {
				kBezPts.add(pts.get(i));
			}
			Interval it = new Interval(seq.get((k + 1) * (dgu + 1) - 1), seq.get((k + 1) * (dgu + 1)), true);
			this.bezMap.put(it, new BezierCurve(kBezPts));
		}
	}

	/**
	 * Calcule un point de la courbe.
	 * 
	 * @param point un point pour stocker le résultat
	 * @param u
	 * @throws ParameterOutOfBoundsException si le paramètre u est hors des bornes
	 */
	public void eval2(Point3d point, double u) {
		if (Math.abs(u - umin) < Util.PREC12)
			u = umin;
		if (Math.abs(u - umax) < Util.PREC12)
			u = umax;
		if (u >= umin && u <= umax) {
			Point3d pt = new Point3d();
			for (Interval i : bezMap.keySet()) {
				if (i.includes(u)) {
					pt = bezMap.get(i).eval((u - i.getLeftValue()) / (i.getRightValue() - i.getLeftValue()));
					break;
				}
			}
			point.set(pt);
		} else {
			throw new ParameterOutOfBoundsException(u);
		}
	}

	/**
	 * Calcule un point de la courbe.
	 * 
	 * @param u
	 * @return un nouveau point contenant le résultat
	 * @throws ParameterOutOfBoundsException si le paramètre u est hors des bornes
	 */
	public Point3d eval(double u) {
		Point3d p = new Point3d();
		this.eval(p, u);
		return p;
	}

	public int getNpu() {
		return npu;
	}

	public int getDgu() {
		return dgu;
	}

	public int getNku() {
		return nku;
	}

	public Point3d[] getPoints() {
		return points;
	}

	public double[] getKnotsu() {
		return knotsu;
	}

	public double getKnot(int index) {
		return knotsu[index];
	}

	/**
	 * Déplace la courbe par une matrice de translation/rotation
	 * 
	 * @param m la matrice de transformation
	 */
	@Override
	public void transform(Matrix4d m) {
		Point3d[] newPoints = new Point3d[npu];
		for (int i = 0; i < npu; i++) {
			newPoints[i] = new Point3d();
			m.transform(this.points[i], newPoints[i]);
		}
		this.points = newPoints;

		this.checkMultiplicity();

		this.buildBezMap();

		setV3f();
	}
}
