/*
 ** BezierCurve.java
 ** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
 **
 ** This file is part of the DGL software package (DGL stands for
 ** Differential Geometry Library). This software provides mathematic and numeric
 ** tools to work with various curves and surfaces (algebraic, Bézier,
 ** NURBS, ...) with an unified interface.
 **
 ** This software is governed by the CeCILL-C V2 license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 ** 
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-C license and that you accept its terms.
 */
package org.lgmt.dgl.curves;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import org.lgmt.dgl.commons.ParameterOutOfBoundsException;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.UnivarEvaluable;
import org.lgmt.dgl.functions.UnivarFunction;
import org.lgmt.dgl.optimization.Integral;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;

/**
 * Une courbe de Bézier.
 * 
 * @navassoc - hodograph - BezierCurve
 * 
 * @author redonnet
 *
 */
public class BezierCurve extends Curve {
	protected int npu;
	protected int dgu;
	protected Point3d[] points;
	protected Point3d[] hornerCoeffs;
	protected BezierCurve hodograph = null;

	/**
	 * Construit une nouvelle courbe de Bézier à partir d'une liste de points de
	 * contrôle.
	 * 
	 * @param points les points de contrôle
	 */
	public BezierCurve(Point3d[] points) {
		init(points);
	}

	/**
	 * Construit une nouvelle courbe de Bézier à partir d'une liste de points de
	 * contrôle.
	 * 
	 * @param points les points de contrôle
	 */
	public BezierCurve(List<Point3d> points) {
		init(points.toArray(new Point3d[points.size()]));
	}

	/**
	 * Construit une nouvelle courbe de Bézier, clone de la courbe passée en
	 * paramètre.
	 * 
	 * NOTE : Deep copy
	 * 
	 * @param curve
	 */
	public BezierCurve(BezierCurve curve) {
		Point3d[] points = new Point3d[curve.npu];
		Arrays.setAll(points, i -> new Point3d(curve.points[i]));

		init(points);
	}

	private void init(Point3d[] points) {
		this.type = C_TYPE.C_BEZIER;
		this.points = points;
		this.npu = Array.getLength(points);
		this.dgu = npu - 1;
		this.umin = 0.0;
		this.umax = 1.0;

		calcHornerCoeffs();

		setV3f();
	}

	/**
	 * Calcule les coefficients de Horner de la courbe.
	 *
	 */
	private void calcHornerCoeffs() {
		hornerCoeffs = new Point3d[npu];
		Point3d pt = new Point3d();

		for (int i = 0; i < npu; i++) {
			hornerCoeffs[i] = new Point3d(0, 0, 0);

			for (int k = 0; k <= i; k++) {
				pt.set(points[k]);
				pt.scale((Math.pow(-1, i - k)) * (Util.binomial(i, k)));
				hornerCoeffs[i].add(pt);
			}
			hornerCoeffs[i].scale(Util.binomial(dgu, i));
		}

	}

	private void setV3f() {
		this.v3f = functionFactory.newVec3Function(new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				double val = hornerCoeffs[dgu].x;
				for (int i = dgu - 1; i >= 0; i--) {
					val = u * val + hornerCoeffs[i].x;
				}
				return val;
			}
		}, new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				double val = hornerCoeffs[dgu].y;
				for (int i = dgu - 1; i >= 0; i--) {
					val = u * val + hornerCoeffs[i].y;
				}
				return val;
			}
		}, new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				double val = hornerCoeffs[dgu].z;
				for (int i = dgu - 1; i >= 0; i--) {
					val = u * val + hornerCoeffs[i].z;
				}
				return val;
			}
		});
	}

	/**
	 * Calcule l'hodographe de la courbe.
	 * 
	 * L'hodographe d'une courbe est une autre courbe constituée par l'ensemble des
	 * vecteurs tangents à la courbe originale ramenés sur une même origine.
	 * L'hodographe d'une courbe de Bézier est une courbe de Bézier.
	 * 
	 * NOTA : Par rapport à la méthode de calcul direct des tangentes par les
	 * dérivées, le temps nécessaire pour calculer l'hodographe est largement
	 * compensé par le temps de calcul effectif des tangentes
	 */
	private void calcHodograph() {
		int npuh = this.npu - 1;

		Point3d[] hpoints = new Point3d[npuh];
		Point3d ptmp = new Point3d();
		for (int i = 0; i < npuh; i++) {
			ptmp.scaleAdd(-1, points[i], points[i + 1]);
			ptmp.scale(npuh);
			hpoints[i] = new Point3d(ptmp);
		}
		this.hodograph = new BezierCurve(hpoints);
	}

	/**
	 * Calculates a point of the curve
	 * 
	 * @param point a Point3d to store the result
	 * @param u
	 * @throws ParameterOutOfBoundsException if parameter u is out of interval [0,1]
	 */
	@Override
	public void eval(Point3d point, double u) {
		if (Math.abs(u - umin) < Util.PREC12)
			u = umin;
		if (Math.abs(u - umax) < Util.PREC12)
			u = umax;
		if (u >= umin && u <= umax) {
			Point3d p = new Point3d(hornerCoeffs[dgu]);
			for (int i = dgu - 1; i >= 0; i--) {
				p.scaleAdd(u, hornerCoeffs[i]);
			}
			point.set(p);
		} else {
			throw new ParameterOutOfBoundsException(u);
		}
	}

	/**
	 * Calculates a point of the curve
	 * 
	 * @param u
	 * @return a new Point3d containing the result
	 */
	@Override
	public Point3d eval(double u) {
		Point3d p = new Point3d();
		this.eval(p, u);
		return p;
	}

	/**
	 * 
	 * @return le nombre de pôles de la courbe
	 */
	public int getNpu() {
		return npu;
	}

	/**
	 * 
	 * @return le degré de la courbe
	 */
	public int getDgu() {
		return dgu;
	}

	/**
	 * Retourne un point de contrôle de la courbe à partir de son index dans la
	 * liste des points de contrôles.
	 * 
	 * @param index l'index du point à retourner dans la liste des points de
	 *              contrôles
	 * @return un point de contrôle de la courbe
	 */
	public Point3d getControlPoint(int index) {
		return points[index];
	}

	/**
	 * Return an array of Point3d containing control points of this curve.
	 * 
	 * @return control points of this curve
	 */
	public Point3d[] getControlPoints() {
		return points;
	}

	public double length() {
		UnivarFunction func = new UnivarFunction(new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				return Math.sqrt(Math.pow(v3f.getFx().diff(u), 2) + Math.pow(v3f.getFy().diff(u), 2)
						+ Math.pow(v3f.getFz().diff(u), 2));
			}
		});
		Integral integral = new Integral(func, 0.0, 1.0);
		
		return integral.calc();
	}

	/**
	 * 
	 * @return la longueur du polygone de contrôle
	 */
	public double polygonLength() {
		double length = 0.;
		for (int i = 0; i < this.npu - 1; i++) {
			length = length + Math.sqrt(Math.pow(this.getControlPoint(i + 1).x - this.getControlPoint(i).x, 2)
					+ Math.pow(this.getControlPoint(i + 1).y - this.getControlPoint(i).y, 2)
					+ Math.pow(this.getControlPoint(i + 1).z - this.getControlPoint(i).z, 2));
		}
		return length;
	}

	/**
	 * Calcule la tangente à la courbe en utilisant son hodographe.
	 * 
	 * S'il n'a pas encore été défini, l'hodographe est calculé préalablement au
	 * calcul de la tangente.
	 * 
	 * @param t un vecteur pour stocker le résultat
	 * @param u
	 */
	@Override
	public void tangent(Vector3d t, double u) {
		if (this.hodograph == null)
			calcHodograph();

		t.set(hodograph.eval(u));
	}

	/**
	 * Calcule la tangente à la courbe en utilisant son hodographe.
	 * 
	 * S'il n'a pas encore été défini, l'hodographe est calculé préalablement au
	 * calcul de la tangente.
	 * 
	 * @param u
	 * @return un nouveau vecteur contenant le résultat
	 */
	@Override
	public Vector3d tangent(double u) {
		if (this.hodograph == null)
			calcHodograph();
		Vector3d t = new Vector3d();
		t.set(hodograph.eval(u));

		return t;
	}

	/**
	 * Retourne la longueur du polygone de contrôle entre deux noeuds.
	 * 
	 * @param firstKnot le premier de la séquence nodale à considérer
	 * @param lastKnot  le denier noeud de la séquence nodale à considérer
	 * @return la longueur du polygone de contrôle entre les points considérés
	 */
	public double subPolygonLength(int firstKnot, int lastKnot) {
		double length = 0;
		for (int i = firstKnot - 1; i < lastKnot - 1; i++) {
			length = length + Math.sqrt(Math.pow(this.getControlPoint(i + 1).x - this.getControlPoint(i).x, 2)
					+ Math.pow(this.getControlPoint(i + 1).y - this.getControlPoint(i).y, 2)
					+ Math.pow(this.getControlPoint(i + 1).z - this.getControlPoint(i).z, 2));
		}
		return length;
	}

	/**
	 * Pour une courbe de Bézier, les valeurs extrêmes du paramètre sont
	 * obligatoirement 0.0 et 1.0. Une tentative pour les modifier lève donc une
	 * exception.
	 */
	@Override
	public void setUmax(double umax) {
		throw (new IllegalCurveTypeException(C_TYPE.C_BEZIER));
	}

	/**
	 * Pour une courbe de Bézier, les valeurs extrêmes du paramètre sont
	 * obligatoirement 0.0 et 1.0. Une tentative pour les modifier lève donc une
	 * exception.
	 */
	@Override
	public void setUmin(double umin) {
		throw (new IllegalCurveTypeException(C_TYPE.C_BEZIER));
	}

	/**
	 * Retourne les coefficients de Horner de cette courbe.
	 * 
	 * @return les coefficients de Horner de cette courbe
	 */
	public Point3d[] getHornerCoeffs() {
		return hornerCoeffs;
	}

	/**
	 * Retourne l'hodographe de cette courbe.
	 * 
	 * @return l'hodographe de cette courbe
	 */
	public BezierCurve getHodograph() {
		return hodograph;
	}

	/**
	 * Déplace la courbe par une matrice de translation/rotation
	 * 
	 * @param m la matrice de transformation
	 */
	@Override
	public void transform(Matrix4d m) {
		Point3d[] newPoints = new Point3d[npu];
		for (int i = 0; i < npu; i++) {
			newPoints[i] = new Point3d();
			m.transform(this.points[i], newPoints[i]);
		}

		this.points = newPoints;

		calcHornerCoeffs();

		setV3f();
	}

	public BezierCurve transformClone(Matrix4d m) {
		BezierCurve clone = new BezierCurve(this);
		clone.transform(m);
		return clone;
	}

}
