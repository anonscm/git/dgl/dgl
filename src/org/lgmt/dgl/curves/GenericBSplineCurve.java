package org.lgmt.dgl.curves;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.lgmt.dgl.commons.ParameterOutOfBoundsException;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.UnivarEvaluable;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;

/**
 * A generic BSpline curve.
 *
 * Differs from BSplineCurve because accepts any knots sequence (i.e. with or
 * without degree+1 multiplicity at endpoints of knots sequence).
 * 
 * For the most commonly used B-Splines curves (i.e. with degree+1 multiplicity
 * at endpoints of knots sequence, which means that the curve starts and ends at
 * the extremes control points), you should use the BSplineCurve object,
 * calculations are significantly faster.
 * 
 * Calculation algorithms are from "The NURBS book", L. Piegl and W. Tiller,
 * Springer editor
 * 
 * @author redonnet
 *
 */
public class GenericBSplineCurve extends Curve {
	private int npu;
	private int dgu;
	private int nku;
	private Point3d[] points;
	private double[] knotsu;

	/**
	 * Builds a new generic BSpline curve using a set of points and a knots
	 * sequence.
	 * 
	 * @param points
	 *            the control points
	 * @param knots
	 *            the knot sequence
	 */
	public GenericBSplineCurve(final Point3d[] points, double[] knots) {
		this.type = C_TYPE.C_BSPLINE;
		this.points = points;
		this.knotsu = knots;
		this.npu = Array.getLength(points);
		this.nku = Array.getLength(knots);
		this.dgu = nku - npu - 1;
		this.umin = knots[0];
		this.umax = knots[nku - 1];
		
		setV3f();

	}
	
	private void setV3f() {
		this.v3f = functionFactory.newVec3Function(new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				int span = findSpan(u);
				double[] N = basisFuns(span, u);
				double val = 0.0;
				for (int i = 0; i <= dgu; i++) {
					val = val + N[i] * points[span - dgu + i].x;
				}
				return val;
			}
		}, new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				int span = findSpan(u);
				double[] N = basisFuns(span, u);
				double val = 0.0;
				for (int i = 0; i <= dgu; i++) {
					val = val + N[i] * points[span - dgu + i].y;
				}
				return val;
			}
		}, new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				int span = findSpan(u);
				double[] N = basisFuns(span, u);
				double val = 0.0;
				for (int i = 0; i <= dgu; i++) {
					val = val + N[i] * points[span - dgu + i].z;
				}
				return val;
			}
		});
	}

	/**
	 * Builds a new generic BSpline curve using a set of points and a knots
	 * sequence.
	 * 
	 * @param points
	 *            the control points
	 * @param knots
	 *            the knot sequence
	 */
	public GenericBSplineCurve(ArrayList<Point3d> points, double[] knots) {
		this(points.toArray(new Point3d[points.size()]), knots);
	}

	/**
	 * Calcule un point de la courbe.
	 * 
	 * @param point
	 *            un point pour stocker le résultat
	 * @param u
	 * @throws ParameterOutOfBoundsException
	 *             si le paramètre u est hors des bornes
	 */
	public void eval(Point3d point, double u) {
		if (Math.abs(u - umin) < Util.PREC12)
			u = umin;
		if (Math.abs(u - umax) < Util.PREC12)
			u = umax;
		if (u >= umin && u <= umax) {
			Point3d pt = new Point3d();
			Point3d tmp = new Point3d();
			int span = findSpan(u);
			double[] N = basisFuns(span, u);
			for (int i = 0; i <= dgu; i++) {
				tmp.set(points[span - dgu + i]);
				tmp.scale(N[i]);
				pt.add(tmp);
			}
			point.set(pt);
		} else {
			throw new ParameterOutOfBoundsException(u);
		}
	}

	/**
	 * Calcule un point de la courbe.
	 * 
	 * @param u
	 * @return un nouveau point contenant le résultat
	 * @throws ParameterOutOfBoundsException
	 *             si le paramètre u est hors des bornes
	 */
	public Point3d eval(double u) {
		Point3d p = new Point3d();
		this.eval(p, u);
		return p;
	}

	/**
	 * Find the span of a given u parameter.
	 * 
	 * MEMO (cf Nurbs Book p. 68) Nombre de poles : npu = n+1 Nombre de noeud :
	 * nku = m+1 Degré : dgu = p nku = npu + dgu +1 m+1 = n+1+p+1 => m = n + p
	 * +1
	 * 
	 * @param u
	 *            : parameter value
	 * 
	 * @return the span of the given u parameter
	 */
	private int findSpan(double u) {
		if (u == knotsu[npu])
			return npu - 1;
		int low = dgu;
		int high = npu;
		int mid = (int) (Math.floor((low + high) / 2));

		while (u < knotsu[mid] || u >= knotsu[mid + 1]) {
			if (u < knotsu[mid])
				high = mid;
			else
				low = mid;
			mid = (int) (Math.floor((low + high) / 2));
		}
		return mid;
	}

	/**
	 * Calculates the value of the ith basis function for th given parameter.
	 * 
	 * MEMO (cf Nurbs Book p. 70) Nombre de poles : npu = n+1 Nombre de noeud :
	 * nku = m+1 Degré : dgu = p nku = npu + dgu +1 m+1 = n+1+p+1 => m = n + p
	 * +1
	 * 
	 * @param i
	 *            : the knot span
	 * @param u
	 *            : parameter value
	 * 
	 * @return basis functions values
	 */
	private double[] basisFuns(int i, double u) {
		double N[] = new double[dgu + 1];
		double temp, saved;
		double left[] = new double[dgu + 1];
		double right[] = new double[dgu + 1];
		N[0] = 1.0;

		for (int j = 1; j <= dgu; j++) {
			left[j] = u - knotsu[i + 1 - j];
			right[j] = knotsu[i + j] - u;
			saved = 0.0;
			for (int r = 0; r < j; r++) {
				temp = N[r] / (right[r + 1] + left[j - r]);
				N[r] = saved + right[r + 1] * temp;
				saved = left[j - r] * temp;
			}
			N[j] = saved;
		}
		return N;
	}

	public int getNpu() {
		return npu;
	}

	public int getDgu() {
		return dgu;
	}

	public int getNku() {
		return nku;
	}

	public Point3d[] getPoints() {
		return points;
	}

	public double[] getKnotsu() {
		return knotsu;
	}

	public double getKnot(int index) {
		return knotsu[index];
	}

	/**
	 * Déplace la courbe par une matrice de translation/rotation
	 * 
	 * @param m la matrice de transformation
	 */
	@Override
	public void transform(Matrix4d m) {
		Point3d[] newPoints = new Point3d[npu];
		for (int i = 0; i < npu; i++) {
			newPoints[i] = new Point3d();
			m.transform(this.points[i], newPoints[i]);
		}
		this.points = newPoints;

		setV3f();
	}
	
}
