/*
** PolynomialCurve.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.curves;

import java.lang.reflect.Array;
import java.util.ArrayList;

import org.lgmt.dgl.functions.UnivarEvaluable;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;

/**
 * Une courbe définie paramétriquement par trois fonctions polynomiales.
 * 
 * @author redonnet
 * 
 */
public class PolynomialCurve extends Curve {
	private ArrayList<Point3d> coeffs;
	private int dgX;
	private int dgY;
	private int dgZ;

	/**
	 * Construit une nouvelle courbe polynomiale.
	 * 
	 * @param coeffsX
	 *            un tableau contenant les valeurs des coefficients du polynôme
	 *            en X en allant du degré 0 au degré le plus élevé
	 * @param coeffsY
	 *            un tableau contenant les valeurs des coefficients du polynôme
	 *            en Y en allant du degré 0 au degré le plus élevé
	 * @param coeffsZ
	 *            un tableau contenant les valeurs des coefficients du polynôme
	 *            en Z en allant du degré 0 au degré le plus élevé
	 * @param umin
	 *            la valeur mini pour le paramètre u
	 * @param umax
	 *            la valeur maxi pour le paramètre u
	 */
	public PolynomialCurve(double[] coeffsX, double[] coeffsY, double[] coeffsZ,
			double umin, double umax){
		this.type = C_TYPE.C_POLY;
		this.umin = umin;
		this.umax = umax;
		this.coeffs = new ArrayList<Point3d>();

		this.dgX = Array.getLength(coeffsX)-1;
		this.dgY = Array.getLength(coeffsY)-1;
		this.dgZ = Array.getLength(coeffsZ)-1;
		int i;
		for (i=0 ; i<=Math.max(dgX, Math.max(dgY, dgZ)) ; i++){
			this.coeffs.add(new Point3d(0.0, 0.0, 0.0));
		}
		for(i=0 ; i<=dgX ; i++){
			this.coeffs.get(i).x = coeffsX[i];
		}
		for(i=0 ; i<=dgY ; i++){
			this.coeffs.get(i).y = coeffsY[i];
		}
		for(i=0 ; i<=dgZ ; i++){
			this.coeffs.get(i).z = coeffsZ[i];
		}
		
		this.v3f = functionFactory.newVec3Function(
				new UnivarEvaluable(){
					@Override
					public double eval(double u){
						double val = coeffs.get(dgX).x;
						for(int i=dgX-1 ; i >=0 ; i-- ){
							val = u*val+coeffs.get(i).x;
						}
						return val;
					}
				},
				new UnivarEvaluable(){
					@Override
					public double eval(double u){
						double val = coeffs.get(dgY).y;
						for(int i=dgY-1 ; i >=0 ; i-- ){
							val = u*val+coeffs.get(i).y;
						}
						return val;
					}
				},
				new UnivarEvaluable(){
					@Override
					public double eval(double u){
						double val = coeffs.get(dgZ).z;
						for(int i=dgZ-1 ; i >=0 ; i-- ){
							val = u*val+coeffs.get(i).z;
						}
						return val;
					}
				}
		);

	}

	/**
	 * Retourne le coefficient du polynôme en X déterminé par index;
	 * 
	 * @param index
	 * @return le coefficient du polynôme en X
	 */
	public double coeffX(int index){
		return coeffs.get(index).x;
	}

	/**
	 * Retourne le coefficient du polynôme en Y déterminé par index;
	 * 
	 * @param index
	 * @return le coefficient du polynôme en Y
	 */
	public double coeffY(int index){
		return coeffs.get(index).y;
	}

	/**
	 * Retourne le coefficient du polynôme en Z déterminé par index;
	 * 
	 * @param index
	 * @return le coefficient du polynôme en Z
	 */
	public double coeffZ(int index){
		return coeffs.get(index).z;
	}

	/**
	 * Retourne le degré du polynôme en X
	 * 
	 * @return le degré du polynôme en X
	 */
	public int getDgX() {
		return dgX;
	}

	/**
	 * Retourne le degré du polynôme en Y
	 * 
	 * @return le degré du polynôme en Y
	 */
	public int getDgY() {
		return dgY;
	}

	/**
	 * Retourne le degré du polynôme en Z
	 * 
	 * @return le degré du polynôme en Z
	 */
	public int getDgZ() {
		return dgZ;
	}

	
	/**
	 * Retourne la longueur totale de la courbe polynomiale
	 * TODO 
	 * @return la longueur totale de la courbe polynomiale
	 */
	public double length(){
		System.out.println("A faire ...");
		return 0.;
	}
	
	/**
	 * Retourne la longueur de la courbe polynomiale entre umin et umax
	 * TODO
	 * @param umin
	 * @param umax
	 * @return la longueur de la courbe polynomiale entre umin et umax
	 */
	public double length(double umin,double umax){
		System.out.println("A faire ...");
		return 0.;
	}

	@Override
	public void transform(Matrix4d m) {
		System.err.println("Pas encore implémenté !");
	}
}
