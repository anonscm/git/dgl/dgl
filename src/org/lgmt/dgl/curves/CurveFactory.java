/*
** CurveFactory.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.curves;

import java.util.ArrayList;

import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.UnivarVec3Function;
import org.lgmt.dgl.vecmath.GMatrix;
import org.lgmt.dgl.vecmath.Point3d;


/**
 * Une fabrique de courbes.
 * 
 * @stereotype singleton
 * 
 * @author redonnet
 *
 */
public class CurveFactory {
	
	private static CurveFactory instance = new CurveFactory();
	
	private CurveFactory () {}

	public static CurveFactory getInstance() { return instance; }

	
	/**
	 * Construit une nouvelle courbe à partir d'une fonction vectorielle d'une
	 * variable et des bornes de cette variable.
	 * 
	 * @param v3f la fonction vectorielle à utiliser
	 * @param umin la valeur mini du paramètre u
	 * @param umax la valeur maxi du paramètre u 
	 * @return la nouvelle courbe
	 */
	public AlgebraicCurve newAlgebricCurve(UnivarVec3Function v3f, double umin, double umax){
		return (new AlgebraicCurve(v3f, umin, umax));
	}

	/**
	 * Construit une nouvelle courbe de Bézier à partir d'une liste de points.
	 * 
	 * <p>
	 * Selon la valeur du paramètre interpolate, la liste de points fournie
	 * constitue le polygone caractéristique de la courbe de Bézier ou les
	 * points à interpoler par une courbe de Bézier. Pour une interpolation, la
	 * liste des points doit être comprise entre 3 (courbe de degré 2) et 11
	 * (courbe de degré 10)
	 * 
	 * @param points
	 *            la liste de points à utiliser
	 * @param interpolate
	 *            true pour une courbe interpolant la liste des points, false
	 *            pour une courbe dont le polynôme caractéristique est constitué
	 *            par la liste des points
	 * @return la nouvelle courbe de Bézier
	 */
	public BezierCurve newBezierCurve(ArrayList<Point3d> points, boolean interpolate){
		if (interpolate == false)
			return (new BezierCurve(points.toArray(new Point3d[points.size()])));
		if (points.size()<3 || points.size() > 11)
			throw (new IndexOutOfBoundsException());
		
		double length = 0;
		int size = points.size();
		double[] lengths = new double[size-1];
		double l;
		for (int i=0 ; i<size-1 ; i++){
			l = points.get(i).distance(points.get(i+1));
			length = length+l;
			lengths[i] = length;
		}
		double[] params = new double[points.size()];
		params[0] = 0.0;
		for (int i=1 ; i<size-1 ; i++){
			params[i] = lengths[i-1] / length;
		}
		params[size-1] = 1.0;
		
//		for (int i=0 ; i<size ; i++){
//			System.out.println("params[] : " + params[i]);
//		}
		
		GMatrix bim = new GMatrix(size, size);
		for(int i=0 ; i<size ; i++)
			for(int j=0 ; j<size ; j++){
				bim.setElement(j, i, Util.bernstein(size-1, i, params[j]));
			}
		GMatrix bim1 = new GMatrix(bim);
		bim1.invert();

		GMatrix x = new GMatrix(size,1);
		GMatrix y = new GMatrix(size,1);
		GMatrix z = new GMatrix(size,1);
		for (int i = 0; i < size; i++) {
			x.setElement(i, 0, points.get(i).x);
			y.setElement(i, 0, points.get(i).y);
			z.setElement(i, 0, points.get(i).z);
		}

		GMatrix sx = new GMatrix(size, 1);
		GMatrix sy = new GMatrix(size, 1);
		GMatrix sz = new GMatrix(size, 1);
		
		sx.mul(bim1, x);
		sy.mul(bim1, y);
		sz.mul(bim1, z);
		
		Point3d[] ctrlpoints = new Point3d[size];
		for (int i=0 ; i< size ; i++){
			ctrlpoints[i] = new Point3d(sx.getElement(i, 0),
										sy.getElement(i, 0),
										sz.getElement(i, 0));
		}
		
		return new BezierCurve(ctrlpoints);
	}
	
	/**
	 * Construit une nouvelle courbe à partir d'une liste de points.
	 * 
	 * <p>
	 * A partir d'une liste de points, on peut construire soit une courbe de
	 * Bézier (dont le polygone caractéristique est alors constitué par les
	 * points de la liste), soit une courbe directement constituée des points de
	 * la liste. Les valeurs possibles pour le type de courbe sont donc
	 * {@link org.lgmt.dgl.curves.C_TYPE#C_BEZIER} et
	 * {@link org.lgmt.dgl.curves.C_TYPE#C_POINTS}.
	 * 
	 * @param points la liste de points à utiliser
	 * @param type le type de courbe voulu
	 * @return la nouvelle courbe
	 * @throws IllegalCurveTypeException
	 *             si on passe un type de courbe différent de
	 *             {@link org.lgmt.dgl.curves.C_TYPE#C_BEZIER} ou
	 *             {@link org.lgmt.dgl.curves.C_TYPE#C_POINTS}
	 */
	public Curve newCurve(ArrayList<Point3d> points, C_TYPE type){
		switch (type){
		case C_BEZIER :
			return (new BezierCurve(points.toArray(new Point3d[points.size()])));
		case C_POINTS :
			return (new ParamPointsCurve<Point3d>(points));
		default :
			throw (new IllegalCurveTypeException(type));
		}
	}
}
