/*
 ** Curve.java
 ** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
 **
 ** This file is part of the DGL software package (DGL stands for
 ** Differential Geometry Library). This software provides mathematic and numeric
 ** tools to work with various curves and surfaces (algebraic, Bézier,
 ** NURBS, ...) with an unified interface.
 **
 ** This software is governed by the CeCILL-C V2 license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 ** 
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-C license and that you accept its terms.
 */
package org.lgmt.dgl.curves;

import org.lgmt.dgl.commons.Transformable;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.UnivarEvaluable;
import org.lgmt.dgl.functions.UnivarVec3Evaluable;
import org.lgmt.dgl.functions.UnivarVec3Function;
import org.lgmt.dgl.functions.UnivarVec3FunctionFactory;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;

/**
 * Une courbe.
 *
 * <p>
 * Une courbe est définie par une fonction vectorielle d'une variable et les
 * bornes de cette variable.
 * </p>
 * <p>
 * Cette classe devrait être abstraite, mais elle est conservée instanciable
 * pour permettre la duplication de n'importe quel type de courbe. (voir
 * {@link #Curve(Curve c) Curve(Curve c)}. Tous les autres constructeurs sont
 * <b>protected</b>.
 * </p>
 * 
 * 
 * 
 * 
 * @navassoc - - - UnivarVec3Function
 * @navassoc - - - C_TYPE
 * 
 * @author redonnet
 *
 */
public class Curve implements UnivarVec3Evaluable, Transformable<Curve> {
	protected static UnivarVec3FunctionFactory functionFactory = new UnivarVec3FunctionFactory();
	protected C_TYPE type;
	protected double umin;
	protected double umax;
	protected UnivarVec3Function v3f;

	protected Curve() {
		super();
	}

	/*
	 * Crée une nouvelle courbe comme un clone en profondeur d'une surface existante
	 * 
	 * @param s
	 */
	public Curve(Curve c) {
		this.type = c.type;
		this.umin = c.umin;
		this.umax = c.umax;
		this.v3f = c.v3f;
	}

	/**
	 * Calcule un point de la courbe.
	 * 
	 * @param point un point pour stocker le résultat
	 * @param u
	 * @throws ParameterOutOfCurveBoundsException si le paramètre u est hors des
	 *                                            bornes
	 */
	@Override
	public void eval(Point3d point, double u) {
		if (Math.abs(u - umin) < Util.PREC12)
			u = umin;
		if (Math.abs(u - umax) < Util.PREC12)
			u = umax;
		if (u >= umin && u <= umax)
			v3f.eval(point, u);
		else {
			throw new ParameterOutOfCurveBoundsException(this, u);
		}
	}

	/**
	 * Calcule un point de la courbe.
	 * 
	 * @param u
	 * @return un nouveau point contenant le résultat
	 * @throws ParameterOutOfCurveBoundsException si le paramètre u est hors des
	 *                                            bornes
	 */
	@Override
	public Point3d eval(double u) {
		Point3d p = new Point3d();
		this.eval(p, u);
		return p;
	}

	/**
	 * Calcule la coordonnée en x d'un point de la courbe.
	 * 
	 * @param u
	 * @throws ParameterOutOfCurveBoundsException si le paramètre u est hors des
	 *                                            bornes
	 */
	public double evalX(double u) {
		if (Math.abs(u - umin) < Util.PREC12)
			u = umin;
		if (Math.abs(u - umax) < Util.PREC12)
			u = umax;
		if (u >= umin && u <= umax)
			return v3f.getFx().eval(u);
		else {
			throw new ParameterOutOfCurveBoundsException(this, u);
		}
	}

	/**
	 * Calcule la coordonnée en y d'un point de la courbe.
	 * 
	 * @param u
	 * @throws ParameterOutOfCurveBoundsException si le paramètre u est hors des
	 *                                            bornes
	 */
	public double evalY(double u) {
		if (Math.abs(u - umin) < Util.PREC12)
			u = umin;
		if (Math.abs(u - umax) < Util.PREC12)
			u = umax;
		if (u >= umin && u <= umax)
			return v3f.getFy().eval(u);
		else {
			throw new ParameterOutOfCurveBoundsException(this, u);
		}
	}

	/**
	 * Calcule la coordonnée en z d'un point de la courbe.
	 * 
	 * @param u
	 * @throws ParameterOutOfCurveBoundsException si le paramètre u est hors des
	 *                                            bornes
	 */
	public double evalZ(double u) {
		if (Math.abs(u - umin) < Util.PREC12)
			u = umin;
		if (Math.abs(u - umax) < Util.PREC12)
			u = umax;
		if (u >= umin && u <= umax)
			return v3f.getFz().eval(u);
		else {
			throw new ParameterOutOfCurveBoundsException(this, u);
		}
	}

	/**
	 * Test if the curve is defined for given parameter.
	 * 
	 * @param u
	 * @return true if the curve is defined for the given parameter
	 */
	public boolean isDefinedFor(double u) {
		if (Math.abs(u - umin) < Util.PREC12)
			u = umin;
		if (Math.abs(u - umax) < Util.PREC12)
			u = umax;
		if (u >= umin && u <= umax)
			return true;
		else
			return false;
	}

	/**
	 * Calcule la tangente à la courbe.
	 * 
	 * @param t un vecteur pour stocker le résultat
	 * @param u
	 */
	public void tangent(Vector3d t, double u) {
		t.x = v3f.diffx(u);
		t.y = v3f.diffy(u);
		t.z = v3f.diffz(u);
	}

	/**
	 * Calcule la tangente à la courbe.
	 * 
	 * @param u
	 * @return un nouveau vecteur contenant le résultat
	 */
	public Vector3d tangent(double u) {
		Vector3d t = new Vector3d();
		this.tangent(t, u);
		return t;
	}

	/**
	 * Calcule la normale à la courbe.
	 * 
	 * @param n un vecteur pour stocker le résultat
	 * @param u
	 */
	public void normal(Vector3d n, double u) {
		UnivarVec3Function t = functionFactory.newDiffVec3Function(v3f);
		n.x = t.diffx(u);
		n.y = t.diffy(u);
		n.z = t.diffz(u);
		n.normalize();
	}

	/**
	 * Calcule la normale à la courbe.
	 * 
	 * @param u
	 * @return un nouveau vecteur contenant le résultat
	 */
	public Vector3d normal(double u) {
		Vector3d n = new Vector3d();
		this.normal(n, u);
		return n;
	}

	/**
	 * 
	 * @return le type de la courbe
	 */
	public C_TYPE getType() {
		return type;
	}

	/**
	 * 
	 * @return la valeur maxi du paramètre u
	 */
	public double getUmax() {
		return umax;
	}

	/**
	 * 
	 * @return la valeur mini du paramètre u
	 */
	public double getUmin() {
		return umin;
	}

	/**
	 * 
	 * @return la fonction vectorielle de la courbe
	 */
	public UnivarVec3Function getV3f() {
		return v3f;
	}

	/**
	 * 
	 * @param umin
	 */
	public void setUmin(double umin) {
		this.umin = umin;
	}

	/**
	 * 
	 * @param umax
	 */
	public void setUmax(double umax) {
		this.umax = umax;
	}

	/**
	 * Calculate the normal vectorial function to the curve n(u)
	 * 
	 * @return the normal function
	 */
	public UnivarVec3Function getNormalFunction() {
		return Curve.getNormalFunctionStatic(this);
	}

	private static UnivarVec3Function getNormalFunctionStatic(final Curve c) {
		UnivarVec3Function result = new UnivarVec3Function(new UnivarEvaluable() {
			public double eval(double u) {
				Vector3d n = c.normal(u);
				return n.x;
			}
		}, new UnivarEvaluable() {
			public double eval(double u) {
				Vector3d n = c.normal(u);
				return n.y;
			}
		}, new UnivarEvaluable() {
			public double eval(double u) {
				Vector3d n = c.normal(u);
				return n.z;
			}
		});
		return result;
	}

	/**
	 * Déplace la courbe par une matrice de translation/rotation
	 * 
	 * @param m la matrice de transformation
	 */
	public void transform(Matrix4d m) {
		UnivarVec3Function newV3f = this.v3f.transformClone(m);
		this.v3f = newV3f;
	}

	/**
	 * Déplace le clone en profondeur d'une courbe en utilisant une matrice de
	 * translation/rotation
	 * 
	 * @param m la matrice de transformation
	 */
	public Curve transformClone(Matrix4d m) {
		Curve clone = new Curve(this);
		UnivarVec3Function newV3f = clone.v3f.transformClone(m);
		clone.v3f = newV3f;
		return clone;
	}

	// TODO : implémenter le calcul de la courbure et du rayon de courbure
	// public double curvature(double u);

}
