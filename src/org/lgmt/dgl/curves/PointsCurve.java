/*
 ** PointsCurve.java
 ** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
 **
 ** This file is part of the DGL software package (DGL stands for
 ** Differential Geometry Library). This software provides mathematic and numeric
 ** tools to work with various curves and surfaces (algebraic, Bézier,
 ** NURBS, ...) with an unified interface.
 **
 ** This software is governed by the CeCILL-C V2 license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 ** 
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-C license and that you accept its terms.
 */
package org.lgmt.dgl.curves;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.lgmt.dgl.commons.Transformable;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;

/**
 * Une courbe constituée par une suite de points.
 * 
 * <p>
 * La courbe n'est pas paramétrée.
 * 
 * <p>
 * Remarque : On utilise la liste de points en attribut plutôt que par un
 * héritage direct afin de pouvoir restreindre et/ou renommer les
 * fonctionnalités voulues en adaptant les delegates.
 * 
 * <p>
 * La courbe peut être observée en utilisant un PropertyChangeListener qui doit
 * être enregistré avec
 * {@link #addPropertyChangeListener(PropertyChangeListener)}. Les événemments
 * observables sont l'ajout, la suppression, l'insertion ou le remplacement d'un
 * point.
 * 
 * @param <T> Le type de points utilisé dans la courbe. Doit hériter de Point3d.
 * 
 * @author redonnet
 * 
 */
public class PointsCurve<T extends Point3d> implements Transformable<PointsCurve<? extends Point3d>> {
	protected final PropertyChangeSupport support = new PropertyChangeSupport(this);
	protected ArrayList<T> points;

	/**
	 * Crée une nouvelle courbe de points vide
	 */
	public PointsCurve() {
		this.points = new ArrayList<T>();
	}

	/**
	 * Crée une nouvelle courbe de points à partir d'une liste de points
	 * 
	 * @param points
	 */
	public PointsCurve(List<T> points) {
		this.points = new ArrayList<T>(points);
	}

	/**
	 * Copie en profondeur d'une courbe de points
	 *
	 * Deep Copy of a curve
	 * 
	 * @param curve la courbe à copier
	 */
	public PointsCurve(PointsCurve<T> curve) {
		ArrayList<T> clone = new ArrayList<T>(curve.getNbPoints());
		Iterator<T> iter = curve.getPoints().iterator();
		while (iter.hasNext())
			clone.add(iter.next());
		this.points = clone;
	}

	/**
	 * Ajout un point à la fin de la courbe.
	 * 
	 * @param point
	 * 
	 * @return true @see java.util.Collection<E>.add(E e)
	 * 
	 * @deprecated Use {@link #addPoint} instead
	 */
	public boolean add(T point) {
		return this.addPoint(point);
	}

	/**
	 * Ajout un point à la fin de la courbe.
	 * 
	 * @param point
	 * 
	 * @return true @see java.util.Collection<E>.add(E e)
	 */
	public boolean addPoint(T point) {
		boolean result = points.add(point);
		if (result)
			this.support.firePropertyChange("addPoint", null, point);
		return result;

	}

	/**
	 * Ajout un point à la fin de la courbe.
	 * 
	 * @param point
	 * 
	 * @deprecated Use {@link #insertPoint} instead
	 */
	public void add(int index, T point) {
		this.insertPoint(index, point);
	}

	/**
	 * Insère un point dans la courbe.
	 * 
	 * Le point est inséré à l'index passé en paramètre. Les points suivants sont
	 * décalés.
	 * 
	 * @param index
	 * @param point
	 */
	public void insertPoint(int index, T point) {
		points.add(index, point);
		this.support.firePropertyChange("insertPoint", index, point);
	}

	/**
	 * Supprime un point de la courbe.
	 * 
	 * Le point à l'index passé en paramètre est supprimé. Les points suivants sont
	 * décalés.
	 * 
	 * @param index
	 * @return le point supprimé
	 */
	public T removePoint(int index) {
		T result = points.remove(index);
		this.support.firePropertyChange("removePoint", index, null);
		return result;
	}

	/**
	 * Remplace un point de la courbe par un autre point du même type.
	 * 
	 * Le point à l'index passé en paramètre est remplacé par le point passé en
	 * paramètre.
	 * 
	 * @param index
	 * @param point
	 * @return le point remplacé
	 */
	public T replacePoint(int index, T point) {
		T result = points.set(index, point);
		this.support.firePropertyChange("replacePoint", index, point);
		return result;
	}

	public int getNbPoints() {
		return points.size();
	}

	public ArrayList<T> getPoints() {
		return points;
	}

	public T getPoint(int index) {
		return points.get(index);
	}

	public T getFirstPoint() {
		return points.get(0);
	}

	public T getLastPoint() {
		return points.get(getNbPoints() - 1);
	}

	public T get(int index) {
		return points.get(index);
	}

	public Iterator<T> iterator() {
		return points.iterator();
	}

	public ListIterator<T> listIterator() {
		return points.listIterator();
	}

	/**
	 * Retourne la longueur de la courbe. Ne pas confondre avec getNbPoints() qui
	 * retourne le nombre de points de la courbe.
	 * 
	 * @return the curve length
	 */
	public double length() {
		double length = 0;
		for (int i = 0; i < points.size() - 1; i++) {
			length = length + points.get(i).distance(points.get(i + 1));
		}
		return length;
	}

	/**
	 * Inverse l'ordre des points de la courbe
	 */
	public void reverse() {
		Collections.reverse(points);
	}

	/**
	 * 
	 * @return l'étendue de la courbe en X
	 */
	public double getXRange() {
		double xmin = Double.MAX_VALUE;
		double xmax = Double.NEGATIVE_INFINITY;
		for (T p : points) {
			if (p.x < xmin)
				xmin = p.x;
			if (p.x > xmax)
				xmax = p.x;
		}
		return xmax - xmin;
	}

	/**
	 * 
	 * @return l'étendue de la courbe en Y
	 */
	public double getYRange() {
		double ymin = Double.MAX_VALUE;
		double ymax = Double.NEGATIVE_INFINITY;
		for (T p : points) {
			if (p.y < ymin)
				ymin = p.y;
			if (p.y > ymax)
				ymax = p.y;
		}
		return ymax - ymin;
	}

	/**
	 * 
	 * @return l'étendue de la courbe en Z
	 */
	public double getZRange() {
		double zmin = Double.MAX_VALUE;
		double zmax = Double.NEGATIVE_INFINITY;
		for (T p : points) {
			if (p.z < zmin)
				zmin = p.z;
			if (p.z > zmax)
				zmax = p.z;
		}
		return zmax - zmin;
	}

	/**
	 * Observer design pattern implementation.
	 * 
	 * Add a listener
	 * 
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.support.addPropertyChangeListener(listener);
	}

	/**
	 * Observer design pattern implementation.
	 * 
	 * Remove a listener
	 * 
	 * @param listener
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.support.removePropertyChangeListener(listener);
	}

	/**
	 * Déplace la courbe par une matrice de translation/rotation
	 * 
	 * @param m la matrice de transformation
	 */
	@Override
	public void transform(Matrix4d m) {
		for (int i = 0; i < points.size(); i++) {
			m.transform(points.get(i));
		}
	}

	/**
	 * Déplace le clone en profondeur d'une courbe en utilisant une matrice de
	 * translation/rotation
	 * 
	 * @param m la matrice de transformation
	 */
	public PointsCurve<T> transformClone(Matrix4d m) {
		PointsCurve<T> clone = new PointsCurve<T>(this);
		clone.transform(m);
		return clone;
	}

}
