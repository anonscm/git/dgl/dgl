package org.lgmt.dgl.curves;

/**
 * Une exception levée quand on rencontre une courbe d'un type non géré.
 * 
 * Utile en particulier pour signaler les courbes B-Splines dont la séquence
 * nodale n'a pas une multiplicité égale au degré + 1 à ses extrémités.
 * 
 * @navassoc - - - Curve
 * 
 * @author redonnet
 *
 */
public class UnhandledCurveTypeException extends RuntimeException {
	private static final long serialVersionUID = 3327924627538915451L;

	public Curve c;
	public String m;

	/**
	 * Constructeur par défaut.
	 *
	 */
	public UnhandledCurveTypeException() {
		super();
	}

	/**
	 * Constructeur personnalisé.
	 * 
	 * @param c
	 *            la courbe
	 */
	public UnhandledCurveTypeException(Curve c) {
		super(new String("Unhandled curve type."));

		this.c = c;
	}

	/**
	 * Constructeur personnalisé.
	 * 
	 * @param c
	 *            la courbe
	 * @param m
	 *            a message
	 */
	public UnhandledCurveTypeException(Curve c, String m) {
		super(new String("Unhandled curve type."));

		this.c = c;
		this.m = m;
	}
}
