/*
** ParamPointsCurve.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.curves;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.UnivarEvaluable;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;

/**
 * Une courbe définie à partir d'une liste de points.
 * 
 * <p>
 * Une fonction vectorielle C(u) est associée à la courbe. La gestion de la
 * synchronisation entre cette fonction vectorielle et les éléments géométriques
 * de la courbe (c'est à dire le nombre et la position des points de la courbe)
 * est laissée à l'utilisateur car elle peut être coûteuse en temps de calcul et
 * n'est pas toujours nécessaire. La méthode {@link #syncParam()} permet
 * d'actualiser C(u).
 * 
 * @author redonnet
 * 
 */
public class ParamPointsCurve<T extends Point3d> extends Curve implements UnivarParametrizable {
	private ArrayList<T> points;
	/** la liste des points */
	private ArrayList<Double> params;
	/** la liste des valeurs paramétriques correspondants au points */
	private ArrayList<Double> lengths;
	/** la liste des longueurs de segments correspondant à chaque point */
	private boolean psync;

	/** true si la paramétrisation est synchronizée avec les points */

	/**
	 * Construit une nouvelle courbe à partir d'une liste de points de contrôle.
	 * 
	 * @param points les points de contrôle
	 */
	public ParamPointsCurve(List<T> points) {
		this.type = C_TYPE.C_POINTS;
		this.points = new ArrayList<T>(points);
		this.lengths = new ArrayList<Double>(points.size());
		this.params = new ArrayList<Double>(points.size());
		this.umin = 0.0;
		this.umax = 1.0;
		this.syncParam();
		this.v3f = functionFactory.newVec3Function(new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				if (Math.abs(u) < Util.PREC12)
					return points.get(0).x;
				double val;
				int i = 0;
				while (params.get(i) < u)
					i++;
				double uFw = params.get(i);
				double uBw = params.get(i - 1);
				Point3d pFw = points.get(i);
				Point3d pBw = points.get(i - 1);
				val = (u - uBw) / (uFw - uBw) * (pFw.x - pBw.x) + pBw.x;
				return val;
			}
		}, new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				if (Math.abs(u) < Util.PREC12)
					return points.get(0).y;
				double val;
				int i = 0;
				while (params.get(i) < u)
					i++;
				double uFw = params.get(i);
				double uBw = params.get(i - 1);
				Point3d pFw = points.get(i);
				Point3d pBw = points.get(i - 1);
				val = (u - uBw) / (uFw - uBw) * (pFw.y - pBw.y) + pBw.y;
				return val;
			}
		}, new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				if (Math.abs(u) < Util.PREC12)
					return points.get(0).z;
				double val;
				int i = 0;
				while (params.get(i) < u)
					i++;
				double uFw = params.get(i);
				double uBw = params.get(i - 1);
				Point3d pFw = points.get(i);
				Point3d pBw = points.get(i - 1);
				val = (u - uBw) / (uFw - uBw) * (pFw.z - pBw.z) + pBw.z;
				return val;
			}
		});
	}

	/**
	 * Copie en profondeur d'une courbe parametrique de points
	 * 
	 * Si la courbe passée en paramètre est synchronisée, son clone sera également
	 * synchronisé.
	 * 
	 * @param curve la courbe à copier
	 */
	public ParamPointsCurve(ParamPointsCurve<T> curve) {
		this.type = C_TYPE.C_POINTS;
		ArrayList<T> clone = new ArrayList<T>(curve.getNbPoints());
		Iterator<T> iter = curve.getPoints().iterator();
		while (iter.hasNext())
			clone.add(iter.next());
		this.points = clone;
		this.umin = 0.0;
		this.umax = 1.0;
		this.lengths = new ArrayList<Double>(points.size());
		this.params = new ArrayList<Double>(points.size());
		if (curve.isParamSync())
			this.syncParam();
	}

	/**
	 * Recalcule la paramétrisation de la courbe.
	 *
	 */
	@Override
	public void syncParam() {
		double l = 0;
		lengths.add(0, 0.0);
		for (int i = 1; i < points.size(); i++) {
			l = l + points.get(i - i).distance(points.get(i));
			lengths.add(i, l);
		}
		double totalLength = l;
		params.add(0, 0.0);
		for (int i = 1; i < points.size(); i++) {
			params.add(i, lengths.get(i) / totalLength);
		}

		psync = true;
	}

	/**
	 * Ajoute un point à la courbe.
	 * 
	 * <p>
	 * Ajoute un point à la fin de la liste des points de la courbe. Si
	 * <b>resync</b> est positionné à true, recalcule la fonction paramétrique
	 * associée.
	 * 
	 * @param point  le point à ajouter
	 * @param resync le flag de resynchronisation
	 */
	public void addPoint(T point, boolean resync) {
		points.add(point);
		if (resync)
			syncParam();
		else
			psync = false;
	}

	/**
	 * Ajoute un point à la courbe.
	 * 
	 * <p>
	 * Insère un point à la position définie par <b>index</b> de la liste des points
	 * de la courbe. Si <b>resync</b> est positionné à true, recalcule la fonction
	 * paramétrique associée.
	 * 
	 * @param index  l'indice du nouveau point
	 * @param point  le point à ajouter
	 * @param resync le flag de resynchronisation
	 */
	public void addPoint(int index, T point, boolean resync) {
		points.add(index, point);
		if (resync)
			syncParam();
		else
			psync = false;
	}

	/**
	 * Ajoute un point à la courbe.
	 * 
	 * <p>
	 * Remplace un point à la position définie par <b>index</b> de la liste des
	 * points de la courbe. Si <b>resync</b> est positionné à true, recalcule la
	 * fonction paramétrique associée.
	 * 
	 * @param index  l'indice du nouveau point
	 * @param point  le point à ajouter
	 * @param resync le flag de resynchronisation
	 */
	public void setPoint(int index, T point, boolean resync) {
		points.set(index, point);
		if (resync)
			syncParam();
		else
			psync = false;
	}

	/**
	 * Ajoute un point à la courbe.
	 * 
	 * <p>
	 * Ajoute un point à la fin de la liste des points de la courbe. Si
	 * <b>resync</b> est positionné à true, recalcule la fonction paramétrique
	 * associée.
	 * 
	 * @param point  le point à ajouter
	 * @param resync le flag de resynchronisation
	 */
	public void add(T point, boolean resync) {
		addPoint(point, resync);
	}

	/**
	 * Ajoute un point à la courbe.
	 * 
	 * <p>
	 * Insère un point à la position définie par <b>index</b> de la liste des points
	 * de la courbe. Si <b>resync</b> est positionné à true, recalcule la fonction
	 * paramétrique associée.
	 * 
	 * @param index  l'indice du nouveau point
	 * @param point  le point à ajouter
	 * @param resync le flag de resynchronisation
	 */
	public void add(int index, T point, boolean resync) {
		addPoint(index, point, resync);
	}

	/**
	 * 
	 * @return le nombre de points de la courbe
	 */
	public int getNbPoints() {
		return points.size();
	}

	/**
	 * 
	 * @return la liste des points de la courbe
	 */
	public ArrayList<T> getPoints() {
		return points;
	}

	/**
	 * 
	 * @param index l'indice du point à retourner
	 * @return un point de la courbe
	 */
	public T getPoint(int index) {
		return points.get(index);
	}

	/**
	 * 
	 * @param index l'indice du point à retourner
	 * @return un point de la courbe
	 */
	public T get(int index) {
		return points.get(index);
	}

	/**
	 * 
	 * 
	 * @return <b>true</b> si la fonction paramétrique de la courbe est synchronisée
	 *         avec la liste de point actuelle, <b>false</b> dans le cas contraire.
	 */
	@Override
	public boolean isParamSync() {
		return psync;
	}

	/**
	 * Déplace la courbe par une matrice de translation/rotation
	 * 
	 * @param m la matrice de transformation
	 */
	@Override
	public void transform(Matrix4d m) {
		for (int i = 0; i < points.size(); i++) {
			m.transform(points.get(i));
		}
	}
}
