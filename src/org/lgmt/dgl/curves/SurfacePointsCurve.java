/*
** SurfacePointsCurve.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.curves;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.surfaces.SurfacePoint;
import org.lgmt.dgl.vecmath.Matrix4d;

/**
 * Une courbe appartenant à une surface.
 * 
 * <p>
 * La courbe est définie par une liste de points appartenant à la surface.
 * 
 * @author redonnet
 * 
 */
public class SurfacePointsCurve extends PointsCurve<SurfacePoint> {
	private Surface surface;

	/**
	 * Construit un courbe vide en l'affectant à la surface passée en paramètre.
	 * 
	 * @param s
	 */
	public SurfacePointsCurve(Surface s) {
		super();
		this.surface = s;
	}

	/**
	 * Construit une nouvelle courbe à partir d'une liste de points.
	 * 
	 * ATTENTION: Tous les points sont censés appartenir à la même surface, mais
	 * aucune vérification n'est faite à ce niveau
	 * 
	 * @param points les points de contrôle
	 */
	public SurfacePointsCurve(List<SurfacePoint> points) {
		super(points);
		this.surface = points.get(0).getSurface();
	}

	/**
	 * Construit une nouvelle courbe à partir d'un tableau de points.
	 * 
	 * ATTENTION: Tous les points sont censés appartenir à la même surface, mais
	 * aucune vérification n'est faite à ce niveau
	 * 
	 * @param points les points de contrôle
	 */
	public SurfacePointsCurve(SurfacePoint[] points) {
		this(Arrays.asList(points));
	}

	/**
	 * Crée une nouvelle courbe définie comme un morceau d'une courbe passée en
	 * paramètre.
	 * 
	 * <p>
	 * Tous les points de la courbe <b>curve</b> dont l'index est compris entre
	 * <b>firstIndex</b> et <b>lastIndex</b> (inclus) sont clonés pour constituer la
	 * nouvelle courbe.
	 * 
	 * @param curve
	 * @param firstIndex
	 * @param lastIndex
	 */
	public SurfacePointsCurve(SurfacePointsCurve curve, int firstIndex, int lastIndex) {
		this.surface = curve.surface;
		this.points = new ArrayList<SurfacePoint>();
		for (int i = firstIndex; i <= lastIndex; i++) {
			points.add(new SurfacePoint(curve.get(i)));
		}
	}

	/**
	 * Copie en profondeur d'une courbe de points sur une surface
	 * 
	 * @param curve
	 */
	public SurfacePointsCurve(SurfacePointsCurve curve) {
		this.surface = curve.surface;
		this.points = new ArrayList<SurfacePoint>();
		for (SurfacePoint p : curve.getPoints()) {
			points.add(new SurfacePoint(p));
		}
	}

	/**
	 * 
	 * @return la surface support de cette courbe
	 */
	public Surface getSurface() {
		return surface;
	}

	/**
	 * Déplace la courbe par une matrice de translation/rotation.
	 * 
	 * La surface associée est transformée et copiée dans un clone.
	 * 
	 * @param m la matrice de transformation
	 */
	public void transform(Matrix4d m) {
		if(this.surface != null)
			this.surface = this.surface.transformClone(m);
		for (int i = 0; i < this.getNbPoints(); i++) {
			m.transform(this.get(i));
		}
	}

	/**
	 * Clone la courbe et effectue un changement de repère sur ce clone
	 * 
	 * @param m matrice de transformation
	 */
	public SurfacePointsCurve transformClone(Matrix4d m) {
		SurfacePointsCurve clone = new SurfacePointsCurve(this);
		clone.transform(m);
		return clone;
	}
}
