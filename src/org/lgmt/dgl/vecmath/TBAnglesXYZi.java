package org.lgmt.dgl.vecmath;

import static java.lang.Math.asin;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class TBAnglesXYZi extends TBAngles {

	private static final long serialVersionUID = 945496874259949305L;

	public TBAnglesXYZi(double alpha, double beta, double gamma) {
		super(alpha, beta, gamma);
		this.order = ORDER.XYZ;
		this.convention = CONVENTION.INTRINSIC;
	}

	public TBAnglesXYZi(Matrix3d m) {
		super(m);
		this.alpha = atan2(-m.m12, m.m22);
		this.beta = asin(m.m02);
		this.gamma = atan2(-m.m01, m.m11);
		this.order = ORDER.XYZ;
		this.convention = CONVENTION.INTRINSIC;
	}

	@Override
	public Matrix3d getRotationMatrix() {
		Matrix3d m = new Matrix3d();

		m.m00 = cos(beta) * cos(gamma);
		m.m01 = -sin(beta);
		m.m02 = cos(beta) * sin(gamma);
		m.m10 = sin(alpha) * sin(gamma) + cos(alpha) * cos(gamma) * sin(beta);
		m.m11 = cos(alpha) * cos(beta);
		m.m12 = cos(alpha) * sin(beta) * sin(gamma) - cos(gamma) * sin(alpha);
		m.m20 = cos(gamma) * sin(alpha) * sin(beta) - cos(alpha) * sin(gamma);
		m.m21 = cos(beta) * sin(alpha);
		m.m22 = cos(alpha) * cos(gamma) + sin(alpha) * sin(beta) * sin(gamma);

		return m;
	}

}
