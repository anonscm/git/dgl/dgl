package org.lgmt.dgl.vecmath;

import org.lgmt.dgl.commons.Util;

/**
 * A double precision floating point 2 by 2 matrix.
 *
 * Adapted from vecmath
 * 
 * NOTE: not present in vecmath.
 *
 */
public class Matrix2d implements java.io.Serializable, Cloneable {
	// Compatible with 1.1
	static final long serialVersionUID = 6837536777072402710L;

	/**
	 * The first matrix element in the first row.
	 */
	public double m00;

	/**
	 * The second matrix element in the first row.
	 */
	public double m01;

	/**
	 * The first matrix element in the second row.
	 */
	public double m10;

	/**
	 * The second matrix element in the second row.
	 */
	public double m11;

	/**
	 * Constructs and initializes a Matrix2d from the specified nine values.
	 * 
	 * @param m00 the [0][0] element
	 * @param m01 the [0][1] element
	 * @param m10 the [1][0] element
	 * @param m11 the [1][1] element
	 */
	public Matrix2d(double m00, double m01, double m10, double m11) {
		this.m00 = m00;
		this.m01 = m01;

		this.m10 = m10;
		this.m11 = m11;

	}

	/**
	 * Constructs and initializes a Matrix2d from the specified four-element array.
	 * 
	 * @param v the array of length 4 containing in order
	 */
	public Matrix2d(double[] v) {
		this.m00 = v[0];
		this.m01 = v[1];

		this.m10 = v[2];
		this.m11 = v[3];

	}

	/**
	 * Constructs a new matrix with the same values as the Matrix2d parameter.
	 * 
	 * @param m1 the source matrix
	 */
	public Matrix2d(Matrix2d m1) {
		this.m00 = m1.m00;
		this.m01 = m1.m01;

		this.m10 = m1.m10;
		this.m11 = m1.m11;
	}

	/**
	 * Constructs and initializes a Matrix2d to all zeros.
	 */
	public Matrix2d() {
		this.m00 = 0.0;
		this.m01 = 0.0;

		this.m10 = 0.0;
		this.m11 = 0.0;

	}

	/**
	 * Returns a string that contains the values of this Matrix2d.
	 * 
	 * @return the String representation
	 */
	@Override
	public String toString() {
		return this.m00 + ", " + this.m01 + "\n" + this.m10 + ", " + this.m11 + "\n";
	}

	/**
	 * Sets this Matrix2d to identity.
	 */
	public final void setIdentity() {
		this.m00 = 1.0;
		this.m01 = 0.0;

		this.m10 = 0.0;
		this.m11 = 1.0;

	}

	/**
	 * Sets the specified element of this matrix2d to the value provided.
	 * 
	 * @param row    the row number to be modified (zero indexed)
	 * @param column the column number to be modified (zero indexed)
	 * @param value  the new value
	 */
	public final void setElement(int row, int column, double value) {
		switch (row) {
		case 0:
			switch (column) {
			case 0:
				this.m00 = value;
				break;
			case 1:
				this.m01 = value;
				break;
			default:
				throw new ArrayIndexOutOfBoundsException();
			}
			break;

		case 1:
			switch (column) {
			case 0:
				this.m10 = value;
				break;
			case 1:
				this.m11 = value;
				break;
			default:
				throw new ArrayIndexOutOfBoundsException();
			}
			break;

		default:
			throw new ArrayIndexOutOfBoundsException();
		}
	}

	/**
	 * Retrieves the value at the specified row and column of the specified matrix.
	 * 
	 * @param row    the row number to be retrieved (zero indexed)
	 * @param column the column number to be retrieved (zero indexed)
	 * @return the value at the indexed element.
	 */
	public final double getElement(int row, int column) {
		switch (row) {
		case 0:
			switch (column) {
			case 0:
				return (this.m00);
			case 1:
				return (this.m01);
			default:
				break;
			}
			break;
		case 1:
			switch (column) {
			case 0:
				return (this.m10);
			case 1:
				return (this.m11);
			default:
				break;
			}
			break;

		default:
			break;
		}

		throw new ArrayIndexOutOfBoundsException();
	}

	/**
	 * Copies the matrix values in the specified row into the vector parameter.
	 * 
	 * @param row the matrix row
	 * @param v   the vector into which the matrix row values will be copied
	 */
	public final void getRow(int row, Vector2d v) {
		if (row == 0) {
			v.x = m00;
			v.y = m01;
		} else if (row == 1) {
			v.x = m10;
			v.y = m11;
		} else {
			throw new ArrayIndexOutOfBoundsException();
		}

	}

	/**
	 * Copies the matrix values in the specified row into the array parameter.
	 * 
	 * @param row the matrix row
	 * @param v   the array into which the matrix row values will be copied
	 */
	public final void getRow(int row, double v[]) {
		if (row == 0) {
			v[0] = m00;
			v[1] = m01;
		} else if (row == 1) {
			v[0] = m10;
			v[1] = m11;
		} else {
			throw new ArrayIndexOutOfBoundsException();
		}

	}

	/**
	 * Copies the matrix values in the specified column into the vector parameter.
	 * 
	 * @param column the matrix column
	 * @param v      the vector into which the matrix row values will be copied
	 */
	public final void getColumn(int column, Vector2d v) {
		if (column == 0) {
			v.x = m00;
			v.y = m10;
		} else if (column == 1) {
			v.x = m01;
			v.y = m11;
		} else {
			throw new ArrayIndexOutOfBoundsException();
		}

	}

	/**
	 * Copies the matrix values in the specified column into the array parameter.
	 * 
	 * @param column the matrix column
	 * @param v      the array into which the matrix row values will be copied
	 */
	public final void getColumn(int column, double v[]) {
		if (column == 0) {
			v[0] = m00;
			v[1] = m10;
		} else if (column == 1) {
			v[0] = m01;
			v[1] = m11;
		} else {
			throw new ArrayIndexOutOfBoundsException();
		}
	}

	/**
	 * Sets the specified row of this Matrix2d to the 2 values provided.
	 * 
	 * @param row the row number to be modified (zero indexed)
	 * @param x   the first column element
	 * @param y   the second column element
	 */
	public final void setRow(int row, double x, double y) {
		switch (row) {
		case 0:
			this.m00 = x;
			this.m01 = y;
			break;

		case 1:
			this.m10 = x;
			this.m11 = y;
			break;

		default:
			throw new ArrayIndexOutOfBoundsException();
		}
	}

	/**
	 * Sets the specified row of this Matrix2d to the Vector provided.
	 * 
	 * @param row the row number to be modified (zero indexed)
	 * @param v   the replacement row
	 */
	public final void setRow(int row, Vector2d v) {
		switch (row) {
		case 0:
			this.m00 = v.x;
			this.m01 = v.y;
			break;

		case 1:
			this.m10 = v.x;
			this.m11 = v.y;
			break;

		default:
			throw new ArrayIndexOutOfBoundsException();
		}
	}

	/**
	 * Sets the specified row of this Matrix2d to the two values provided.
	 * 
	 * @param row the row number to be modified (zero indexed)
	 * @param v   the replacement row
	 */
	public final void setRow(int row, double v[]) {
		switch (row) {
		case 0:
			this.m00 = v[0];
			this.m01 = v[1];
			break;

		case 1:
			this.m10 = v[0];
			this.m11 = v[1];
			break;

		default:
			throw new ArrayIndexOutOfBoundsException();
		}
	}

	/**
	 * Sets the specified column of this Matrix2d to the two values provided.
	 * 
	 * @param column the column number to be modified (zero indexed)
	 * @param x      the first row element
	 * @param y      the second row element
	 */
	public final void setColumn(int column, double x, double y) {
		switch (column) {
		case 0:
			this.m00 = x;
			this.m10 = y;
			break;

		case 1:
			this.m01 = x;
			this.m11 = y;
			break;

		default:
			throw new ArrayIndexOutOfBoundsException();
		}
	}

	/**
	 * Sets the specified column of this Matrix2d to the vector provided.
	 * 
	 * @param column the column number to be modified (zero indexed)
	 * @param v      the replacement column
	 */
	public final void setColumn(int column, Vector3d v) {
		switch (column) {
		case 0:
			this.m00 = v.x;
			this.m10 = v.y;
			break;

		case 1:
			this.m01 = v.x;
			this.m11 = v.y;
			break;

		default:
			throw new ArrayIndexOutOfBoundsException();
		}
	}

	/**
	 * Sets the specified column of this Matrix2d to the three values provided.
	 * 
	 * @param column the column number to be modified (zero indexed)
	 * @param v      the replacement column
	 */
	public final void setColumn(int column, double v[]) {
		switch (column) {
		case 0:
			this.m00 = v[0];
			this.m10 = v[1];
			break;

		case 1:
			this.m01 = v[0];
			this.m11 = v[1];
			break;

		default:
			throw new ArrayIndexOutOfBoundsException();
		}
	}

	/**
	 * Adds a scalar to each component of this matrix.
	 * 
	 * @param scalar the scalar adder
	 */
	public final void add(double scalar) {
		m00 += scalar;
		m01 += scalar;

		m10 += scalar;
		m11 += scalar;

	}

	/**
	 * Adds a scalar to each component of the matrix m1 and places the result into
	 * this. Matrix m1 is not modified.
	 * 
	 * @param scalar the scalar adder
	 * @param m1     the original matrix values
	 */
	public final void add(double scalar, Matrix2d m1) {
		this.m00 = m1.m00 + scalar;
		this.m01 = m1.m01 + scalar;

		this.m10 = m1.m10 + scalar;
		this.m11 = m1.m11 + scalar;
	}

	/**
	 * Sets the value of this matrix to the matrix sum of matrices m1 and m2.
	 * 
	 * @param m1 the first matrix
	 * @param m2 the second matrix
	 */
	public final void add(Matrix2d m1, Matrix2d m2) {
		this.m00 = m1.m00 + m2.m00;
		this.m01 = m1.m01 + m2.m01;

		this.m10 = m1.m10 + m2.m10;
		this.m11 = m1.m11 + m2.m11;

	}

	/**
	 * Sets the value of this matrix to the sum of itself and matrix m1.
	 * 
	 * @param m1 the other matrix
	 */
	public final void add(Matrix2d m1) {
		this.m00 += m1.m00;
		this.m01 += m1.m01;

		this.m10 += m1.m10;
		this.m11 += m1.m11;
	}

	/**
	 * Sets the value of this matrix to the matrix difference of matrices m1 and m2.
	 * 
	 * @param m1 the first matrix
	 * @param m2 the second matrix
	 */
	public final void sub(Matrix2d m1, Matrix2d m2) {
		this.m00 = m1.m00 - m2.m00;
		this.m01 = m1.m01 - m2.m01;

		this.m10 = m1.m10 - m2.m10;
		this.m11 = m1.m11 - m2.m11;
	}

	/**
	 * Sets the value of this matrix to the matrix difference of itself and matrix
	 * m1 (this = this - m1).
	 * 
	 * @param m1 the other matrix
	 */
	public final void sub(Matrix2d m1) {
		this.m00 -= m1.m00;
		this.m01 -= m1.m01;

		this.m10 -= m1.m10;
		this.m11 -= m1.m11;
	}

	/**
	 * Sets the value of this matrix to its transpose.
	 */
	public final void transpose() {
		double temp;

		temp = this.m10;
		this.m10 = this.m01;
		this.m01 = temp;
	}

	/**
	 * Sets the value of this matrix to the transpose of the argument matrix.
	 * 
	 * @param m1 the matrix to be transposed
	 */
	public final void transpose(Matrix2d m1) {
		if (this != m1) {
			this.m00 = m1.m00;
			this.m01 = m1.m10;

			this.m10 = m1.m01;
			this.m11 = m1.m11;

		} else
			this.transpose();
	}

	/**
	 * Sets the value of this matrix to the value of the Matrix2d argument.
	 * 
	 * @param m1 the source Matrix2d
	 */
	public final void set(Matrix2d m1) {
		this.m00 = m1.m00;
		this.m01 = m1.m01;

		this.m10 = m1.m10;
		this.m11 = m1.m11;

	}

	/**
	 * Sets the values in this Matrix2d equal to the row-major array parameter (ie,
	 * the first three elements of the array will be copied into the first row of
	 * this matrix, etc.).
	 * 
	 * @param m the double precision array of length 4
	 */
	public final void set(double[] m) {
		m00 = m[0];
		m01 = m[1];

		m10 = m[2];
		m11 = m[3];

	}

	/**
	 * Sets the value of this matrix to the matrix inverse of the passed matrix m1.
	 * 
	 * @param m1 the matrix to be inverted
	 */
	public final void invert(Matrix2d m1) {
		invertGeneral(m1);
	}

	/**
	 * Inverts this matrix in place.
	 */
	public final void invert() {
		invertGeneral(this);
	}

	/**
	 * General invert routine. Inverts m1 and places the result in "this". Note that
	 * this routine handles both the "this" version and the non-"this" version.
	 *
	 */
	private final void invertGeneral(Matrix2d m1) {
		double a, b, c, d;

		double det = this.determinant();

		if (Math.abs(det) < Util.PREC12) {
			// Matrix has no inverse
			throw new SingularMatrixException();
		}

		a = m1.m00;
		b = m1.m01;
		c = m1.m10;
		d = m1.m11;

		this.m00 = d;
		this.m01 = -b;

		this.m10 = -c;
		this.m11 = a;

		this.mul(1.0 / det);
	}

	/**
	 * Computes the determinant of this matrix.
	 * 
	 * @return the determinant of the matrix
	 */
	public final double determinant() {
		double total;

		total = this.m00 * this.m11 - this.m01 * this.m10;
		return total;
	}

	/**
	 * Multiplies each element of this matrix by a scalar.
	 * 
	 * @param scalar The scalar multiplier.
	 */
	public final void mul(double scalar) {
		m00 *= scalar;
		m01 *= scalar;

		m10 *= scalar;
		m11 *= scalar;

	}

	/**
	 * Multiplies each element of matrix m1 by a scalar and places the result into
	 * this. Matrix m1 is not modified.
	 * 
	 * @param scalar the scalar multiplier
	 * @param m1     the original matrix
	 */
	public final void mul(double scalar, Matrix2d m1) {
		this.m00 = scalar * m1.m00;
		this.m01 = scalar * m1.m01;

		this.m10 = scalar * m1.m10;
		this.m11 = scalar * m1.m11;

	}

	/**
	 * Sets the value of this matrix to the result of multiplying itself with matrix
	 * m1.
	 * 
	 * @param m1 the other matrix
	 */
	public final void mul(Matrix2d m1) {
		double m00, m01, m10, m11;

		m00 = this.m00 * m1.m00 + this.m01 * m1.m10;
		m01 = this.m00 * m1.m01 + this.m01 * m1.m11;

		m10 = this.m10 * m1.m00 + this.m11 * m1.m10;
		m11 = this.m10 * m1.m01 + this.m11 * m1.m11;

		this.m00 = m00;
		this.m01 = m01;
		this.m10 = m10;
		this.m11 = m11;

	}

	/**
	 * Sets the value of this matrix to the result of multiplying the two argument
	 * matrices together.
	 * 
	 * @param m1 the first matrix
	 * @param m2 the second matrix
	 */
	public final void mul(Matrix2d m1, Matrix2d m2) {
		if (this != m1 && this != m2) {
			this.m00 = m1.m00 * m2.m00 + m1.m01 * m2.m10;
			this.m01 = m1.m00 * m2.m01 + m1.m01 * m2.m11;

			this.m10 = m1.m10 * m2.m00 + m1.m11 * m2.m10;
			this.m11 = m1.m10 * m2.m01 + m1.m11 * m2.m11;

		} else {
			double m00, m01, m10, m11; // vars for temp result matrix

			m00 = m1.m00 * m2.m00 + m1.m01 * m2.m10;
			m01 = m1.m00 * m2.m01 + m1.m01 * m2.m11;

			m10 = m1.m10 * m2.m00 + m1.m11 * m2.m10;
			m11 = m1.m10 * m2.m01 + m1.m11 * m2.m11;

			this.m00 = m00;
			this.m01 = m01;
			this.m10 = m10;
			this.m11 = m11;
		}
	}

	/**
	 * Multiplies the transpose of matrix m1 times the transpose of matrix m2, and
	 * places the result into this.
	 * 
	 * @param m1 the matrix on the left hand side of the multiplication
	 * @param m2 the matrix on the right hand side of the multiplication
	 */
	public final void mulTransposeBoth(Matrix2d m1, Matrix2d m2) {
		if (this != m1 && this != m2) {
			this.m00 = m1.m00 * m2.m00 + m1.m10 * m2.m01;
			this.m01 = m1.m00 * m2.m10 + m1.m10 * m2.m11;

			this.m10 = m1.m01 * m2.m00 + m1.m11 * m2.m01;
			this.m11 = m1.m01 * m2.m10 + m1.m11 * m2.m11;
		} else {
			double m00, m01, m10, m11; // vars for temp result matrix

			m00 = m1.m00 * m2.m00 + m1.m10 * m2.m01;
			m01 = m1.m00 * m2.m10 + m1.m10 * m2.m11;

			m10 = m1.m01 * m2.m00 + m1.m11 * m2.m01;
			m11 = m1.m01 * m2.m10 + m1.m11 * m2.m11;

			this.m00 = m00;
			this.m01 = m01;
			this.m10 = m10;
			this.m11 = m11;
		}

	}

	/**
	 * Multiplies matrix m1 times the transpose of matrix m2, and places the result
	 * into this.
	 * 
	 * @param m1 the matrix on the left hand side of the multiplication
	 * @param m2 the matrix on the right hand side of the multiplication
	 */
	public final void mulTransposeRight(Matrix2d m1, Matrix2d m2) {
		if (this != m1 && this != m2) {
			this.m00 = m1.m00 * m2.m00 + m1.m01 * m2.m01;
			this.m01 = m1.m00 * m2.m10 + m1.m01 * m2.m11;

			this.m10 = m1.m10 * m2.m00 + m1.m11 * m2.m01;
			this.m11 = m1.m10 * m2.m10 + m1.m11 * m2.m11;

		} else {
			double m00, m01, m10, m11; // vars for temp result matrix

			m00 = m1.m00 * m2.m00 + m1.m01 * m2.m01;
			m01 = m1.m00 * m2.m10 + m1.m01 * m2.m11;

			m10 = m1.m10 * m2.m00 + m1.m11 * m2.m01;
			m11 = m1.m10 * m2.m10 + m1.m11 * m2.m11;

			this.m00 = m00;
			this.m01 = m01;
			this.m10 = m10;
			this.m11 = m11;
		}
	}

	/**
	 * Multiplies the transpose of matrix m1 times matrix m2, and places the result
	 * into this.
	 * 
	 * @param m1 the matrix on the left hand side of the multiplication
	 * @param m2 the matrix on the right hand side of the multiplication
	 */
	public final void mulTransposeLeft(Matrix2d m1, Matrix2d m2) {
		if (this != m1 && this != m2) {
			this.m00 = m1.m00 * m2.m00 + m1.m10 * m2.m10;
			this.m01 = m1.m00 * m2.m01 + m1.m10 * m2.m11;

			this.m10 = m1.m01 * m2.m00 + m1.m11 * m2.m10;
			this.m11 = m1.m01 * m2.m01 + m1.m11 * m2.m11;
		} else {
			double m00, m01, m10, m11; // vars for temp result matrix

			m00 = m1.m00 * m2.m00 + m1.m10 * m2.m10;
			m01 = m1.m00 * m2.m01 + m1.m10 * m2.m11;

			m10 = m1.m01 * m2.m00 + m1.m11 * m2.m10;
			m11 = m1.m01 * m2.m01 + m1.m11 * m2.m11;

			this.m00 = m00;
			this.m01 = m01;
			this.m10 = m10;
			this.m11 = m11;
		}
	}

	/**
	 * Perform cross product normalization of this matrix.
	 */

	public final void normalizeCP() {
		double mag = 1.0 / Math.sqrt(m00 * m00 + m10 * m10);
		m00 = m00 * mag;
		m10 = m10 * mag;

		mag = 1.0 / Math.sqrt(m01 * m01 + m11 * m11);
		m01 = m01 * mag;
		m11 = m11 * mag;
	}

	/**
	 * Perform cross product normalization of matrix m1 and place the normalized
	 * values into this.
	 * 
	 * @param m1 Provides the matrix values to be normalized
	 */
	public final void normalizeCP(Matrix2d m1) {
		double mag = 1.0 / Math.sqrt(m1.m00 * m1.m00 + m1.m10 * m1.m10);
		m00 = m1.m00 * mag;
		m10 = m1.m10 * mag;

		mag = 1.0 / Math.sqrt(m1.m01 * m1.m01 + m1.m11 * m1.m11);
		m01 = m1.m01 * mag;
		m11 = m1.m11 * mag;
	}

	/**
	 * Returns true if all of the data members of Matrix2d m1 are equal to the
	 * corresponding data members in this Matrix2d.
	 * 
	 * @param m1 the matrix with which the comparison is made
	 * @return true or false
	 */
	public boolean equals(Matrix2d m1) {
		try {
			return (this.m00 == m1.m00 && this.m01 == m1.m01 && this.m10 == m1.m10 && this.m11 == m1.m11);
		} catch (NullPointerException e2) {
			return false;
		}

	}

	/**
	 * Returns true if the Object t1 is of type Matrix2d and all of the data members
	 * of t1 are equal to the corresponding data members in this Matrix2d.
	 * 
	 * @param t1 the matrix with which the comparison is made
	 * @return true or false
	 */
	@Override
	public boolean equals(Object t1) {
		try {
			Matrix2d m2 = (Matrix2d) t1;
			return (this.m00 == m2.m00 && this.m01 == m2.m01 && this.m10 == m2.m10 && this.m11 == m2.m11);
		} catch (ClassCastException e1) {
			return false;
		} catch (NullPointerException e2) {
			return false;
		}

	}

	/**
	 * Returns true if the L-infinite distance between this matrix and matrix m1 is
	 * less than or equal to the epsilon parameter, otherwise returns false. The
	 * L-infinite distance is equal to MAX[i=0,1 ; j=0,1 ; abs(this.m(i,j) -
	 * m1.m(i,j))]
	 * 
	 * @param m1      the matrix to be compared to this matrix
	 * @param epsilon the threshold value
	 */
	public boolean epsilonEquals(Matrix2d m1, double epsilon) {
		double diff;

		diff = m00 - m1.m00;
		if ((diff < 0 ? -diff : diff) > epsilon)
			return false;

		diff = m01 - m1.m01;
		if ((diff < 0 ? -diff : diff) > epsilon)
			return false;

		diff = m10 - m1.m10;
		if ((diff < 0 ? -diff : diff) > epsilon)
			return false;

		diff = m11 - m1.m11;
		if ((diff < 0 ? -diff : diff) > epsilon)
			return false;

		return true;
	}

	/**
	 * Sets this matrix to all zeros.
	 */
	public final void setZero() {
		m00 = 0.0;
		m01 = 0.0;

		m10 = 0.0;
		m11 = 0.0;

	}

	/**
	 * Negates the value of this matrix: this = -this.
	 */
	public final void negate() {
		this.m00 = -this.m00;
		this.m01 = -this.m01;

		this.m10 = -this.m10;
		this.m11 = -this.m11;

	}

	/**
	 * Sets the value of this matrix equal to the negation of of the Matrix2d
	 * parameter.
	 * 
	 * @param m1 the source matrix
	 */
	public final void negate(Matrix2d m1) {
		this.m00 = -m1.m00;
		this.m01 = -m1.m01;

		this.m10 = -m1.m10;
		this.m11 = -m1.m11;

	}

	/**
	 * Multiply this matrix by the tuple t and place the result back into the tuple
	 * (t = this*t).
	 * 
	 * @param t the tuple to be multiplied by this matrix and then replaced
	 */
	public final void transform(Tuple2d t) {
		double x, y;
		x = m00 * t.x + m01 * t.y;
		y = m10 * t.x + m11 * t.y;
		t.set(x, y);
	}

	/**
	 * Multiply this matrix by the tuple t and and place the result into the tuple
	 * "result" (result = this*t).
	 * 
	 * @param t      the tuple to be multiplied by this matrix
	 * @param result the tuple into which the product is placed
	 */
	public final void transform(Tuple2d t, Tuple2d result) {
		double x, y;
		x = m00 * t.x + m01 * t.y;
		y = m10 * t.x + m11 * t.y;
		result.x = x;
		result.y = y;
	}

	static double max(double a, double b) {
		if (a > b)
			return (a);
		else
			return (b);
	}

	static double min(double a, double b) {
		if (a < b)
			return (a);
		else
			return (b);
	}

	static void print_mat(double[] mat) {
		int i;
		for (i = 0; i < 2; i++) {
			System.out.println(mat[i * 2 + 0] + " " + mat[i * 2 + 1] + " " + mat[i * 2 + 2] + "\n");
		}

	}

	static void print_det(double[] mat) {
		double det;

		det = mat[0] * mat[4] * mat[8] + mat[1] * mat[5] * mat[6] + mat[2] * mat[3] * mat[7] - mat[2] * mat[4] * mat[6]
				- mat[0] * mat[5] * mat[7] - mat[1] * mat[3] * mat[8];
		System.out.println("det= " + det);
	}

	/**
	 * Get the first matrix element in the first row.
	 * 
	 * @return Returns the m00.
	 * @since vecmath 1.5
	 */
	public final double getM00() {
		return m00;
	}

	/**
	 * Set the first matrix element in the first row.
	 * 
	 * @param m00 The m00 to set.
	 * 
	 * @since vecmath 1.5
	 */
	public final void setM00(double m00) {
		this.m00 = m00;
	}

	/**
	 * Get the second matrix element in the first row.
	 * 
	 * @return Returns the m01.
	 * 
	 * @since vecmath 1.5
	 */
	public final double getM01() {
		return m01;
	}

	/**
	 * Set the second matrix element in the first row.
	 * 
	 * @param m01 The m01 to set.
	 * 
	 * @since vecmath 1.5
	 */
	public final void setM01(double m01) {
		this.m01 = m01;
	}

	/**
	 * Get first matrix element in the second row.
	 * 
	 * @return Returns the m10.
	 * 
	 * @since vecmath 1.5
	 */
	public final double getM10() {
		return m10;
	}

	/**
	 * Set first matrix element in the second row.
	 * 
	 * @param m10 The m10 to set.
	 * 
	 * @since vecmath 1.5
	 */
	public final void setM10(double m10) {
		this.m10 = m10;
	}

	/**
	 * Get second matrix element in the second row.
	 * 
	 * @return Returns the m11.
	 * 
	 * @since vecmath 1.5
	 */
	public final double getM11() {
		return m11;
	}

	/**
	 * Set the second matrix element in the second row.
	 * 
	 * @param m11 The m11 to set.
	 * 
	 * @since vecmath 1.5
	 */
	public final void setM11(double m11) {
		this.m11 = m11;
	}

}
