/*
 * Copyright 1997-2008 Sun Microsystems, Inc.  All Rights Reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Sun Microsystems, Inc., 4150 Network Circle, Santa Clara,
 * CA 95054 USA or visit www.sun.com if you need additional information or
 * have any questions.
 *
 * $Revision: 1.5 $
 * $Date: 2008/02/28 20:18:50 $
 * $State: Exp $
 */

package org.lgmt.dgl.vecmath;

/**
 * A toolbox class gathering all static points methods
 * 
 * @author redonnet
 *
 */
public class Points {
	/**
	 * Returns p1 + p2 in a new point.
	 * 
	 * @param p1 the first point
	 * @param p2 the second point
	 */
	public static final Point3d add(Point3d p1, Point3d p2) {
		return new Point3d(p1.x + p2.x, p1.y + p2.y, p1.z + p2.z);
	}

	/**
	 * Returns p1 - p2 in a new point.
	 * 
	 * @param p1 the first point
	 * @param p2 the second point
	 */
	public static final Point3d sub(Point3d p1, Point3d p2) {
		return new Point3d(p1.x - p2.x, p1.y - p2.y, p1.z - p2.z);
	}

	/**
	 * Returns -p1 in a new point.
	 * 
	 * @param p1 the point
	 */
	public static final Point3d negate(Point3d p1) {
		return new Point3d(-p1.x, -p1.y, -p1.z);
	}

	/**
	 * Returns s*p1 in a new point.
	 * 
	 * @param s  the scalar value
	 * @param p1 the source point
	 * @return
	 */
	public static final Point3d scale(double s, Point3d p1) {
		return new Point3d(s * p1.x, s * p1.y, s * p1.z);
	}

	/**
	 * Returns the result of a point translated along a vector in a new Point.
	 * 
	 * @param p the initial point
	 * @param v the translation vector
	 * @return
	 */
	public static final Point3d translateClone(Point3d p, Vector3d v) {
		return new Point3d(p.x + v.x, p.y + v.y, p.z + v.z);
	}

	/**
	 * Returns (1 - k) * p1 + k * p2 in a new point.
	 * 
	 * @param p1 the first point
	 * @param p2 the second point
	 * @param k  the scalar interpolation factor
	 */
	public static final Point3d interpolate(Point3d p1, Point3d p2, double k) {
		return new Point3d((1 - k) * p1.x + k * p2.x, (1 - k) * p1.y + k * p2.y, (1 - k) * p1.z + k * p2.z);
	}

}
