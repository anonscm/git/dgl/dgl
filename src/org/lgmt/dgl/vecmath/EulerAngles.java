package org.lgmt.dgl.vecmath;

import static java.lang.Math.acos;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

import org.lgmt.dgl.commons.Util;

/**
 * A class to represent Euler angles
 * 
 * Euler angles are used to define generic rotations corresponding to the
 * following frame axis rotations (in this order):
 * <ol>
 * <li>psi rotation around Z</li>
 * <li>theta rotation around new X axis</li>
 * <li>phi rotation around new Z axis</li>
 * </ol>
 * 
 */
public class EulerAngles implements java.io.Serializable {
	private static final long serialVersionUID = -8117923351110379623L;

	/** the precession angle (around Z) */
	public double psi;
	/** the nutation angle (around X') */
	public double theta;
	/** the intrinsic rotation angle (around Z') */
	public double phi;

	/**
	 * Creates a new Euler angles.
	 * 
	 * @param psi
	 * @param theta
	 * @param phi
	 */
	public EulerAngles(double psi, double theta, double phi) {
		super();
		this.psi = Util.angleToInterval(psi, 0.0);
		this.theta = Util.angleToInterval(theta, 0.0);
		this.phi = Util.angleToInterval(phi, 0.0);
	}

	public EulerAngles(Matrix3d m) {
		this.theta = acos(m.m22);
		this.phi = atan2(m.m20, m.m21);
		this.psi = atan2(m.m02, -m.m12);
	}

	/**
	 * Convert <code>this</code> to a rotation 3x3 matrix.
	 * 
	 * @return
	 */
	public Matrix3d getRotationMatrix() {
		Matrix3d m = new Matrix3d();

		m.m00 = cos(psi) * cos(phi) - sin(psi) * cos(theta) * sin(phi);
		m.m01 = -cos(psi) * sin(phi) - sin(psi) * cos(theta) * cos(phi);
		m.m02 = sin(psi) * sin(theta);
		m.m10 = sin(psi) * cos(phi) + cos(psi) * cos(theta) * sin(phi);
		m.m11 = -sin(psi) * sin(phi) + cos(psi) * cos(theta) * cos(phi);
		m.m12 = -cos(psi) * sin(theta);
		m.m20 = sin(theta) * sin(phi);
		m.m21 = sin(theta) * cos(phi);
		m.m22 = cos(theta);

		return m;
	}

	/**
	 * Convert <code>this</code> to a rotation 4x4 matrix.
	 * 
	 * Translation component of the result is set to zero.
	 * 
	 * @return
	 */
	public Matrix4d getRotationMatrixHomogen() {
		Matrix4d m = new Matrix4d();

		m.setIdentity();
		m.setRotation(getRotationMatrix());

		return m;
	}

	public boolean epsilonEquals(EulerAngles ea, double epsilon) {
		if (!this.getRotationMatrix().epsilonEquals(ea.getRotationMatrix(), epsilon))
			return false;

		return true;
	}

	public String toString() {
		return "(" + String.format(Util.format, this.psi) + ", " + String.format(Util.format, this.theta) + ", "
				+ String.format(Util.format, this.phi) + ")";
	}
}
