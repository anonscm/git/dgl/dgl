package org.lgmt.dgl.vecmath;

import static java.lang.Math.asin;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class TBAnglesZYXi extends TBAngles {
	private static final long serialVersionUID = 6789186630520589853L;

	public TBAnglesZYXi(double alpha, double beta, double gamma) {
		super(alpha, beta, gamma);
		this.order = ORDER.ZYX;
		this.convention = CONVENTION.INTRINSIC;
	}

	public TBAnglesZYXi(Matrix3d m) {
		super(m);
		this.alpha = atan2(m.m10, m.m00);
		this.beta = asin(-m.m20);
		this.gamma = atan2(m.m21, m.m22);
		this.order = ORDER.ZYX;
		this.convention = CONVENTION.INTRINSIC;
	}

	@Override
	public Matrix3d getRotationMatrix() {
		Matrix3d m = new Matrix3d();

		m.m00 = cos(alpha) * cos(beta);
		m.m01 = cos(alpha) * sin(beta) * sin(gamma) - cos(gamma) * sin(alpha);
		m.m02 = sin(alpha) * sin(gamma) + cos(alpha) * cos(gamma) * sin(beta);
		m.m10 = cos(beta) * sin(alpha);
		m.m11 = cos(alpha) * cos(gamma) + sin(alpha) * sin(beta) * sin(gamma);
		m.m12 = cos(gamma) * sin(alpha) * sin(beta) - cos(alpha) * sin(gamma);
		m.m20 = -sin(beta);
		m.m21 = cos(beta) * sin(gamma);
		m.m22 = cos(beta) * cos(gamma);

		return m;
	}

}
