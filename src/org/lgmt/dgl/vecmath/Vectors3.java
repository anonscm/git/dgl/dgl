/*
** Vectors.java
** Copyright 2022 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/

package org.lgmt.dgl.vecmath;

import org.lgmt.dgl.commons.Util;

/**
 * A toolbox class gathering all static vectors methods
 * 
 * @author redonnet
 *
 */
public class Vectors3 {

	/**
	 * Returns v1 + v2 in a new vector.
	 * 
	 * @param v1 the first vector
	 * @param v2 the second vector
	 * @return
	 */
	public static final Vector3d addV(Vector3d v1, Vector3d v2) {
		return new Vector3d(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
	}

	/**
	 * Returns v1 - v2 in a new vector.
	 * 
	 * @param v1 the first vector
	 * @param v2 the second vector
	 * @return
	 */
	public static final Vector3d subV(Vector3d v1, Vector3d v2) {
		return new Vector3d(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
	}

	/**
	 * Returns -v1 in a new vector.
	 * 
	 * @param v1 the vector
	 * @return
	 */
	public static final Vector3d negateV(Vector3d v1) {
		return new Vector3d(-v1.x, -v1.y, -v1.z);
	}

	/**
	 * Returns s*v1 in a new vector.
	 * 
	 * @param s  the scalar value
	 * @param v1 the source vector
	 * @return
	 */
	public static final Vector3d scaleV(double s, Vector3d v1) {
		return new Vector3d(s * v1.x, s * v1.y, s * v1.z);
	}

	/**
	 * Matrix vector product
	 * 
	 * <p>
	 * Same result than {@link Matrix3d#transform(Tuple3d t)}.
	 * 
	 * @param m a 3by3 matrix
	 * @param a a Vector3d
	 * @return b = m*a
	 */
	public static final Vector3d mulMV(Matrix3d m, Vector3d a) {
		return new Vector3d(m.m00 * a.x + m.m01 * a.y + m.m02 * a.z, m.m10 * a.x + m.m11 * a.y + m.m12 * a.z,
				m.m20 * a.x + m.m21 * a.y + m.m22 * a.z);
	}

	/**
	 * Returns v1 x v2 in a new vector.
	 * 
	 * @param v1 the first vector
	 * @param v2 the second vector
	 * @return
	 */
	public static final Vector3d crossV(Vector3d v1, Vector3d v2) {
		Vector3d v = new Vector3d();
		v.cross(v1, v2);
		return v;
	}

	/**
	 * Returns v1 . v2 in a new vector.
	 * 
	 * @param v1 the first vector
	 * @param v2 the second vector
	 * @return
	 */
	public static final double dotV(Vector3d v1, Vector3d v2) {
		Vector3d v = new Vector3d(v1);
		return v.dot(v2);
	}

	/**
	 * Returns the angle between two vectors (in radians).
	 * The returned value is constrained to the range [0,PI].
	 * 
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static final double angleV(Vector3d v1, Vector3d v2) {
		double vDot = v1.dot(v2) / (v1.length() * v2.length());
		if (vDot < -1.0)
			vDot = -1.0;
		if (vDot > 1.0)
			vDot = 1.0;
		return ((double) (Math.acos(vDot)));
	}

	/**
	 * Returns the rotation matrix that align vector source to vector target
	 * 
	 * Do not work if the two vectors point to exact opposite directions.
	 * 
	 * Translation and scale are set to null.
	 * 
	 * @param source the first vector
	 * @param target the second vector
	 * @return
	 */
	public static final Matrix3d getAlignMat3(Vector3d source, Vector3d target) {
		Vector3d v1 = new Vector3d(source);
		v1.normalize();
		Vector3d v2 = new Vector3d(target);
		v2.normalize();
		Matrix3d m = new Matrix3d();
		m.setIdentity();
		Vector3d v = Vectors3.crossV(v1, v2);
		double c = Vectors3.dotV(v1, v2);

		if (Math.abs(1 - c) < Util.PREC12) {
			m.mul(-1.0);
			return m;
//			throw new IllegalArgumentException("Vectors are pointing to exact opposite directions.");
//			// FIXME: Return the correct transformation anyway
		}

		Matrix3d vx = new Matrix3d(0.0, -v.z, v.y, v.z, 0.0, -v.x, -v.y, v.x, 0.0);
		Matrix3d vx2 = new Matrix3d(vx);
		vx2.mul(vx);
		vx2.mul(1 / (1 + c));
		m.setIdentity();
		m.add(vx);
		m.add(vx2);

		return m;
	}

	/**
	 * Returns the rotation matrix that align vector source to vector target
	 * 
	 * Do not work if the two vectors point to exact opposite directions.
	 * 
	 * Translation and scale are set to null.
	 * 
	 * @param source the first vector
	 * @param target the second vector
	 * @return
	 */
	public static final Matrix4d getAlignMat4(Vector3d source, Vector3d target) {
		Matrix4d m = new Matrix4d();
		m.setRotation(getAlignMat3(source, target));

		return m;
	}

	/**
	 * Returns <b>true</b> if vectors v1 and v2 are aligned (colinear) within the
	 * epsilon value angular accuracy.
	 * 
	 * Returns <b>true</b> if vectors are colinear but in opposite directions.
	 * 
	 * Return <b>false</b> if v1 or v2 equals zero.
	 * 
	 * @param v1      the first vector to test
	 * @param v2      the second vector to test
	 * @param epsilon the angular accuracy (in radians)
	 * @return <b>true</b> if v1 and v2 are aligned
	 */
	public static boolean epsilonAligned(Vector3d v1, Vector3d v2, double epsilon) {
		double dot = Math.abs(v1.dot(v2));
		if (dot < Util.PREC12) // v1 and v2 are perpendicular or v1 is zero or v2 is zero
			return false;

		if (Math.acos(dot / (v1.length() * v2.length())) < epsilon)
			return true;

		return false;
	}
}
