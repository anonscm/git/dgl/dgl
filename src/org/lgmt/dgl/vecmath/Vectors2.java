package org.lgmt.dgl.vecmath;

public class Vectors2 {

	/**
	 * Returns the angle between two vectors (in radians).
	 * The returned value is constrained to the range [0,PI].
	 * 
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static final double angleV(Vector2d v1, Vector2d v2) {
		double vDot = v1.dot(v2) / (v1.length() * v2.length());
		if (vDot < -1.0)
			vDot = -1.0;
		if (vDot > 1.0)
			vDot = 1.0;
		return ((double) (Math.acos(vDot)));
	}

}
