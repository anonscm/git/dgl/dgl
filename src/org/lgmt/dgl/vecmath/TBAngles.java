package org.lgmt.dgl.vecmath;

import org.lgmt.dgl.commons.Util;

/**
 * A class to represent Tait–Bryan angles
 * 
 * Tait-Bryan angles are used to define generic rotations around X, Y and Z
 * axes.
 * 
 * The order of rotation depends on the convention chosen (XYZ and ZYX are the
 * most commonly used). Angles can be defined according to an intrinsic or
 * extrinsic convention:
 * <ol>
 * <li>intrinsic: each rotation is based on the axes resulting from the previous
 * rotation</li>
 * <li>extrinsic: each rotation is based on the axes of the initial frame of
 * reference</li>
 * </ol>
 */
public abstract class TBAngles implements java.io.Serializable {
	private static final long serialVersionUID = -8117923351110379623L;

	protected static enum ORDER {
		XYZ, XZY, YXZ, YZX, ZXY, ZYX
	}

	protected static enum CONVENTION {
		INTRINSIC, EXTRINSIC
	}

	public ORDER order;
	public CONVENTION convention;
	public double alpha;
	public double beta;
	public double gamma;

	/**
	 * Creates a new Tait–Bryan angles.
	 * 
	 * @param alpha
	 * @param beta
	 * @param gamma
	 */
	public TBAngles(double alpha, double beta, double gamma) {
		super();
		this.alpha = Util.angleToInterval(alpha, 0.0);
		this.beta = Util.angleToInterval(beta, 0.0);
		this.gamma = Util.angleToInterval(gamma, 0.0);
	}

	public TBAngles(Matrix3d m) {
		super();
	}

	/**
	 * Convert <code>this</code> to a rotation 3x3 matrix.
	 * 
	 * @return
	 */
	public abstract Matrix3d getRotationMatrix();

	/**
	 * Convert <code>this</code> to a rotation 4x4 matrix.
	 * 
	 * Translation component of the result is set to zero.
	 * 
	 * @return
	 */
	public Matrix4d getRotationMatrixHomogen() {
		Matrix4d m = new Matrix4d();

		m.setIdentity();
		m.setRotation(getRotationMatrix());

		return m;
	}

	public boolean epsilonEquals(TBAngles tba, double epsilon) {
		if (!this.getRotationMatrix().epsilonEquals(tba.getRotationMatrix(), epsilon))
			return false;

		return true;
	}

	public String toString() {
		return "(" + String.format(Util.format, this.alpha) + ", " + String.format(Util.format, this.beta) + ", "
				+ String.format(Util.format, this.gamma) + ")";
	}
}
