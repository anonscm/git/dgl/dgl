/*
** UnivarFunction.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.functions;

import org.lgmt.dgl.commons.Util;


/**
 * Une fonction d'une seule variable.
 * 
 * @author redonnet
 *
 */
public class UnivarFunction implements UnivarEvaluable{
	protected UnivarEvaluable f;
	
	/**
	 * Construit une nouvelle fonction à partir de son expression.
	 * 
	 * @param expression l'expression à utiliser
	 */
	public UnivarFunction(UnivarEvaluable expression){
		this.f = expression;
	}
	
	/**
	 * Evalue la fonction.
	 * 
	 * @param u
	 * @return la valeur de la fonction en u
	 */
	@Override
	public double eval(double u){
		return f.eval(u);
	}
	
	/**
	 * Différencie la fonction par rapport à u.
	 * 
	 * @param u
	 * @return la valeur de la dérivée par rapport à u
	 */
	public double diff(double u){
		double h = Util.PREC6;
		return (eval(u+h)-eval(u-h))/(2*h);
	}
	
	/**
	 * Différencie à droite la fonction par rapport à u.
	 * 
	 * @param u
	 * @return la valeur de la dérivée par rapport à u en (u,v)
	 */
	public double diffuFw(double u) {
		final double h = 1.0E-6;
		return (eval(u + 2 * h) - eval(u)) / (2 * h);
	}

	/**
	 * Différencie à gauche la fonction par rapport à u.
	 * 
	 * @param u
	 * @return la valeur de la dérivée par rapport à u en (u,v)
	 */
	public double diffuBw(double u) {
		final double h = 1.0E-6;
		return (eval(u) - eval(u - 2 * h)) / (2 * h);
	}
}
