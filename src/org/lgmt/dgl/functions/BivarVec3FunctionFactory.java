/*
** BivarVec3FunctionFactory.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.functions;


/**
 * Une fabrique de fonctions vectorielles de deux variables.
 * 
 * @author redonnet
 *
 */
public class BivarVec3FunctionFactory {
	/**
	 * Contruit une nouvelle fonction vectorielle à partir de 3 expressions.
	 * 
	 * @param evalx l'expression à utiliser pour la coordonnée en x
	 * @param evaly l'expression à utiliser pour la coordonnée en y
	 * @param evalz l'expression à utiliser pour la coordonnée en z
	 * @return la nouvelle fonction vectorielle
	 */
	public BivarVec3Function newVec3Function(BivarEvaluable evalx, BivarEvaluable evaly,
			BivarEvaluable evalz) {
		return new BivarVec3Function(evalx, evaly, evalz);
	}

	/**
	 * Contruit une nouvelle fonction vectorielle en dérivant en u une autre
	 * fonction vectorielle.
	 * 
	 * @param v3f la fonction vectorielle à utiliser
	 * @return la nouvelle fonction vectorielle
	 */
	public BivarVec3Function newDiffuVec3Function(final BivarVec3Function v3f) {
		return new BivarVec3Function(
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffux(u, v);
					}
				}, 
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffuy(u, v);
					}
				}, 
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffuz(u, v);
					}
				}
		);
	}

	/**
	 * Contruit une nouvelle fonction vectorielle en dérivant en u à droite une autre
	 * fonction vectorielle.
	 * 
	 * @param v3f la fonction vectorielle à utiliser
	 * @return la nouvelle fonction vectorielle
	 */
	public BivarVec3Function newDiffuFwVec3Function(final BivarVec3Function v3f) {
		return new BivarVec3Function(
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffuxFw(u, v);
					}
				}, 
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffuyFw(u, v);
					}
				}, 
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffuzFw(u, v);
					}
				}
		);
	}

	/**
	 * Contruit une nouvelle fonction vectorielle en dérivant en u à gauche une autre
	 * fonction vectorielle.
	 * 
	 * @param v3f la fonction vectorielle à utiliser
	 * @return la nouvelle fonction vectorielle
	 */
	public BivarVec3Function newDiffuBwVec3Function(final BivarVec3Function v3f) {
		return new BivarVec3Function(
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffuxBw(u, v);
					}
				}, 
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffuyBw(u, v);
					}
				}, 
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffuzBw(u, v);
					}
				}
		);
	}

	/**
	 * Contruit une nouvelle fonction vectorielle en dérivant en v une autre
	 * fonction vectorielle.
	 * 
	 * @param v3f la fonction vectorielle à utiliser
	 * @return la nouvelle fonction vectorielle
	 */
	public BivarVec3Function newDiffvVec3Function(final BivarVec3Function v3f) {
		return new BivarVec3Function(
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffvx(u, v);
					}
				}, 
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffvy(u, v);
					}
				}, 
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffvz(u, v);
					}
				}
		);
	}

	/**
	 * Contruit une nouvelle fonction vectorielle en dérivant en v à droite une autre
	 * fonction vectorielle.
	 * 
	 * @param v3f la fonction vectorielle à utiliser
	 * @return la nouvelle fonction vectorielle
	 */
	public BivarVec3Function newDiffvFwVec3Function(final BivarVec3Function v3f) {
		return new BivarVec3Function(
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffvxFw(u, v);
					}
				}, 
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffvyFw(u, v);
					}
				}, 
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffvzFw(u, v);
					}
				}
		);
	}
	/**
	 * Contruit une nouvelle fonction vectorielle en dérivant en v à gauche une autre
	 * fonction vectorielle.
	 * 
	 * @param v3f la fonction vectorielle à utiliser
	 * @return la nouvelle fonction vectorielle
	 */
	public BivarVec3Function newDiffvBwVec3Function(final BivarVec3Function v3f) {
		return new BivarVec3Function(
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffvxBw(u, v);
					}
				}, 
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffvyBw(u, v);
					}
				}, 
				new BivarEvaluable(){
					@Override
					public double eval(double u, double v){
						return v3f.diffvzBw(u, v);
					}
				}
		);
	}
}
