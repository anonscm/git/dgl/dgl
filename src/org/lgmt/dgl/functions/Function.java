package org.lgmt.dgl.functions;
import java.util.Arrays;

import org.lgmt.dgl.commons.InvalidArraySizeException;

public class Function implements IFunction{
	IFunction f;
	int size;
	
	public Function(int size, IFunction f) {
		super();
		this.size = size;
		this.f = f;
	}

	public int getSize() {
		return size;
	}

	@Override
	public double eval(double... x) {
		if(x.length < size)
			throw new InvalidArraySizeException(x.length);
		return f.eval(x);
	}
	
	/*
	 * i: index of the variable to be derivated. Begins at 0
	 */
	public double diff(int i, double... x) {
		if(x.length < size)
			throw new InvalidArraySizeException(x.length);
		final double h = 1.0E-6;
		
		int n = x.length;
		
		double[] inf = Arrays.copyOf(x, n);
		inf[i] = inf[i]-h; 
		double[] sup = Arrays.copyOf(x, n);;
		sup[i] = sup[i]+h;
		
		return (f.eval(sup) - f.eval(inf))/(2*h);
	}
	
}
