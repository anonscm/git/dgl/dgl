/*
** MultivarFunction.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.functions;

import org.lgmt.dgl.commons.InvalidArraySizeException;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.vecmath.GVector;


/**
 * Une fonction de plusieurs variables.
 * 
 * @author redonnet
 *
 */
public class MultivarFunction implements MultivarEvaluable {
	private MultivarEvaluable f;
	private int nbvar;

	/**
	 * Construit une nouvelle fonction à partir de son expression.
	 * 
	 * @param expression l'expression à utiliser
	 */
	public MultivarFunction(int nbvar, MultivarEvaluable expression) {
		this.nbvar = nbvar;
		this.f = expression;
	}

	/**
	 * Evalue la fonction.
	 * 
	 * @param var
	 * @return la valeur de la fonction
	 */
	@Override
	public double eval(GVector var) {
		if(var.getSize() != nbvar)
			throw new InvalidArraySizeException(var.getSize());
		return f.eval(var);
	}

	public int getNbvar() {
		return nbvar;
	}

	/**
	 * Différencie la fonction par rapport à une variable.
	 * 
	 * La variable de différentiation est définie par l'indice <b>index</b>
	 * 
	 * @param var
	 * @param index l'indice de la variable de différenciation
	 * @return la valeur de la dérivée. 
	 */
	public double diff(GVector var, int index) {
		double h = Util.PREC6;

		GVector deltaSup = new GVector(var);
		deltaSup.setElement(index, var.getElement(index) + h);
		GVector deltaInf = new GVector(var);
		deltaInf.setElement(index, var.getElement(index) - h);
		return (eval(deltaSup) - eval(deltaInf)) / (2 * h);
	}

	/**
	 * Calcule le gradient de la fonction
	 * 
	 * @return une fonction vectorielle
	 */
	public MultivarVectorialFunction gradient(){
		MultivarVectorialFunction g;
		g = new MultivarVectorialFunction(nbvar, nbvar);
		
		final MultivarFunction function = this;
		
		for(int i=0 ; i<nbvar ; i++){
			final int j = i;
			g.setFunction(j, new MultivarFunction(nbvar, new MultivarEvaluable(){
				@Override
				public double eval(GVector var){
					return function.diff(var, j);
				}
			}));
		}
		
		return g;
	}
	
	/**
	 * Calcule la valeur du gradient en un point
	 * 
	 * @param var
	 * @return un vecteur
	 */
	public GVector evalGradient(GVector var){
		if(var.getSize() != nbvar)
			throw new InvalidArraySizeException(var.getSize());
		
		GVector grad = new GVector(var.getSize());
		
		for(int i=0 ; i<nbvar ; i++){
			grad.setElement(i, this.diff(var, i));
		}
		
		return grad;
	}
}
