/*
** UnivarVec3Function.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.functions;

import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;

/**
 * Une fonction vectorielle (de taille 3) d'une seule variable.
 * 
 * @navassoc - fx,fy,fz 3 UnivarFunction
 * @author redonnet
 *
 */
public class UnivarVec3Function implements UnivarVec3Evaluable {
	private UnivarFunction fx;
	private UnivarFunction fy;
	private UnivarFunction fz;

	private Cache cache = null;

	/**
	 * Contruit une nouvelle fonction vectorielle à partir de 3 expressions.
	 * 
	 * @param evalx l'expression à utiliser pour la coordonnée en x
	 * @param evaly l'expression à utiliser pour la coordonnée en y
	 * @param evalz l'expression à utiliser pour la coordonnée en z
	 */
	public UnivarVec3Function(UnivarEvaluable evalx, UnivarEvaluable evaly, UnivarEvaluable evalz) {
		this.fx = new UnivarFunction(evalx);
		this.fy = new UnivarFunction(evaly);
		this.fz = new UnivarFunction(evalz);
	}

	/**
	 * Contruit une nouvelle fonction vectorielle à partir de 3 fonctions.
	 * 
	 * @param fx la fonction à utiliser pour la coordonnée en x
	 * @param fy la fonction à utiliser pour la coordonnée en y
	 * @param fz la fonction à utiliser pour la coordonnée en z
	 */
	public UnivarVec3Function(UnivarFunction fx, UnivarFunction fy, UnivarFunction fz) {
		this.fx = fx;
		this.fy = fy;
		this.fz = fz;
	}

	/**
	 * Evalue la fonction vectorielle.
	 * 
	 * @param point un point pour stocker le résultat
	 * @param u
	 */
	@Override
	public void eval(Point3d point, double u) {
		point.x = fx.eval(u);
		point.y = fy.eval(u);
		point.z = fz.eval(u);
	}

	/**
	 * Evalue la fonction vectorielle.
	 * 
	 * @param u
	 * @return un nouveau point contenant le résultat
	 */
	@Override
	public Point3d eval(double u) {
		Point3d p = new Point3d();
		this.eval(p, u);
		return p;
	}

	/**
	 * Evalue la dérivée de la fonction vectorielle.
	 * 
	 * @param point un point pour stocker le résultat
	 * @param u
	 */
	public void diff(Point3d point, double u) {
		point.x = fx.diff(u);
		point.y = fy.diff(u);
		point.z = fz.diff(u);
	}

	/**
	 * Evalue la dérivée de la fonction vectorielle.
	 * 
	 * @param ptx la coordonnée en x du résultat
	 * @param pty la coordonnée en y du résultat
	 * @param ptz la coordonnée en z du résultat
	 * @param u
	 */
	public void diff(double ptx, double pty, double ptz, double u) {
		ptx = fx.diff(u);
		pty = fy.diff(u);
		ptz = fz.diff(u);
	}

	/**
	 * Evalue la dérivée de la fonction en x de la fonction vectorielle.
	 * 
	 * @param u
	 * @return la valeur de la dérivée
	 */
	public double diffx(double u) {
		return (fx.diff(u));
	}

	/**
	 * Evalue la dérivée de la fonction en y de la fonction vectorielle.
	 * 
	 * @param u
	 * @return la valeur de la dérivée
	 */
	public double diffy(double u) {
		return (fy.diff(u));
	}

	/**
	 * Evalue la dérivée de la fonction en z de la fonction vectorielle.
	 * 
	 * @param u
	 * @return la valeur de la dérivée
	 */
	public double diffz(double u) {
		return (fz.diff(u));
	}

	public UnivarFunction getFx() {
		return fx;
	}

	public UnivarFunction getFy() {
		return fy;
	}

	public UnivarFunction getFz() {
		return fz;
	}

	public UnivarVec3Function transformClone(Matrix4d m) {
		this.cache = new Cache();
		UnivarVec3Function newV3f = new UnivarVec3Function(new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				Point3d p = cache.getPoint(u);
				return m.m00 * p.x + m.m01 * p.y + m.m02 * p.z + m.m03;
			}
		}, new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				Point3d p = cache.getPoint(u);
				return m.m10 * p.x + m.m11 * p.y + m.m12 * p.z + m.m13;
			}
		}, new UnivarEvaluable() {
			@Override
			public double eval(double u) {
				Point3d p = cache.getPoint(u);
				return m.m20 * p.x + m.m21 * p.y + m.m22 * p.z + m.m23;
			}
		});
		return newV3f;
	}

	private class Cache {
		private double u;
		private Point3d point = new Point3d();

		private Cache() {
			/* initialisation pour éviter une incohérence fortuite lors du premier calcul */
			this.u = 0.0; // Erreur si pas evaluable en 0.0 ?
			eval(point, u);
		}

		public Point3d getPoint(double u) {
			if (u == this.u)
				return point;
			this.u = u;
			eval(point, u);

			return point;
		}
	}
}
