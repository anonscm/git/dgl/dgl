package org.lgmt.dgl.functions;

public interface IFunction {
	public double eval(double... vars);
}
