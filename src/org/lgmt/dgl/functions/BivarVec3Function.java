/*
** BivarVec3Function.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.functions;

import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;

/**
 * Une fonction vectorielle (de taille 3) de deux variables.
 * 
 * @navassoc - fx,fy,fz 3 BivarFunction
 * 
 * @author redonnet
 *
 */
public class BivarVec3Function implements BivarVec3Evaluable {
	private BivarFunction fx;
	private BivarFunction fy;
	private BivarFunction fz;

	private Cache cache = null;

	/**
	 * Contruit une nouvelle fonction vectorielle à partir de 3 expressions.
	 * 
	 * @param evalx l'expression à utiliser pour la coordonnée en x
	 * @param evaly l'expression à utiliser pour la coordonnée en y
	 * @param evalz l'expression à utiliser pour la coordonnée en z
	 */
	public BivarVec3Function(BivarEvaluable evalx, BivarEvaluable evaly, BivarEvaluable evalz) {
		this.fx = new BivarFunction(evalx);
		this.fy = new BivarFunction(evaly);
		this.fz = new BivarFunction(evalz);
	}

	/**
	 * Contruit une nouvelle fonction vectorielle à partir de 3 fonctions.
	 * 
	 * @param fx la fonction à utiliser pour la coordonnée en x
	 * @param fy la fonction à utiliser pour la coordonnée en y
	 * @param fz la fonction à utiliser pour la coordonnée en z
	 */
	public BivarVec3Function(BivarFunction fx, BivarFunction fy, BivarFunction fz) {
		this.fx = fx;
		this.fy = fy;
		this.fz = fz;
	}

	/**
	 * Contruit une nouvelle fonction vectorielle à partir d'une autre fonction
	 * vectorielle et d'une matrice de transformation.
	 * 
	 * @param f la fonction vectorielle à utiliser
	 * @param t la matrice de transformation à utiliser
	 */
	public BivarVec3Function(final BivarVec3Function f, final Matrix4d t) {
		this.fx = new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return t.m00 * f.fx.eval(u, v) + t.m01 * f.fy.eval(u, v) + t.m02 * f.fz.eval(u, v) + t.m03;
			}
		});
		this.fy = new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return t.m10 * f.fx.eval(u, v) + t.m11 * f.fy.eval(u, v) + t.m12 * f.fz.eval(u, v) + t.m13;
			}
		});
		this.fz = new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return t.m20 * f.fx.eval(u, v) + t.m21 * f.fy.eval(u, v) + t.m22 * f.fz.eval(u, v) + t.m23;
			}
		});
	}

	/**
	 * Evalue la fonction vectorielle.
	 * 
	 * @param point un point pour stocker le résultat
	 * @param u
	 * @param v
	 */
	@Override
	public void eval(Point3d point, double u, double v) {
		point.x = fx.eval(u, v);
		point.y = fy.eval(u, v);
		point.z = fz.eval(u, v);
	}

	/**
	 * Evalue la fonction vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return un nouveau point contenant le résultat
	 */
	@Override
	public Point3d eval(double u, double v) {
		Point3d p = new Point3d();
		this.eval(p, u, v);
		return p;
	}

	/**
	 * Evalue la dérivée en u de la fonction vectorielle. Le résultat est donné sous
	 * la forme d'un vecteur non normé.
	 * 
	 * @param vector un vecteur pour stocker le résultat
	 * @param u
	 * @param v
	 */
	public void diffu(Vector3d vector, double u, double v) {
		vector.x = fx.diffu(u, v);
		vector.y = fy.diffu(u, v);
		vector.z = fz.diffu(u, v);
	}

	/**
	 * Evalue à droite la dérivée en u de la fonction vectorielle. Le résultat est
	 * donné sous la forme d'un vecteur non normé.
	 * 
	 * @param vector un vecteur pour stocker le résultat
	 * @param u
	 * @param v
	 */
	public void diffuFw(Vector3d vector, double u, double v) {
		vector.x = fx.diffuFw(u, v);
		vector.y = fy.diffuFw(u, v);
		vector.z = fz.diffuFw(u, v);
	}

	/**
	 * Evalue à gauche la dérivée en u de la fonction vectorielle. Le résultat est
	 * donné sous la forme d'un vecteur non normé.
	 * 
	 * @param vector un vecteur pour stocker le résultat
	 * @param u
	 * @param v
	 */
	public void diffuBw(Vector3d vector, double u, double v) {
		vector.x = fx.diffuBw(u, v);
		vector.y = fy.diffuBw(u, v);
		vector.z = fz.diffuBw(u, v);
	}

	/**
	 * Evalue la dérivée en u de la fonction vectorielle.
	 * 
	 * @param ptx la coordonnée en x du résultat
	 * @param pty la coordonnée en y du résultat
	 * @param ptz la coordonnée en z du résultat
	 * @param u
	 * @param v
	 */
	public void diffu(double ptx, double pty, double ptz, double u, double v) {
		ptx = fx.diffu(u, v);
		pty = fy.diffu(u, v);
		ptz = fz.diffu(u, v);
	}

	/**
	 * Evalue à droite la dérivée en u de la fonction vectorielle.
	 * 
	 * @param ptx la coordonnée en x du résultat
	 * @param pty la coordonnée en y du résultat
	 * @param ptz la coordonnée en z du résultat
	 * @param u
	 * @param v
	 */
	public void diffuFw(double ptx, double pty, double ptz, double u, double v) {
		ptx = fx.diffuFw(u, v);
		pty = fy.diffuFw(u, v);
		ptz = fz.diffuFw(u, v);
	}

	/**
	 * Evalue à gauche la dérivée en u de la fonction vectorielle.
	 * 
	 * @param ptx la coordonnée en x du résultat
	 * @param pty la coordonnée en y du résultat
	 * @param ptz la coordonnée en z du résultat
	 * @param u
	 * @param v
	 */
	public void diffuBw(double ptx, double pty, double ptz, double u, double v) {
		ptx = fx.diffuBw(u, v);
		pty = fy.diffuBw(u, v);
		ptz = fz.diffuBw(u, v);
	}

	/**
	 * Evalue la dérivée en u de la fonction en x de la fonction vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffux(double u, double v) {
		return (fx.diffu(u, v));
	}

	/**
	 * Evalue à droite la dérivée en u de la fonction en x de la fonction
	 * vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffuxFw(double u, double v) {
		return (fx.diffuFw(u, v));
	}

	/**
	 * Evalue à gauche la dérivée en u de la fonction en x de la fonction
	 * vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffuxBw(double u, double v) {
		return (fx.diffuBw(u, v));
	}

	/**
	 * Evalue la dérivée en u de la fonction en y de la fonction vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffuy(double u, double v) {
		return (fy.diffu(u, v));
	}

	/**
	 * Evalue à droite la dérivée en u de la fonction en y de la fonction
	 * vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffuyFw(double u, double v) {
		return (fy.diffuFw(u, v));
	}

	/**
	 * Evalue à gauche la dérivée en u de la fonction en y de la fonction
	 * vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffuyBw(double u, double v) {
		return (fy.diffuBw(u, v));
	}

	/**
	 * Evalue la dérivée en u de la fonction en z de la fonction vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffuz(double u, double v) {
		return (fz.diffu(u, v));
	}

	/**
	 * Evalue à droite la dérivée en u de la fonction en z de la fonction
	 * vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffuzFw(double u, double v) {
		return (fz.diffuFw(u, v));
	}

	/**
	 * Evalue à gauche la dérivée en u de la fonction en z de la fonction
	 * vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffuzBw(double u, double v) {
		return (fz.diffuBw(u, v));
	}

	/**
	 * Evalue la dérivée en v de la fonction vectorielle.Le résultat est donné sous
	 * la forme d'un vecteur non normé.
	 * 
	 * @param vector un vecteur pour stocker le résultat
	 * @param u
	 * @param v
	 */
	public void diffv(Vector3d vector, double u, double v) {
		vector.x = fx.diffv(u, v);
		vector.y = fy.diffv(u, v);
		vector.z = fz.diffv(u, v);
	}

	/**
	 * Evalue la dérivée en v à droite de la fonction vectorielle.Le résultat est
	 * donné sous la forme d'un vecteur non normé.
	 * 
	 * @param vector un vecteur pour stocker le résultat
	 * @param u
	 * @param v
	 */
	public void diffvFw(Vector3d vector, double u, double v) {
		vector.x = fx.diffvFw(u, v);
		vector.y = fy.diffvFw(u, v);
		vector.z = fz.diffvFw(u, v);
	}

	/**
	 * Evalue la dérivée en v à gauche de la fonction vectorielle.Le résultat est
	 * donné sous la forme d'un vecteur non normé.
	 * 
	 * @param vector un vecteur pour stocker le résultat
	 * @param u
	 * @param v
	 */
	public void diffvBw(Vector3d vector, double u, double v) {
		vector.x = fx.diffvBw(u, v);
		vector.y = fy.diffvBw(u, v);
		vector.z = fz.diffvBw(u, v);
	}

	/**
	 * Evalue la dérivée en v de la fonction vectorielle.
	 * 
	 * @param ptx la coordonnée en x du résultat
	 * @param pty la coordonnée en y du résultat
	 * @param ptz la coordonnée en z du résultat
	 * @param u
	 * @param v
	 */
	public void diffv(double ptx, double pty, double ptz, double u, double v) {
		ptx = fx.diffv(u, v);
		pty = fy.diffv(u, v);
		ptz = fz.diffv(u, v);
	}

	/**
	 * Evalue la dérivée en v à droite de la fonction vectorielle.
	 * 
	 * @param ptx la coordonnée en x du résultat
	 * @param pty la coordonnée en y du résultat
	 * @param ptz la coordonnée en z du résultat
	 * @param u
	 * @param v
	 */
	public void diffvFw(double ptx, double pty, double ptz, double u, double v) {
		ptx = fx.diffvFw(u, v);
		pty = fy.diffvFw(u, v);
		ptz = fz.diffvFw(u, v);
	}

	/**
	 * Evalue la dérivée en v à gauche de la fonction vectorielle.
	 * 
	 * @param ptx la coordonnée en x du résultat
	 * @param pty la coordonnée en y du résultat
	 * @param ptz la coordonnée en z du résultat
	 * @param u
	 * @param v
	 */
	public void diffvBw(double ptx, double pty, double ptz, double u, double v) {
		ptx = fx.diffvBw(u, v);
		pty = fy.diffvBw(u, v);
		ptz = fz.diffvBw(u, v);
	}

	/**
	 * Evalue la dérivée en v de la fonction en x de la fonction vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffvx(double u, double v) {
		return (fx.diffv(u, v));
	}

	/**
	 * Evalue la dérivée en v à droite de la fonction en x de la fonction
	 * vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffvxFw(double u, double v) {
		return (fx.diffvFw(u, v));
	}

	/**
	 * Evalue la dérivée en v à gauche de la fonction en x de la fonction
	 * vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffvxBw(double u, double v) {
		return (fx.diffvBw(u, v));
	}

	/**
	 * Evalue la dérivée en v de la fonction en y de la fonction vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffvy(double u, double v) {
		return (fy.diffv(u, v));
	}

	/**
	 * Evalue la dérivée en v à droite de la fonction en y de la fonction
	 * vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffvyFw(double u, double v) {
		return (fy.diffvFw(u, v));
	}

	/**
	 * Evalue la dérivée en v à gauche de la fonction en y de la fonction
	 * vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffvyBw(double u, double v) {
		return (fy.diffvBw(u, v));
	}

	/**
	 * Evalue la dérivée en v de la fonction en z de la fonction vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffvz(double u, double v) {
		return (fz.diffv(u, v));
	}

	/**
	 * Evalue la dérivée en v à droite de la fonction en z de la fonction
	 * vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffvzFw(double u, double v) {
		return (fz.diffvFw(u, v));
	}

	/**
	 * Evalue la dérivée en v à gauche de la fonction en z de la fonction
	 * vectorielle.
	 * 
	 * @param u
	 * @param v
	 * @return la valeur de la dérivée
	 */
	public double diffvzBw(double u, double v) {
		return (fz.diffvBw(u, v));
	}

	/**
	 * 
	 * @return la fonction en x
	 */
	public BivarFunction getFx() {
		return fx;
	}

	/**
	 * 
	 * @return la fonction en y
	 */
	public BivarFunction getFy() {
		return fy;
	}

	/**
	 * 
	 * @return la fonction en x
	 */
	public BivarFunction getFz() {
		return fz;
	}

	public BivarVec3Function transformClone(Matrix4d m) {
		this.cache = new Cache();
		BivarVec3Function newV3f = new BivarVec3Function(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Point3d p = cache.getPoint(u, v);
				return m.m00 * p.x + m.m01 * p.y + m.m02 * p.z + m.m03;
			}
		}, new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Point3d p = cache.getPoint(u, v);
				return m.m10 * p.x + m.m11 * p.y + m.m12 * p.z + m.m13;
			}
		}, new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Point3d p = cache.getPoint(u, v);
				return m.m20 * p.x + m.m21 * p.y + m.m22 * p.z + m.m23;
			}
		});
		return newV3f;
	}

	private class Cache {
		private double u, v;
		private Point3d point = new Point3d();

		private Cache() {
			/* initialisation pour éviter une incohérence fortuite lors du premier calcul */
			this.u = 0.0; // Erreur si pas evaluable en 0.0 ?
			this.v = 0.0;
			eval(point, u, v);
		}

		public Point3d getPoint(double u, double v) {
			if (u == this.u && v == this.v)
				return point;
			this.u = u;
			this.v = v;
			eval(point, u, v);

			return point;
		}
	}
}
