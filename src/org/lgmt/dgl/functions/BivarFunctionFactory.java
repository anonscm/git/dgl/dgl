/*
** BivarFunctionFactory.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.functions;


/**
 * Une fabrique de fonctions de deux variables.
 * 
 * @author redonnet
 *
 */
public class BivarFunctionFactory {
	
	/**
	 * Construit une nouvelle fonction à partir de son expression.
	 * 
	 * @param expression l'expression à utiliser
	 * @return la nouvelle fonction
	 */
	public BivarFunction newFunction(BivarEvaluable expression) {
		return new BivarFunction(expression);
	}

	/**
	 * Construit une nouvelle fonction à partir de la dérivée en u d'une autre
	 * fonction.
	 * 
	 * @param function la fonction à utiliser
	 * @return la nouvelle fonction
	 */
	public BivarFunction newDiffuFunction(final BivarFunction function) {
		return new BivarFunction(new BivarEvaluable(){
			@Override
			public double eval(double u, double v){
				return function.diffu(u, v);
			}
		});
	}

	/**
	 * Construit une nouvelle fonction à partir de la dérivée en u à droite d'une autre
	 * fonction.
	 * 
	 * @param function la fonction à utiliser
	 * @return la nouvelle fonction
	 */
	public BivarFunction newDiffuFwFunction(final BivarFunction function) {
		return new BivarFunction(new BivarEvaluable(){
			@Override
			public double eval(double u, double v){
				return function.diffuFw(u, v);
			}
		});
	}

	/**
	 * Construit une nouvelle fonction à partir de la dérivée en u à gauche d'une autre
	 * fonction.
	 * 
	 * @param function la fonction à utiliser
	 * @return la nouvelle fonction
	 */
	public BivarFunction newDiffuBwFunction(final BivarFunction function) {
		return new BivarFunction(new BivarEvaluable(){
			@Override
			public double eval(double u, double v){
				return function.diffuBw(u, v);
			}
		});
	}

	/**
	 * Construit une nouvelle fonction à partir de la dérivée en v d'une autre
	 * fonction.
	 * 
	 * @param function la fonction à utiliser
	 * @return la nouvelle fonction
	 */
	public BivarFunction newDiffvFunction(final BivarFunction function) {
		return new BivarFunction(new BivarEvaluable(){
			@Override
			public double eval(double u, double v){
				return function.diffv(u, v);
			}
		});
	}
	
	/**
	 * Construit une nouvelle fonction à partir de la dérivée en v à droite d'une autre
	 * fonction.
	 * 
	 * @param function la fonction à utiliser
	 * @return la nouvelle fonction
	 */
	public BivarFunction newDiffvFwFunction(final BivarFunction function) {
		return new BivarFunction(new BivarEvaluable(){
			@Override
			public double eval(double u, double v){
				return function.diffvFw(u, v);
			}
		});
	}
	
	/**
	 * Construit une nouvelle fonction à partir de la dérivée en v à gauche d'une autre
	 * fonction.
	 * 
	 * @param function la fonction à utiliser
	 * @return la nouvelle fonction
	 */
	public BivarFunction newDiffvBwFunction(final BivarFunction function) {
		return new BivarFunction(new BivarEvaluable(){
			@Override
			public double eval(double u, double v){
				return function.diffvBw(u, v);
			}
		});
	}
}
