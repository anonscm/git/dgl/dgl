/*
** UnivarBoundedFunction.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.functions;

import org.lgmt.dgl.commons.Interval;
import org.lgmt.dgl.commons.OutOfIntervalValueException;

/**
 * Une fonction bornée.
 * 
 * @navassoc - - - Interval
 * 
 * @author redonnet
 *
 */
public class UnivarBoundedFunction extends UnivarFunction {
	protected Interval interval;

	public UnivarBoundedFunction(UnivarEvaluable expression, Interval interval){
		super(expression);
		this.interval = interval;
	}

	/**
	 * Evalue la fonction.
	 * 
	 * @param u
	 * @return la valeur de la fonction en u
	 */
	@Override
	public double eval(double u){
		if (this.interval.includes(u))
			return f.eval(u);
		else
			throw(new OutOfIntervalValueException(u));
	}

	public boolean isDefinedFor(double u){
		if (this.interval.includes(u))
			return true;
		else
			return false;
	}

	public Interval getInterval() {
		return interval;
	}

	public void setIncludeRightBound(){
		this.interval.setRightIncluded(true);
	}
	
	/**
	 * Différencie la fonction en tenant compte des bornes.
	 */
	public double diff(double u){
		double h = 1.0E-6; // cf diff
		if (!this.interval.includes(u))
			throw(new OutOfIntervalValueException(u));
		else if(u<interval.getLeftValue() + h)
			return diffuFw(u);
		else if(u>interval.getRightValue() - h)
			return diffuBw(u);
		else
			return diff(u);
	}
}
