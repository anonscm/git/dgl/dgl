/**
 ** FrameSet.java
 ** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
 **
 ** This file is part of the DGL software package (DGL stands for
 ** Differential Geometry Library). This software provides mathematic and numeric
 ** tools to work with various curves and surfaces (algebraic, Bézier,
 ** NURBS, ...) with an unified interface.
 **
 ** This software is governed by the CeCILL-C V2 license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 ** 
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-C license and that you accept its terms.
 */
package org.lgmt.dgl.commons;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;

/**
 * Un système de repères.
 * 
 * Gère et maintient un système de repères cohérent. La liste des repères est
 * stockée dans un HasMap&lt;Long,Frame&gt;, chaque repère étant identifié de manière
 * unique par un Long. Comme on suppose qu'on ne peut manipuler qu'un système de
 * repères à la fois, cette classe est un singleton.
 * 
 * @stereotype singleton
 * @composed - - * Frame
 * @navassoc - globalFrame - Frame
 * 
 * @author redonnet
 */
public final class FrameSet {
	private static volatile FrameSet instance = null;
	private HashMap<Long, Frame> frames;
	private Frame globalFrame;

	/**
	 * Crée un nouveau système de repères et définit le repère global de ce nouveau
	 * système.
	 */
	private FrameSet() {
		super();
		frames = new HashMap<Long, Frame>();
		globalFrame = new Frame(this);
		this.addFrame(globalFrame);
	}

	public final static FrameSet getInstance() {
		// Le "Double-Checked Singleton"/"Singleton doublement vérifié" permet
		// d'éviter un appel coûteux à synchronized,
		// une fois que l'instanciation est faite.
		if (FrameSet.instance == null) {
			// Le mot-clé synchronized sur ce bloc empêche toute instanciation
			// multiple même par différents "threads".
			// Il est TRES important.
			synchronized (FrameSet.class) {
				if (FrameSet.instance == null) {
					FrameSet.instance = new FrameSet();
				}
			}
		}
		return FrameSet.instance;
	}

	/**
	 * Ajoute un nouveau repère à this.
	 * 
	 * @param frame le repère à ajouter.
	 */
	public void addFrame(Frame frame) {
		frames.put(frame.getId(), frame);
	}

	/**
	 * Supprime un repère de l'arborescence
	 * 
	 * TODO : test
	 * 
	 * @param frame
	 */
	public void removeFrame(Frame frame) {
		if (frame.getId() == 1L) {
			// On cherche à supprimer le repère racine
			// TODO : lever une erreur
			return;
		}
		// On reconnecte les fils de frame au père de frame
		for (Long id : this.getChildsId(frame)) {
			getFrameById(id).setParent(frame.getParent());
		}
		// On supprime frame de la liste
		frames.remove(frame.getId());
	}

	/**
	 * 
	 * @return le repère global de this.
	 */
	public Frame getGlobalFrame() {
		return globalFrame;
	}

	/**
	 * Retourne la liste des identifiants internes des repères fils du repère passé
	 * en paramètre. N'est a priori nécessaire qu'à des fins de débogage ou de
	 * manipulation de l'arbre des repères en interne. Devra être passé en private
	 * 
	 * @param frame le repère dont on veut identifier les fils
	 * @return la liste des identifiants internes des repères dont le père est frame
	 */
	public ArrayList<Long> getChildsId(Frame frame) {
		ArrayList<Long> list = new ArrayList<Long>(0);
		for (Map.Entry<Long, Frame> f : frames.entrySet()) {
			if (f.getValue().getParent().getId() == frame.getId())
				list.add(f.getKey());
		}
		return list;
	}

	/**
	 * Retourne la liste des repères fils du repère passé en paramètre.
	 * 
	 * @param frame le repère dont on veut connaître les fils
	 * @return la liste des repères dont le père est frame
	 */
	public ArrayList<Frame> getChilds(Frame frame) {
		ArrayList<Frame> list = new ArrayList<Frame>(0);
		for (Map.Entry<Long, Frame> fes : frames.entrySet()) {
			Frame f = fes.getValue();
			if (f.getParent() != null)
				if (f.getParent().getId() == frame.getId())
					list.add(f);
		}
		return list;
	}

	/**
	 * Calcule le chemin le plus court entre deux repères de this.
	 * 
	 * Le chemin calculé est retourné sous la forme d'une liste d'identifiants
	 * internes de repères.
	 * 
	 * @param frame1 le point de départ du chemin à calculer
	 * @param frame2 le point d'arrivée du chemin à calculer
	 * @return la liste des identifiants internes des repères du chemin
	 */
	public static ArrayList<Long> getIdPath(Frame frame1, Frame frame2) {
		ArrayList<Long> path = getIdPathToRoot(frame1);
		ArrayList<Long> path2 = getIdPathToRoot(frame2);
		if (path.size() >= 2 && path2.size() >= 2)
			while (path.get(path.size() - 2) == path2.get(path2.size() - 2)) {
				path.remove(path.size() - 1);
				path2.remove(path2.size() - 1);
			}
		Collections.reverse(path2);
		path2.remove(0);
		path.addAll(path2);
		return path;
	}

	/**
	 * Calcule le chemin du repère passé en paramètre au repère global.
	 * 
	 * @param frame un repère de this
	 * @return la liste des identifiants internes des repères du chemin
	 */
	private static ArrayList<Long> getIdPathToRoot(Frame frame) {
		ArrayList<Long> path = new ArrayList<Long>(1);
		Frame f = frame;
		path.add(f.getId());
		if (f.getParent() != null) {// si on n'est pas déjà à la racine
			f = f.getParent();
			while (f != null) {
				path.add(f.getId());
				f = f.getParent();
			}
		}
		return path;
	}

	/**
	 * 
	 * @return la liste des repères de this
	 */
	public HashMap<Long, Frame> getFrames() {
		return frames;
	}

	/**
	 * Retourne un repère déterminé par son id passée en paramètre.
	 * 
	 * @param id l'identifiant interne du repère recherché
	 * @return le repère identifié par id
	 */
	public Frame getFrameById(Long id) {
		return frames.get(id);
	}

	/**
	 * Effectue un changement de repère pour un point. Le résultat est stocké dans
	 * un point passé en paramètre.
	 * 
	 * Les coordonnées du point initPoint, exprimées dans initFrame sont calculées
	 * dans le repère finalFrame puis stockées dans le point finalPoint.
	 * 
	 * @param initPoint  les coordonnées du point de départ exprimées dans initFrame
	 * @param initFrame  le repère de départ
	 * @param finalFrame le repère d'arrivée
	 * @param finalPoint un point pour stocker le résultat
	 */
	public void transform(Point3d initPoint, Frame initFrame, Frame finalFrame, Point3d finalPoint) {
		Matrix4d m = this.getTransformationMatrix(initFrame, finalFrame);
		m.transform(initPoint, finalPoint);
	}

	/**
	 * Effectue un changement de repère pour un point. Le résultat retourné dans un
	 * point créé à cette fin.
	 * 
	 * Les coordonnées du point initPoint, exprimées dans initFrame sont calculées
	 * dans le repère finalFrame puis stockées dans le point finalPoint.
	 * 
	 * @param initPoint  les coordonnées du point de départ exprimées dans initFrame
	 * @param initFrame  le repère de départ
	 * @param finalFrame le repère d'arrivée
	 */
	public Point3d transform(Point3d initPoint, Frame initFrame, Frame finalFrame) {
		Point3d result = new Point3d();

		transform(initPoint, initFrame, finalFrame, result);

		return result;
	}

	/**
	 * Effectue un changement de repère pour un vecteur.
	 * 
	 * Les coordonnées du vecteur initVector, exprimées dans initFrame sont
	 * calculées dans le repère finalFrame puis stockées dans le vecteur
	 * finalVector.
	 * 
	 * @param initVector  les coordonnées du point de départ exprimées dans
	 *                    initFrame
	 * @param initFrame   le repère de départ
	 * @param finalFrame  le repère d'arrivée
	 * @param finalVector un vecteur pour stocker le résultat
	 */
	public void transform(Vector3d initVector, Frame initFrame, Frame finalFrame, Vector3d finalVector) {
		Matrix4d m = this.getTransformationMatrix(initFrame, finalFrame);
		m.transform(initVector, finalVector);
	}

	/**
	 * Effectue un changement de repère pour un vecteur.
	 * 
	 * Les coordonnées du vecteur initVector, exprimées dans initFrame sont
	 * calculées dans le repère finalFrame puis stockées dans le vecteur
	 * finalVector.
	 * 
	 * @param initVector  les coordonnées du point de départ exprimées dans
	 *                    initFrame
	 * @param initFrame   le repère de départ
	 * @param finalFrame  le repère d'arrivée
	 */
	public Vector3d transform(Vector3d initVector, Frame initFrame, Frame finalFrame) {
		Vector3d result = new Vector3d();
		transform(initVector, initFrame, finalFrame, result);
		return result;
	}

	/**
	 * Effectue un changement de repère pour une surface. Le résultat retourné dans
	 * une surface créée à cette fin.
	 * 
	 * Les coordonnées du vecteur initSurface, exprimées dans initFrame sont
	 * calculées dans le repère finalFrame puis retournées.
	 * 
	 * @param initSurface les coordonnées du point de départ exprimées dans
	 *                    initFrame
	 * @param initFrame   le repère de départ
	 * @param finalFrame  le repère d'arrivée
	 */
	public Surface transform(Surface initSurface, Frame initFrame, Frame finalFrame) {
		Surface newSurface = new Surface(initSurface);
		Matrix4d m = instance.getTransformationMatrix(initFrame, finalFrame);
		newSurface.transform(m);
		return newSurface;
	}

	/**
	 * Retourne la matrice de transformation permettant de passer d'un repère à un autre
	 * 
	 * @param initFrame
	 * @param finalFrame
	 * @return la matrice de transformation
	 */
	public Matrix4d getTransformationMatrix(Frame initFrame, Frame finalFrame) {
		ArrayList<Long> path1 = getIdPathToRoot(initFrame);
		ArrayList<Long> path2 = getIdPathToRoot(finalFrame);

		// calcul du chemin le plus court entre frame1 et frame2
		int s = Math.min(path1.size(), path2.size());
		while (s != 0) {
			if (s > 1)
				if (path1.get(path1.size() - 2) == path2.get(path2.size() - 2)) {
					path1.remove(path1.size() - 1);
					path2.remove(path2.size() - 1);
					s = s - 1;
				} else
					s = 0;
			else
				s = 0;
		}

		Frame f;
		Matrix4d m = new Matrix4d();

		Matrix4d mt1 = new Matrix4d();
		mt1.setIdentity();
		for (int i = 0; i < path1.size() - 1; i++) {
			f = getFrameById(path1.get(i));
			m.set(f.getOrientation(), new Vector3d(f.getPosition()), 1.0);
			mt1.mul(m, mt1);
		}

		Matrix4d mt2 = new Matrix4d();
		mt2.setIdentity();
		for (int i = 0; i < path2.size() - 1; i++) {
			f = getFrameById(path2.get(i));
			m.set(f.getOrientation(), new Vector3d(f.getPosition()), 1.0);
			mt2.mul(m, mt2);
		}
		mt2.invert();

		Matrix4d mt = new Matrix4d();
		mt.mul(mt2, mt1);
		return (mt);
	}
}
