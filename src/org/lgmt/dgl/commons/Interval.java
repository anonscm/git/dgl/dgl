/*
 ** Interval.java
 ** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
 **
 ** This file is part of the DGL software package (DGL stands for
 ** Differential Geometry Library). This software provides mathematic and numeric
 ** tools to work with various curves and surfaces (algebraic, Bézier,
 ** NURBS, ...) with an unified interface.
 **
 ** This software is governed by the CeCILL-C V2 license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 ** 
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-C license and that you accept its terms.
 */
package org.lgmt.dgl.commons;

/**
 * Un intervalle.
 * 
 * @author redonnet
 * 
 */
public class Interval {
	private double leftValue;
	private boolean leftIncluded;
	private double rightValue;
	private boolean rightIncluded;

	public Interval(double leftValue, boolean leftIncluded, double rightValue, boolean rightIncluded) {
		this.leftValue = leftValue;
		this.leftIncluded = leftIncluded;
		this.rightValue = rightValue;
		this.rightIncluded = rightIncluded;
	}

	/**
	 * Par défaut les bornes sont inclues dans l'intervalle
	 * 
	 * @param leftValue
	 * @param rightValue
	 */
	public Interval(double leftValue, double rightValue) {
		this(leftValue, true, rightValue, true);
	}

	public Interval(double leftValue, boolean leftIncluded, double rightValue) {
		this(leftValue, leftIncluded, rightValue, true);
	}

	public Interval(double leftValue, double rightValue, boolean rightIncluded) {
		this(leftValue, true, rightValue, rightIncluded);
	}

	public boolean includes(double value) {
		if (leftIncluded && value < leftValue)
			return false;
		if (!leftIncluded && value <= leftValue)
			return false;
		if (rightIncluded && value > rightValue)
			return false;
		if (!rightIncluded && value >= rightValue)
			return false;
		return true;
	}

	public boolean overlaps(Interval interval) {
		if (interval.leftValue > this.rightValue)
			return false;
		if (interval.rightValue < this.leftValue)
			return false;
		if (interval.leftValue == this.rightValue) {
			if (interval.isLeftIncluded() != this.isRightIncluded())
				return false;
			if (!interval.isLeftIncluded() && !this.isRightIncluded())
				return false;
		}
		if (interval.rightValue == this.leftValue) {
			if (interval.isRightIncluded() != this.isLeftIncluded())
				return false;
			if (!interval.isRightIncluded() && !this.isLeftIncluded())
				return false;
		}
		return true;
	}

	public double getLeftValue() {
		return leftValue;
	}

	public void setLeftValue(double leftValue) {
		if (leftValue > rightValue)
			/** TODO : lever l'exception adéquate */
			System.err.println("Erreur...");
		else
			this.leftValue = leftValue;
	}

	public boolean isLeftIncluded() {
		return leftIncluded;
	}

	public void setLeftIncluded(boolean leftIncluded) {
		this.leftIncluded = leftIncluded;
	}

	public double getRightValue() {
		return rightValue;
	}

	public void setRightValue(double rightValue) {
		if (rightValue < leftValue)
			/** TODO : lever l'exception adéquate */
			System.err.println("Erreur...");
		else
			this.rightValue = rightValue;
	}

	public boolean isRightIncluded() {
		return rightIncluded;
	}

	public void setRightIncluded(boolean rightIncluded) {
		this.rightIncluded = rightIncluded;
	}

	@Override
	public String toString() {
		String str = new String();
		if (leftIncluded)
			str += "[";
		else
			str += "]";
		str += leftValue + " , " + rightValue;
		if (rightIncluded)
			str += "]";
		else
			str += "[";

		return str;
	}
}
