/*
** Index2.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the CoSMO software package (CoSMO stands for
** Complex Surfaces Machining Optimization). This software aims to deal
** with optimization of toolpath planning strategy when machining complex
** surfaces.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.commons;

import java.util.Comparator;

/**
 * Classe utilitaire pour manipuler les index dans deux directions.
 * 
 * Par commodité les valeurs <b>i</b> et <b>j</b> des index sont laissées en
 * <i>public</i>.
 * 
 * @author redonnet
 *
 */
public class Index2 {
	public int i;
	public int j;

	public Index2(int i, int j) {
		super();
		this.i = i;
		this.j = j;
	}

	public boolean isNextTo(Index2 idx) {
		if ((this.i == idx.i && (this.j == idx.j + 1 || this.j == idx.j - 1))
				|| (this.j == idx.j && (this.i == idx.i + 1 || this.i == idx.i - 1)))
			return true;
		else
			return false;
	}

	public boolean isNvoisin(Index2 idx) {
		if ((this.i == idx.i && (this.j == idx.j + 1 || this.j == idx.j - 1))
				|| (this.j == idx.j && (this.i == idx.i + 1 || this.i == idx.i - 1))
				|| (this.i == idx.i + 1 && (this.j == idx.j + 1 || this.j == idx.j - 1))
				|| (this.i == idx.i - 1 && (this.j == idx.j + 1 || this.j == idx.j - 1)))
			return true;
		else
			return false;
	}

	@Override
	public boolean equals(Object obj) {
		if (this.i == ((Index2) obj).i && this.j == ((Index2) obj).j)
			return true;
		else
			return false;
	}

	@Override
	public String toString() {
		return "(" + i + ", " + j + ")";
	}

	public static class iComparator implements Comparator<Index2> {

		@Override
		public int compare(Index2 idx1, Index2 idx2) {
			if (idx1.i < idx2.i)
				return -1;
			if (idx1.i > idx2.i)
				return 1;
			if (idx1.i == idx2.i) {
				if (idx1.j < idx2.j)
					return -1;
				if (idx1.j > idx2.j)
					return 1;
				if (idx1.j == idx2.j)
					return 0;
			}
			return 0;
		}
	}

	public static class jComparator implements Comparator<Index2> {

		@Override
		public int compare(Index2 idx1, Index2 idx2) {
			if (idx1.j < idx2.j)
				return -1;
			if (idx1.j > idx2.j)
				return 1;
			if (idx1.j == idx2.j) {
				if (idx1.i < idx2.i)
					return -1;
				if (idx1.i > idx2.i)
					return 1;
				if (idx1.i == idx2.i)
					return 0;
			}
			return 0;
		}
	}
}