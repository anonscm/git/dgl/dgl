/*
 ** Interval.java
 ** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
 **
 ** This file is part of the DGL software package (DGL stands for
 ** Differential Geometry Library). This software provides mathematic and numeric
 ** tools to work with various curves and surfaces (algebraic, Bézier,
 ** NURBS, ...) with an unified interface.
 **
 ** This software is governed by the CeCILL-C V2 license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 ** 
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-C license and that you accept its terms.
 */
package org.lgmt.dgl.commons;

/**
 * Un double intervalle.
 * 
 * @author redonnet
 * 
 */
public class Interval2 {
	private Interval interval1;
	private Interval interval2;

	public Interval2(Interval i1, Interval i2) {
		this.interval1 = i1;
		this.interval2 = i2;
	}

	public boolean includes(double value1, double value2) {
		if (interval1.includes(value1) && interval2.includes(value2))
			return true;
		return false;
	}

	public double getLeftValue1() {
		return interval1.getLeftValue();
	}

	public double getRightValue1() {
		return interval1.getRightValue();
	}
	
	public double getLeftValue2() {
		return interval2.getLeftValue();
	}

	public double getRightValue2() {
		return interval2.getRightValue();
	}

	@Override
	public String toString() {
		String str = new String("("+interval1.toString()+","+interval2.toString()+")");

		return str;
	}
	
	
}
