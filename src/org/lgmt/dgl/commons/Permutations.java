package org.lgmt.dgl.commons;

import java.util.ArrayList;

/**
 * Computes the permutations of an ArrayList
 * 
 * Uses the Heap's algorithm: « Permutations by Interchanges », The Computer
 * Journal, vol. 6, no 3,‎ 1963, pp. 293–4
 * 
 * @author redonnet
 *
 */
public final class Permutations {
	private Permutations() {
	}

	public static <T> ArrayList<ArrayList<T>> permut(ArrayList<T> list) {
		ArrayList<ArrayList<T>> permutList = new ArrayList<ArrayList<T>>();
		int n = list.size();

		calcPermut(n, list, permutList);

		return permutList;
	}

	public static <T> void calcPermut(int n, ArrayList<T> elements, ArrayList<ArrayList<T>> permutList) {
		if (n == 1) {
			permutList.add(new ArrayList<T>(elements));
		} else {
			for (int i = 0; i < n - 1; i++) {
				calcPermut(n - 1, elements, permutList);
				if (n % 2 == 0) {
					swap(elements, i, n - 1);
				} else {
					swap(elements, 0, n - 1);
				}
			}
			calcPermut(n - 1, elements, permutList);
		}
	}

	private static <T> void swap(ArrayList<T> elements, int a, int b) {
		T tmp = elements.get(a);
		elements.set(a, elements.get(b));
		elements.set(b, tmp);
	}

}
