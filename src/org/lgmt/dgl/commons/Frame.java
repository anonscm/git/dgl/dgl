/*
** Frame.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.commons;

import java.util.ArrayList;

import org.lgmt.dgl.vecmath.AxisAngle4d;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Quat4d;
import org.lgmt.dgl.vecmath.Vector3d;


/**
 * Un repère.
 * 
 * Chaque repère est défini par sa position et son orientation par rapport à son
 * repère parent. Le repère global est construit automatiquement à la création
 * d'un système de repères (voir {@link FrameSet}).
 * 
 * @navassoc - frameSet - FrameSet
 * @navassoc - parent - Frame
 * 
 * @author redonnet
 * 
 */
public class Frame {
	private static Long nextId = 1L;
	private Long id;
	private FrameSet frameSet;
	private Frame parent;
	private Point3d position;
	private Quat4d orientation;

	/**
	 * Avec un paramètre FrameSet, le constructeur Frame() crée un repère global
	 * pour le système de repères FrameSet.
	 * 
	 * Est appelé par le constructeur de FrameSet pour construire le repère
	 * global de l'environnement courant. Ne devrait jamais être appelé par
	 * ailleurs...
	 * 
	 * @param frameSet
	 *            le système de repère pour lequel on veut définir un repère
	 *            global
	 */
	public Frame(FrameSet frameSet) {
		this.parent = null;
		this.frameSet = frameSet;
		this.position = new Point3d(0, 0, 0);
		this.orientation = new Quat4d(0, 0, 0, 1);
		this.id = nextId;
		nextId++;
	}

	/**
	 * Crée un nouveau repère.
	 * 
	 * Le posage du repère créé est identique à celui du parent.
	 * 
	 * @param parent
	 *            le repère parent
	 */
	public Frame(Frame parent) {
		this.frameSet = parent.frameSet;
		this.parent = parent;
		this.position = new Point3d(0.0, 0.0, 0.0);
		this.orientation = new Quat4d(0.0, 0.0, 0.0, 1.0);
		this.id = nextId;
		nextId++;
		frameSet.addFrame(this);
	}

	/**
	 * Crée un nouveau repère.
	 * 
	 * Le posage du repère créé est défini par la matrice de transformation
	 * transform par rapport au repère parent.
	 * 
	 * @param parent
	 *            le repère parent
	 * @param transform
	 *            la matrice de transformation homogène
	 */
	public Frame(Frame parent, Matrix4d transform) {
		this.frameSet = parent.frameSet;
		this.parent = parent;
		Vector3d tmp = new Vector3d();
		transform.get(tmp);
		this.position = new Point3d(tmp);
		this.orientation = new Quat4d();
		transform.get(orientation);
		this.id = nextId;
		nextId++;
		frameSet.addFrame(this);
	}

	/**
	 * Crée un nouveau repère.
	 * 
	 * Le posage du repère créé est défini par sa position et son orientation
	 * par rapport au repère parent définie par un quaternion.
	 * 
	 * @param parent
	 *            le repère parent
	 * @param position
	 *            la position du nouveau repère
	 * @param orientation
	 *            le quaternion définissant l'orientation du nouveau repère
	 */
	public Frame(Frame parent, Point3d position, Quat4d orientation) {
		super();
		this.frameSet = parent.frameSet;
		this.parent = parent;
		this.position = position;
		this.orientation = orientation;
		this.id = nextId;
		nextId++;
		frameSet.addFrame(this);
	}

	/**
	 * Crée un nouveau repère.
	 * 
	 * Le posage du repère créé est défini par sa position et son orientation
	 * par rapport au repère parent définie par une rotation.
	 * 
	 * @param parent
	 *            le repère parent
	 * @param position
	 *            la position du nouveau repère
	 * @param orientation
	 *            la rotation définissant l'orientation du nouveau repère
	 */
	public Frame(Frame parent, Point3d position, AxisAngle4d orientation) {
		super();
		this.frameSet = parent.frameSet;
		this.parent = parent;
		this.position = position;
		this.orientation = new Quat4d();
		this.orientation.set(orientation);
		this.id = nextId;
		nextId++;
		frameSet.addFrame(this);
	}

	/**
	 * 
	 * @return le repère parent de this
	 */
	public Frame getParent() {
		return parent;
	}

	/**
	 * Teste si le repère frame est un fils de this.
	 * 
	 * @param frame
	 *            le repère à tester
	 * @return true si frame est fils de this, false dans le cas contraire
	 */
	public boolean isChildOf(Frame frame) {
		ArrayList<Frame> childs = frameSet.getChilds(frame);
		for (Frame f : childs)
			if (f.getId() == this.id)
				return true;
		return false;
	}

	/**
	 * Définit le repère parent de this.
	 * 
	 * @param parent
	 *            le repère parent
	 */
	public void setParent(Frame parent) {
		this.parent = parent;
	}

	/**
	 * 
	 * @return la position de this par rapport à son repère parent.
	 */
	public Point3d getPosition() {
		return position;
	}

	/**
	 * Définit la position de this par rapport à son repère parent.
	 * 
	 * @param position
	 *            un point défini dans le repère parent
	 */
	public void setPosition(Point3d position) {
		this.position = position;
	}

	/**
	 * 
	 * @return l'orientation de this par rapport à son repère parent.
	 */
	public Quat4d getOrientation() {
		return orientation;
	}

	/**
	 * Définit l'orientation de this par rapport à son repère parent.
	 * 
	 * @param orientation
	 *            un quaternion
	 */
	public void setOrientation(Quat4d orientation) {
		this.orientation = orientation;
	}

	/**
	 * Définit l'orientation de this par rapport à son repère parent.
	 * 
	 * @param orientation
	 *            une rotation par rapport à un vecteur quelconque.
	 */
	public void setOrientation(AxisAngle4d orientation) {
		this.orientation.set(orientation);
	}
	
	
	/**
	 * Retourne la matrice de transformation permettant de passer du repère
	 * parent à this.
	 * 
	 * @return la matrice de transformation permettant de passer du repère
	 *         parent à this.
	 */
	public Matrix4d getTransform() {
		return new Matrix4d(this.orientation, new Vector3d(this.position), 1.0);
	}

	/**
	 * Retourne l'identifiant interne de this. Utilisé essentiellement à des
	 * fins de débogage
	 * 
	 * @return l'identifiant interne de this.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @return le système de repère auquel appartient this.
	 */
	public FrameSet getFrameSet() {
		return this.frameSet;
	}
}
