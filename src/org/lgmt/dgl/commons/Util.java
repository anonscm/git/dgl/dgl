/*
 ** Util.java
 ** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
 **
 ** This file is part of the DGL software package (DGL stands for
 ** Differential Geometry Library). This software provides mathematic and numeric
 ** tools to work with various curves and surfaces (algebraic, Bézier,
 ** NURBS, ...) with an unified interface.
 **
 ** This software is governed by the CeCILL-C V2 license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 ** 
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-C license and that you accept its terms.
 */
package org.lgmt.dgl.commons;

import java.util.InputMismatchException;

import org.lgmt.dgl.curves.Curve;
import org.lgmt.dgl.surfaces.Surface;
import org.lgmt.dgl.surfaces.SurfaceParam;
import org.lgmt.dgl.vecmath.AxisAngle4d;
import org.lgmt.dgl.vecmath.GVector;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector2d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.dgl.vecmath.Vectors3;

/**
 * Classe de fonctions utilitaires et de constantes.
 * 
 * @author redonnet
 * 
 */
public class Util {
	/** 1E-3 */
	public static final double PREC3 = 1.0E-03D;
	/** 1E-6 */
	public static final double PREC6 = 1.0E-06D;
	/** 1E-9 */
	public static final double PREC9 = 1.0E-09D;
	/** 1E-12 */
	public static final double PREC12 = 1.0E-12D;
	/** Précision d'affichage des nombres */
	private static int displayPrecision = 3;
	/** Format des nombre correspondant à displayPrecision */
	public static String format = new String("%." + displayPrecision + "f");

	public static int getDisplayPrecision() {
		return displayPrecision;
	}

	public static void setDisplayPrecision(int displayPrecision) {
		Util.displayPrecision = displayPrecision;
		format = new String("%." + displayPrecision + "f");
	}

	/**
	 * Factorielle d'un entier.
	 * 
	 * WARNING: hits the max int limit for x {@literal >} 12
	 * 
	 * @param x
	 * @return x!
	 */
	static public int factorial(int x) {
		if (x>12)
			throw new InputMismatchException("Factorial result out of integer range");
		if (x <= 1)
			return 1;
		else
			return x * factorial(x - 1);
	}

	/**
	 * Binôme de Newton.
	 * 
	 * @param sup
	 * @param inf
	 * @return la valeur du binôme de Newton
	 * 
	 * @deprecated Use binomial(int n, int k) instead
	 */
	@Deprecated
	static public double newtonBinom(int sup, int inf) {
		if (sup < inf)
			return 0.0;
		return (double) factorial(sup) / (double) (factorial(sup - inf) * factorial(inf));
	}

	/**
	 * Binomial coefficient 
	 * /n\
	 * | |
	 * \k/
	 * 
	 * @param n the upper value
	 * @param k the lower value
	 * @return the value of the binomial coefficient
	 */
	static public double binomial(int n, int k) {
		if(k>n)
			throw new ArithmeticException();
		if ((n == k) || (k == 0))
			return 1;
		else
			return binomial(n - 1, k) + binomial(n - 1, k - 1);
	}

	/**
	 * Fonction de Bernstein.
	 * 
	 * @param sup
	 * @param inf
	 * @param u
	 * @return la valeur de la fonction de Bernstein
	 */
	static public double bernstein(int sup, int inf, double u) {
		return binomial(sup, inf) * Math.pow(u, inf) * Math.pow((1 - u), (sup - inf));
	}

	/**
	 * Valeur d'un angle sur un intervalle de 2 PI donné.
	 * 
	 * <p>
	 * Retourne la valeur de l'angle comprise entre <b>intervalCenter - PI</b> et
	 * <b>intervalCenter + PI</b>.
	 * 
	 * @param value          la valeur d'entrée (en radians)
	 * @param intervalCenter
	 * @return la valeur calculée (en radians)
	 */
	static public double angleToInterval(double value, double intervalCenter) {
		double result = value;

		while (result < intervalCenter - Math.PI)
			result = result + 2 * Math.PI;
		while (result > intervalCenter + Math.PI)
			result = result - 2 * Math.PI;
		return result;
	}

	/**
	 * Retourne la norme 1 d'un vecteur.
	 * 
	 * <p>
	 * La norme 1 d'un vecteur est donnée par la somme des valeurs absolues des
	 * composantes du vecteur.
	 * </p>
	 * 
	 * @param v le vecteur
	 * @return la norme 1 calculée
	 */
	static public double norm1(GVector v) {
		double sum = 0.0;
		for (int i = 0; i < v.getSize(); i++) {
			sum = sum + Math.abs(v.getElement(i));
		}
		return sum;
	}

	static public boolean isNull(double value) {
		if (Math.abs(value) < PREC12)
			return true;
		else
			return false;
	}

	/**
	 * Valeur d'un angle entre deux vecteurs ramené sur l'intervalle [-Pi/2,Pi/2]
	 * 
	 * @param v1 le premier vecteur
	 * @param v2 le second vecteur
	 * @return la valeur calculée (en radians)
	 */
	static public double angle0(Vector2d v1, Vector2d v2) {
		double result;

		// La méthode angle de vecmath retourne un angle entre 0 et Pi
		result = v1.angle(v2);

		if (result > Math.PI / 2)
			result = result - Math.PI;

		return result;
	}

	/**
	 * Valeur d'un angle entre deux vecteurs ramené sur l'intervalle [-Pi/2,Pi/2]
	 * 
	 * @param v1 le premier vecteur
	 * @param v2 le second vecteur
	 * @return la valeur calculée (en radians)
	 */
	static public double angle0(Vector3d v1, Vector3d v2) {
		double result;

		// La méthode angle de vecmath retourne un angle entre 0 et Pi
		result = v1.angle(v2);

		if (result > Math.PI / 2)
			result = result - Math.PI;

		return result;
	}

	/**
	 * Calcul d'un angle orienté entre deux vecteurs
	 * 
	 * En utilisant la méthode atan2
	 * 
	 * @param v1
	 * @param v2
	 * @return l'angle compris entre -Pi et Pi
	 */
	static public double angle2(Vector3d v1, Vector3d v2) {
		double result;

		Vector3d crossV = Vectors3.crossV(v1, v2);
		double dotV = v1.dot(v2);

		result = Math.atan2(crossV.length(), dotV);

		return result;
	}

	/**
	 * Calcule l'angle formé par les points passés en paramètres en utilisant la
	 * méthode atan2
	 * 
	 * L'angle à calculer est formé par les points p1, p2 et le sommet s.
	 * 
	 * @param p1
	 * @param p2
	 * @param s
	 * @return l'angle compris entre -Pi et Pi
	 */
	static public double angle2(Point3d p1, Point3d s, Point3d p2) {
		Vector3d v1 = new Vector3d(s, p1);
		Vector3d v2 = new Vector3d(s, p2);

		return angle2(v1, v2);
	}

	/**
	 * Ramène une valeur à un intervalle.
	 * 
	 * @param value
	 * @param min
	 * @param max
	 * @return la valeur corrigée
	 */
	static public double clamp(double value, double min, double max) {
		if (value < min)
			value = min;
		if (value > max)
			value = max;

		return value;
	}

	/**
	 * Ramène une valeur à un intervalle définit par les limites paramétriques d'une
	 * courbe.
	 * 
	 * @param value
	 * @param c
	 * @return la valeur corrigée
	 */
	static public double clamp(double value, Curve c) {
		if (value < c.getUmin())
			value = c.getUmin();
		if (value > c.getUmax())
			value = c.getUmax();

		return value;
	}

	/**
	 * Ramène une valeur à un intervalle définit par les limites paramétriques d'une
	 * surface.
	 * 
	 * @param value
	 * @param s
	 * @param param la direction paramétrique à considérer
	 * @return la valeur corrigée
	 */
	static public double clamp(double value, Surface s, SurfaceParam param) {
		if (param == SurfaceParam.U) {
			if (value < s.getUmin())
				value = s.getUmin();
			if (value > s.getUmax())
				value = s.getUmax();
		}
		if (param == SurfaceParam.V) {
			if (value < s.getVmin())
				value = s.getVmin();
			if (value > s.getVmax())
				value = s.getVmax();
		}

		return value;
	}

	/**
	 * Arrondi la valeur d'un double à un nombre de décimale fixé
	 * 
	 * @param value la valeur à arrondir
	 * @param prec  le nombre de décimales significatives
	 * @return la valeur arrondie
	 */
	static public double round(double value, int prec) {
		if (prec < 1)
			return value;
		double factor = 10.0;
		for (int i = 1; i < prec; i++)
			factor = factor * 10.0;
		return Math.floor(value * factor) / factor;
	}

	/**
	 * Calcule la rotation entre deux vecteurs.
	 * 
	 * @param v1 le premier vecteur
	 * @param v2 le deuxième vecteur
	 * @return un AxisAngle contenant la rotation permettant de passer de v1 à v2
	 */
	static public AxisAngle4d getRotation(Vector3d v1, Vector3d v2) {
		AxisAngle4d aa = new AxisAngle4d();
		Vector3d v = new Vector3d();
		v.cross(v1, v2);
		aa.set(v, v1.angle(v2));

		return aa;
	}
}
