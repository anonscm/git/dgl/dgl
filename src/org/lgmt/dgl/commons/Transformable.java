package org.lgmt.dgl.commons;

import org.lgmt.dgl.vecmath.Matrix4d;

/**
 * Une interface pour appliquer des rotations/translations aux objets
 * 
 * @author redonnet
 *
 */
public interface Transformable<T> {
	/**
	 * Applique une transformation à un objet
	 * 
	 * @param m
	 */
	public void transform(Matrix4d m);

	/**
	 * Clone en profondeur un objet et applique la transformation définie par la
	 * matrice m à ce clone
	 * 
	 * @param m
	 */
	public T transformClone(Matrix4d m);
}
