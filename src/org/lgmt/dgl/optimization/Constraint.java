/*
 ** Constraint.java
 ** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
 **
 ** This file is part of the DGL software package (DGL stands for
 ** Differential Geometry Library). This software provides mathematic and numeric
 ** tools to work with various curves and surfaces (algebraic, Bézier,
 ** NURBS, ...) with an unified interface.
 **
 ** This software is governed by the CeCILL-C V2 license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 ** 
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-C license and that you accept its terms.
 */
package org.lgmt.dgl.optimization;

import static java.lang.Math.pow;
import org.lgmt.dgl.functions.MultivarEvaluable;
import org.lgmt.dgl.functions.MultivarFunction;
import org.lgmt.dgl.vecmath.GVector;

/**
 * Une classe pour gérer les contraintes d'un problème d'optimisation.
 * 
 * Une contrainte est une fonction des variables du problème qui doit être
 * égale, inférieure ou égale ou supérieure ou égale à zéro.
 * 
 * @navassoc - - - MultivarFunction
 * @navassoc - - - TYPE
 * 
 * @author redonnet
 *
 */
public class Constraint {
	private static double defaultTolerance = 1E-8;

	/**
	 * Une enum définissant les différents types de contraintes.
	 * 
	 * @author redonnet
	 *
	 */
	public enum TYPE {
		EQ, LEQ, GEQ
	};

	private TYPE type;
	private MultivarFunction function;
	private boolean linear;
	private double tolerance;

	public Constraint(MultivarFunction function, TYPE type, boolean linear, double tolerance) {
		this.function = function;
		this.type = type;
		this.linear = linear;
		this.tolerance = tolerance;
	}

	public Constraint(MultivarFunction function, TYPE type, boolean linear) {
		this.function = function;
		this.type = type;
		this.linear = linear;
		this.tolerance = Constraint.defaultTolerance;
	}

	public Constraint(MultivarFunction function, TYPE type) {
		this.function = function;
		this.type = type;
		this.linear = false;
		this.tolerance = Constraint.defaultTolerance;
	}

	public double eval(GVector var) {
		return function.eval(var);
	}

	public boolean isValid(GVector var) {
		double val = function.eval(var);
		if (this.type == TYPE.LEQ && val < tolerance)
			return true;
		if (this.type == TYPE.GEQ && val > -tolerance)
			return true;
		if (this.type == TYPE.EQ && Math.abs(val) < tolerance)
			return true;
		return false;
	}

	/**
	 * Return the quadratic loss function corresponding to this constraint.
	 * 
	 * GEQ constraints are transformed to LEQ constraints before computing the penalty function. 
	 * 
	 * @return the newly calculated MultivarFunction
	 */
	public MultivarFunction getPenaltyFunction() {
		final MultivarFunction f = this.function;
		final Constraint c;
		if (this.type == TYPE.GEQ) { // conversion > en <
			c = new Constraint(new MultivarFunction(f.getNbvar(), new MultivarEvaluable() {
				@Override
				public double eval(GVector var) {
					return -1.0 * f.eval(var);
				}
			}), TYPE.LEQ);
		} else {
			c = this;
		}
		MultivarFunction penalty = new MultivarFunction(f.getNbvar(), new MultivarEvaluable() {
			@Override
			public double eval(GVector var) {
				if (!c.isValid(var))
					return pow(c.eval(var), 2);
				return 0.0;
			}
		});
		return penalty;
	}

	/**
	 * Return the logarithm barrier loss function corresponding to this constraint.
	 * 
	 * GEQ constraints are transformed to LEQ constraints before computing the barrier function. 
	 * 
	 * @return the newly calculated MultivarFunction
	 */
	public MultivarFunction getBarrierFunction() {
		final MultivarFunction f = this.function;
		final Constraint c;
		if (this.type == TYPE.GEQ) { // conversion > en <
			c = new Constraint(new MultivarFunction(f.getNbvar(), new MultivarEvaluable() {
				@Override
				public double eval(GVector var) {
					return -1.0 * f.eval(var);
				}
			}), TYPE.LEQ);
		} else {
			c = this;
		}
		MultivarFunction barrier = new MultivarFunction(f.getNbvar(), new MultivarEvaluable() {
			@Override
			public double eval(GVector var) {
				if (!c.isValid(var))
					throw new NonFeasiblePointException(var);
				return -1.0*Math.log(-1.0*c.eval(var));
			}
		});
		return barrier;
	}

	/*
	 * GETTERS et SETTERS
	 */
	public static double getDefaultTolerance() {
		return defaultTolerance;
	}

	public static void setDefaultTolerance(double defaultTolerance) {
		Constraint.defaultTolerance = defaultTolerance;
	}

	public TYPE getType() {
		return type;
	}

	public MultivarFunction getFunction() {
		return function;
	}

	public boolean isLinear() {
		return linear;
	}

	public double getTolerance() {
		return tolerance;
	}

	public void setTolerance(double tolerance) {
		this.tolerance = tolerance;
	}
}
