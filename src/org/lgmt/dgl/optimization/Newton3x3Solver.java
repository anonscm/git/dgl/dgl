package org.lgmt.dgl.optimization;

import java.util.Arrays;
import java.util.logging.Level;

import org.lgmt.dgl.commons.InvalidArraySizeException;
import org.lgmt.dgl.commons.MaxIterException;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.Function;
import org.lgmt.dgl.vecmath.GVector;
import org.lgmt.dgl.vecmath.SingularMatrixException;

public class Newton3x3Solver extends AbstractSolver {
	private Function[] f;
	private double[] y;
	private double[] x;
	private double[][] jacobian;
	private double[] delta;

	/**
	 * Construit un nouveau solver de taille 3.
	 * 
	 * @param maxIter   le nombre maximum d'itération allouées à cette résolution
	 * @param tolerance la tolérance de résolution
	 */
	public Newton3x3Solver(Function f1, Function f2, Function f3, int maxIter, double tolerance) {
		this.size = 3;
		this.f = new Function[] { f1, f2, f3 };
		for (Function func : f)
			if (func.getSize() != size)
				throw new IllConditionedProblemException();
		this.type = TYPE.NEWTON3x3;
		this.maxIter = maxIter;
		this.epsilon = tolerance;
		this.y = new double[size];
		this.jacobian = new double[size][size];
		this.delta = new double[size];
	}

	/**
	 * Construit un nouveau solver de taille 3.
	 * 
	 * Par défaut la tolérance est fixée à 1E-6
	 * 
	 * @param maxIter le nombre maximum d'itération allouées à cette résolution
	 */
	public Newton3x3Solver(Function f1, Function f2, Function f3, int maxIter) {
		this(f1, f2, f3, maxIter, Util.PREC6);
	}

	/**
	 * Construit un nouveau solver de taille 3.
	 * 
	 * Par défaut la tolérance est fixée à 1E-6 et le nombre maxi d'itérations est
	 * fixé à 100;
	 * 
	 * @param f1
	 * @param f2
	 */
	public Newton3x3Solver(Function f1, Function f2, Function f3) {
		this(f1, f2, f3, 100, Util.PREC6);
	}

	public boolean init(double[] x) {
		if (x.length != size)
			throw new InvalidArraySizeException(x.length);

		this.x = Arrays.copyOf(x, size);
		evalF();
		iter = -1;

		return true;
	}

	/**
	 * Initialise le solveur en définissant un point de départ.
	 * 
	 * @param var1 la valeur initiale de la première variable
	 * @param var2 la valeur initiale de la deuxième variable
	 * 
	 * @return true
	 */
	public boolean init(double var1, double var2, double var3) {
		return this.init(new double[] { var1, var2, var3 });
	}

	/**
	 * Lance la résolution.
	 * 
	 * @throws MaxIterException si le nombre maximum d'itérations allouées à cette
	 *                          résolution a été atteint sans que le critère de
	 *                          convergence soit satisfait.
	 */
	public Status run() {
		do {
			if (iter > maxIter)
				return Status.MAXITER_REACHED;
			iter++;
			if (iterate() == Status.SINGULAR_POINT)
				return Status.SINGULAR_POINT;
			Arrays.setAll(x, i -> x[i] + delta[i]);
		} while (Arrays.stream(y).map(val -> val * val).sum() > Util.PREC6);
		return Status.TOL_REACHED;
	}

	private void evalF() {
		y[0] = f[0].eval(x);
		y[1] = f[1].eval(x);
		y[2] = f[2].eval(x);
	}

	private void evalJacobian() {
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				jacobian[i][j] = f[i].diff(j, x);
	}

	private double[][] invert3x3(double[][] m) {
		double[][] inv = new double[3][3];

		double det = m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1])
				- m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0]) + m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]);

		if (Math.abs(det) < Util.PREC12)
			throw new SingularMatrixException();

		inv[0][0] = (m[1][1] * m[2][2] - m[1][2] * m[2][1]) / det;
		inv[0][1] = (m[0][2] * m[2][1] - m[0][1] * m[2][2]) / det;
		inv[0][2] = (m[0][1] * m[1][2] - m[0][2] * m[1][1]) / det;
		inv[1][0] = (m[1][2] * m[2][0] - m[1][0] * m[2][2]) / det;
		inv[1][1] = (m[0][0] * m[2][2] - m[0][2] * m[2][0]) / det;
		inv[1][2] = (m[0][2] * m[1][0] - m[0][0] * m[1][2]) / det;
		inv[2][0] = (m[1][0] * m[2][1] - m[1][1] * m[2][0]) / det;
		inv[2][1] = (m[0][1] * m[2][0] - m[0][0] * m[2][1]) / det;
		inv[2][2] = (m[0][0] * m[1][1] - m[0][1] * m[1][0]) / det;

		return inv;
	}

	private Status iterate() {
		evalJacobian();
		double[][] invJac;
		try {
			invJac = invert3x3(jacobian);
		} catch (SingularMatrixException e) {
			return Status.SINGULAR_POINT;
		}
		evalF();

		delta[0] = -(invJac[0][0] * y[0] + invJac[0][1] * y[1] + invJac[0][2] * y[2]);
		delta[1] = -(invJac[1][0] * y[0] + invJac[1][1] * y[1] + invJac[1][2] * y[2]);
		delta[2] = -(invJac[2][0] * y[0] + invJac[2][1] * y[1] + invJac[2][2] * y[2]);

		logger.log(Level.FINE, "X" + iter + " : " + Arrays.toString(x));
		logger.log(Level.FINE, "F(X" + iter + ") : " + Arrays.toString(y));
		logger.log(Level.FINER, "jacobian : \n" + Arrays.deepToString(jacobian));
		return Status.SUCCESS;
	}

	public double[] getY() {
		return y;
	}

	public double[] getX() {
		return x;
	}

	/**
	 * 
	 * @return les valeurs des fonctions
	 */
	public GVector getF() {
		return new GVector(y);
	}

	/**
	 * 
	 * @return les valeurs des variables
	 */
	public GVector getVar() {
		return new GVector(x);
	}

}
