/*
** Newton1x1Solver.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.optimization;

import org.lgmt.dgl.commons.MaxIterException;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.UnivarFunction;

/**
 * Un solver unidimensionnel rudimentaire.
 * 
 * <p>
 * L'algorithme de résolution est un Newton classique avec calcul de la dérivée.
 * 
 * @navassoc - - - UnivarFunction
 * 
 * @author redonnet
 * 
 */
public class Newton1x1Solver extends AbstractSolver {
	protected UnivarFunction function;
	protected double f;
	protected double var;

	public Newton1x1Solver(UnivarFunction function, double var, int maxIter, double epsilon) {
		this.type = TYPE.NEWTON1x1;
		this.maxIter = maxIter;
		this.epsilon = epsilon;
		this.function = function;
		this.var = var;
		this.f = function.eval(var);
		iter = -1;
	}

	public Newton1x1Solver(UnivarFunction function, double var, int maxIter) {
		this(function, var, maxIter, Util.PREC6);
	}

	/**
	 * Lance la résolution.
	 * 
	 * @throws MaxIterException si le nombre maximum d'itérations allouées à cette
	 *                          résolution a été atteint sans que le critère de
	 *                          convergence soit satisfait.
	 */
	public Status run() {
		double delta;
		do {
			if (iter > maxIter)
				return Status.MAXITER_REACHED;
			iter++;
			f = function.eval(var);
			delta = -f / function.diff(var);
			var += delta;
		} while (Math.pow(f, 2) > this.epsilon);
		return Status.TOL_REACHED;
	}

	/**
	 * 
	 * @return le résultat de la résolution
	 */
	public double getVar() {
		return this.var;
	}

	public double getF() {
		return this.f;
	}
}
