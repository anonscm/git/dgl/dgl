package org.lgmt.dgl.optimization;

import java.util.Arrays;
import java.util.logging.Level;

import org.lgmt.dgl.commons.InvalidArraySizeException;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.Function;
import org.lgmt.dgl.vecmath.GMatrix;
import org.lgmt.dgl.vecmath.GVector;
import org.lgmt.dgl.vecmath.SingularMatrixException;

public class NewtonSolverLambda extends AbstractSolver {
	private Function[] f;
	private double[] y; // y = f(x)
	private double[] x;
	private GMatrix jacobian;
	private double[] delta;

	public NewtonSolverLambda(Function[] functions, int maxIter, double epsilon) {
		this.type = TYPE.NEWTON;
		this.f = functions;
		this.size = f.length;
		for (Function func : f)
			if (func.getSize() != size)
				throw new IllConditionedProblemException();
		this.maxIter = maxIter;
		this.epsilon = epsilon;
		this.y = new double[size];
		this.jacobian = new GMatrix(size, size);
		this.delta = new double[size];
	}

	public NewtonSolverLambda(Function[] functions, int maxIter) {
		this(functions, maxIter, Util.PREC6);
	}

	public NewtonSolverLambda(Function[] functions) {
		this(functions, 100, Util.PREC6);
	}

	public boolean init(double[] x) {
		if (x.length != size)
			throw new InvalidArraySizeException(x.length);
		this.x = Arrays.copyOf(x, size);
		evalF();
		iter = -1;

		return true;
	}

	public Status run() {
		do {
			if (iter > maxIter)
				return Status.MAXITER_REACHED;
			iter++;
			if (iterate() == Status.SINGULAR_POINT)
				return Status.SINGULAR_POINT;
			Arrays.setAll(x, i -> x[i] + delta[i]);
		} while (Arrays.stream(y).map(val -> val * val).sum() > Util.PREC6);
		return Status.TOL_REACHED;
	}

	private void evalF() {
		Arrays.setAll(y, i -> f[i].eval(x));
	}

	private void evalJacobian() {
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				jacobian.setElement(i, j, f[i].diff(j, x));
	}

	private Status iterate() {
		evalJacobian();
		GMatrix invJac = new GMatrix(jacobian);
		try {
			invJac.invert();
		} catch (SingularMatrixException e) {
			return Status.SINGULAR_POINT;
		}
		evalF();
		for (int i = 0; i < size; i++) {
			delta[i] = 0;
			for (int j = 0; j < size; j++)
				delta[i] = delta[i] + invJac.getElement(i, j) * y[j];
			delta[i] = -delta[i];
		}
		logger.log(Level.FINE, "X" + iter + " : " + Arrays.toString(x));
		logger.log(Level.FINE, "F(X" + iter + ") : " + Arrays.toString(y));
		logger.log(Level.FINER, "jacobian : \n" + jacobian.toString());
		return Status.SUCCESS;
	}

	public double[] getY() {
		return y;
	}

	public double[] getX() {
		return x;
	}

	/**
	 * 
	 * @return les valeurs des fonctions
	 */
	public GVector getF() {
		return new GVector(y);
	}

	/**
	 * 
	 * @return les valeurs des variables
	 */
	public GVector getVar() {
		return new GVector(x);
	}

}
