/*
 ** OptimizerNLSimplexe.java
 ** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
 **
 ** This file is part of the DGL software package (DGL stands for
 ** Differential Geometry Library). This software provides mathematic and numeric
 ** tools to work with various curves and surfaces (algebraic, Bézier,
 ** NURBS, ...) with an unified interface.
 **
 ** This software is governed by the CeCILL-C V2 license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 ** 
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-C license and that you accept its terms.
 */
package org.lgmt.dgl.optimization;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.lgmt.dgl.vecmath.GVector;

/**
 * Une classe pour réaliser une optimisation avec un simplexe non-linéaire. Algorithme de Nelder-Mead.
 * 
 * J.A. Nelder and R. Meads : "A simplex method for function minimization", The
 * Computer Journal, vol.7, 1965
 * 
 * D.J. Woods :
 * "An interactive approach for solving multi-objective optimization problems",
 * PhD Thesis, chapter 2, Rice University, 1985
 * 
 * M.B. Subrahmantam :
 * "An extension of the Simplex Method to Constrained Nonlinear Optimization",
 * Journal of optimization Theory and Applications, vol.62, no.2, august 1989
 * 
 * @navassoc - - * Simplexe
 * 
 * @author redonnet
 *
 */
public class NelderMeadOptimizer extends AbstractOptimizer {
	protected List<Simplexe> simplexesList;
	protected GVector var; /** valeur courante des variables */
	private double tolerance;
	private double alpha; // reflexion coeff
	private double gamma; // contraction coeff
	private double beta; // expansion coeff

	public NelderMeadOptimizer(OptProbUnconstrained problem, double tolerance, int maxIter) {
		super(problem, maxIter);
		if (problem.type == OptProb.Type.CONSTRAINED)
			throw (new IllConditionedProblemException("Simplexe algorithm applies only to unconstrained problems"));

		this.tolerance = tolerance;
		this.alpha = 1.0;
		this.gamma = 2.0;
		this.beta = 0.5;

		simplexesList = new ArrayList<Simplexe>();
	}

	public NelderMeadOptimizer(OptProbUnconstrained problem, double tolerance) {
		this(problem, tolerance, AbstractOptimizer.defaultMaxIter);
	}

	public NelderMeadOptimizer(OptProbUnconstrained problem, int maxIter) {
		this(problem, AbstractOptimizer.defaultTolerance, maxIter);
	}

	public NelderMeadOptimizer(OptProbUnconstrained problem) {
		this(problem, AbstractOptimizer.defaultTolerance, AbstractOptimizer.defaultMaxIter);
	}

	public boolean init(GVector var) {
		this.var = new GVector(var);

		Simplexe s1 = new Simplexe(new Vertex(var, problem.objective));
		s1.sort();

		OptimizerLogger.getLogger().log(Level.CONFIG, "Starting simplexe : " + s1.toString());
		simplexesList.add(s1);

		return true;
	}

	/**
	 * Initialisation en utilisant une liste de vecteurs qui correspondent à
	 * autant de points formant le premier simplexe
	 * 
	 * @param varList
	 *            liste des points à utiliser pour former le premier simplexe.
	 *            Doit contenir n+1 vecteurs de taille n (où n est la taille du
	 *            problème)
	 * @return false si la liste des vecteur ne contient pas le nombre
	 *         d'éléments attendus (n+1)<br/>
	 *         false si un des vecteur ne correspond pas à la taille du problème
	 *         (n)<br/>
	 *         true si tout se passe bien
	 */
	public boolean init(ArrayList<GVector> varList) {
		int n = this.getSize();

		if (varList.size() != n + 1)
			return false;

		for (GVector var : varList) {
			if (var.getSize() != n)
				return false;
			if (!init(var))
				return false;
		}

		ArrayList<Vertex> vertexList = new ArrayList<Vertex>();
		for (int i = 0; i < n + 1; i++)
			vertexList.add(new Vertex(varList.get(i), problem.objective));
		Simplexe s1 = new Simplexe(vertexList);
		s1.sort();

		OptimizerLogger.getLogger().log(Level.CONFIG, "Starting simplexe : " + s1.toString());
		simplexesList.add(s1);

		return true;
	}

	private boolean evalCriterion(Simplexe s) {
		double min = s.getFirstVertex().getValue();
		double max = min;
		double value;

		for (int i = 1; i < s.getVerticesCount(); i++) {
			value = s.getVertex(i).getValue();
			min = value < min ? value : min;
			max = value > max ? value : max;
		}

		if (Math.abs(max - min) < this.tolerance) {
			return true;
		}

		return false;
	}

	@Override
	public Status run() {
		this.iter = 1;
		boolean criterion = false;

		Simplexe s, newS;
		Vertex v0, v2last, vlast;

		Vertex vr, ve, vc;

		Vertex vk, vt;

		do {
			OptimizerLogger.getLogger().log(Level.CONFIG, " --- iteration " + iter + " ---");
			s = simplexesList.get(simplexesList.size() - 1);
			newS = new Simplexe(s);

			v0 = s.getFirstVertex();
			v2last = s.get2ndLastVertex();
			vlast = s.getLastVertex();

			vr = s.getReflectedVertex(alpha);

			vk = new Vertex(vr);

			if (vr.getValue() < v2last.getValue()) {
				if (vr.getValue() >= v0.getValue()) {
					OptimizerLogger.getLogger().log(Level.FINE, "Reflected simplexe");
				} else { // vr.getValue() < v0.getValue()
					ve = s.getExpandedVertex(vr, gamma);
					if (ve.getValue() < v0.getValue()) {
						OptimizerLogger.getLogger().log(Level.FINE, "Expanded simplexe");
						vk.set(ve);
					} else {
						OptimizerLogger.getLogger().log(Level.FINE, "Reflected simplexe");
					}
				}
			} else { // vr.getValue() >= v2last.getValue()
				vt = new Vertex(vlast);
				if (vr.getValue() < vt.getValue())
					vt.set(vr);
				vc = s.getContractedVertex(vt, beta);
				if (vc.getValue() <= v2last.getValue()) {
					OptimizerLogger.getLogger().log(Level.FINE, "Contracted simplexe");
					vk.set(vc);
				} else { // vc.getValue() > v2last.getValue()
					OptimizerLogger.getLogger().log(Level.FINE, "Shrinked simplexe");
					newS.reducedShrink();
					GVector vtmp = new GVector(v0.getVector());
					vtmp.add(vlast.getVector());
					vtmp.scale(0.5);
					vk.set(new Vertex(vtmp, problem.objective));
				}
			}

			newS.putVertex(vk, newS.getDimension());
			newS.sort();
			// retrouver le vertex ajouté
			int i = 0;
			while (vk.equals(newS.getVertex(i)) == false)
				i++;

			OptimizerLogger.getLogger().log(Level.FINER, "Replacing vertex number " + i);

			OptimizerLogger.getLogger().log(Level.CONFIG, "New simplexe is :" + newS.toString());

			simplexesList.add(newS);

			iter++;
			criterion = evalCriterion(simplexesList.get(simplexesList.size() - 1));

		} while (criterion == false && iter < maxIter);

		this.var = simplexesList.get(simplexesList.size() - 1).getFirstVertex().getVector();
		this.value = simplexesList.get(simplexesList.size() - 1).getFirstVertex().getValue();

		if (criterion == true)
			return Status.TOL_REACHED;
		else
			return Status.MAXITER_REACHED;
	}

	public GVector getVar() {
		return var;
	}

	public double getTolerance() {
		return tolerance;
	}

	public void setTolerance(double tolerance) {
		this.tolerance = tolerance;
	}

	public Simplexe getLastSimplexes() {
		if (simplexesList.size() > 0)
			return simplexesList.get(simplexesList.size() - 1);
		return null;
	}

	public double getAlpha() {
		return alpha;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	public double getGamma() {
		return gamma;
	}

	public void setGamma(double gamma) {
		this.gamma = gamma;
	}

	public double getBeta() {
		return beta;
	}

	public void setBeta(double beta) {
		this.beta = beta;
	}

}
