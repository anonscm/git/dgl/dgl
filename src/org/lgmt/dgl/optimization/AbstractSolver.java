package org.lgmt.dgl.optimization;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Un solveur générique.
 * 
 * @author redonnet
 *
 */
public abstract class AbstractSolver {
	protected static Logger logger = SolverLogger.getLogger();

	public enum TYPE {
		NEWTON, NEWTON1x1, NEWTON2x2, NEWTON3x3, BRENT
	};

	protected TYPE type;
	protected int size;
	protected int iter;
	protected int maxIter;
	protected double epsilon;

	/**
	 * 
	 * @return le nombre maximum d'itération du solveur
	 */
	public int getMaxIter() {
		return maxIter;
	}

	/**
	 * 
	 * @return la taille du problème.
	 */
	public int getSize() {
		return size;
	}

	/**
	 * 
	 * @return true si le log est activé, false dans le cas contraire
	 */
	public boolean isLoggingOn() {
		return SolverLogger.isLoggingOn();
	}

	/**
	 * Active ou désactive le log
	 * 
	 * @param logging
	 */
	public void setLogging(boolean logging) {
		SolverLogger.setLogging(logging);
	}

	/**
	 * Définit le niveau de log du Solver (voir java.util.logging.Level)
	 * 
	 * @param level
	 */
	public void setLogLevel(Level level) {
		SolverLogger.setLogLevel(level);
	}

	/**
	 * 
	 * @return la tolérance définie pour ce problème
	 */
	public double getEpsilon() {
		return epsilon;
	}

}
