/*
** Simplexe.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.optimization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

import org.lgmt.dgl.commons.IndexOutOfBoundsException;
import org.lgmt.dgl.functions.MultivarEvaluable;
import org.lgmt.dgl.functions.MultivarFunction;
import org.lgmt.dgl.vecmath.GVector;

/**
 * Une classe pour représenter un simplexe.
 * 
 * <p>
 * Un simplexe est un volume de l'espace des variables. Les sommets du simplexe
 * sont définis par n+1 vertex, où n est le nombre de variables (et donc aussi
 * le nombre de composantes du vecteur des vertex)
 * </p>
 * 
 * TODO : passer les paramètres alpha, gamma et beta en paramètres (en static ?)
 * 
 * @navassoc - - * Vertex
 * @navassoc - - - MultivarFunction
 * 
 * @author redonnet
 * 
 */
public class Simplexe{
	/** la dimension du simplexe. Un simplexe de dimension n contient n+1 vertex */
	private int dimension;
	/** le nombre de vertices du simplexe. En dimension n le simplexe doit contenir n+1 vertex */
	private int verticesCount;
	/** la liste des vertex du simplexe */
	private List<Vertex> vertices;
	/** la fonction objectif associée à ce simplexe */
	private MultivarFunction f;

	/**
	 * Crée un simplexe unitaire à partir du vertex passé en paramètre.
	 * 
	 * <p>
	 * Le nouveau simplexe est construit à partir du vertex passé en paramètre
	 * en de n autres vertex (n étant la taille du vertex passé en paramètre et
	 * la dimension du simplexe) définis par : v_i = v + e_i pour i=1...n où e_i
	 * est un vecteur unitaire dans la dimension i
	 * </p>
	 * 
	 * @param v
	 *            le vertex
	 */
	public Simplexe(Vertex v){
		this.dimension = v.getSize();
		this.verticesCount = dimension + 1;
		this.f = v.getFunction();
		this.vertices = new ArrayList<Vertex>(verticesCount);
		vertices.add(new Vertex(v));
		GVector vtmp = new GVector(dimension);
		GVector unitV = new GVector(dimension);
		for(int i=1 ; i<verticesCount ; i++){
			unitV.zero();
			unitV.setElement(i-1, 1.0);
			vtmp.add(v.getVector(),unitV);
			vertices.add(new Vertex(vtmp, this.f));
		}
	}
	
	/**
	 * Crée un simplexe à partir d'une liste de vertex.
	 * 
	 * <p>
	 * ATTENTION : Aucune vérification n'est faite sur la validité du simplexe.
	 * En particulier, le simplexe ne sera pas valable si :
	 * <ul>
	 * <li>plusieurs vertex sont alignés dans la même dimension</li>
	 * <li>le nombre de vertex ne correspond pas à leur taille + 1</li>
	 * </ul>
	 * </p>
	 * 
	 * @param vertices
	 *            une liste de vertex
	 */
	public Simplexe(List<Vertex> vertices){
		this.verticesCount = vertices.size();
		this.dimension = verticesCount-1;
		this.vertices = vertices;
		this.f = vertices.get(0).getFunction();
	}
	
	public Simplexe(Simplexe s){
		this.dimension = s.dimension;
		this.verticesCount = s.verticesCount;
		this.vertices = new ArrayList<Vertex>(s.vertices);
		this.f = s.f;
	}
	
	public Simplexe(Simplexe s, MultivarFunction f){
		this.dimension = s.dimension;
		this.verticesCount = s.verticesCount;
		this.f = f;
		this.vertices = new ArrayList<Vertex>(s.vertices);
		for(int i=0 ; i<this.verticesCount ; i++){
			this.vertices.set(i, new Vertex(s.getVertex(i), f));
		}
	}

	@Override
	public String toString(){
		String str = new String();
		for(int i=0 ; i<verticesCount ; i++){
			str = str+"\n vertex "+i+" : "+vertices.get(i).toString();
		}
		return str;
	}

	/**
	 * Remplace un vertex du simplexe.
	 * 
	 * <p>
	 * Le vertex situé à la position spécifiée par l'index est remplacé par le
	 * vertex passé en paramètre. La numérotation commence à 0.
	 * </p>
	 * 
	 * @throws IndexOutOfBoundsException
	 *             si la valeur de l'index est négative ou supérieure à size
	 * 
	 */
	public void putVertex(Vertex vertex, int index){
		if (index < 0 || index > dimension)
			throw(new IndexOutOfBoundsException(index));
		vertices.set(index, vertex);
	}
	
	/**
	 * Retourne la dimension du simplexe.
	 * 
	 * @return la dimension du simplexe
	 */
	public int getDimension(){
		return this.dimension;
	}

	/**
	 * Retourne le nombre de vertex du simplexe.
	 * 
	 * @return le nombre de vertex du simplexe
	 */
	public int getVerticesCount() {
		return verticesCount;
	}

	/**
	 * Retourne un vertex du simplexe.
	 * 
	 * @param index
	 *            désigne la position du vertex à retourner.
	 * @return le vertex situé à la position désignée par index
	 * @throws IndexOutOfBoundsException
	 *             si la valeur de l'index est négative ou supérieure à size
	 */
	public Vertex getVertex(int index){
		if(index < 0 || index > dimension)
			throw(new IndexOutOfBoundsException(index));
		return vertices.get(index);
	}
	
	/**
	 * Retourne la liste des vertex de ce simplexe.
	 * 
	 * @return la liste des vertex de ce simplexe.
	 */
	public List<Vertex> getVertices() {
		return vertices;
	}

	/**
	 * Retourne le premier vertex du simplexe.
	 * 
	 * @return le premier vertex du simplexe
	 * 
	 */
	public Vertex getFirstVertex(){
		return vertices.get(0);
	}
	
	/**
	 * Retourne le dernier vertex du simplexe.
	 * 
	 * @return le dernier vertex du simplexe
	 */
	public Vertex getLastVertex(){
		return vertices.get(dimension);
	}

	/**
	 * Retourne l'avant-dernier vertex du simplexe.
	 * 
	 * @return l'avant-dernier vertex du simplexe
	 */
	public Vertex get2ndLastVertex(){
		return vertices.get(dimension-1);
	}

	/**
	 * Trie les vertex du simplexe dans l'ordre croissant des valeurs de ses vertex.
	 * 
	 */
	public void sort(){
		Collections.sort(this.vertices);
	}

	/**
	 * Retourne le centroide du simplexe.
	 * 
	 * <p>
	 * Le centroid du simplexe est défini par
	 * (1/verticesCount) sum_(i=0)^size vertex(i)
	 * </p>
	 * @return le centroid
	 */
	public Vertex getCentroid(){
		GVector centroidVector = new GVector(dimension);
		for(int i=0 ; i<dimension ; i++){ //pour chaque coordonnée
			for(int j=0 ; j<verticesCount ; j++){ //pour chaque élément de la liste
				centroidVector.setElement(i, centroidVector.getElement(i)+vertices.get(j).getVector().getElement(i));
			}
			centroidVector.setElement(i, centroidVector.getElement(i)/verticesCount);
		}
		return new Vertex(centroidVector, f);
		
	}
	
	/**
	 * Retourne le barycentre des vecteurs du simplexe en excluant le dernier.
	 * 
	 * @return le vecteur calculé
	 */
	private GVector getReducedCentroidVector(){
		int n = verticesCount-1; //nombre de vertex à prendre en compte pour le calcul du barycentre

		GVector centroid = new GVector(dimension);
		for(int i=0 ; i<dimension ; i++){ //pour chaque coordonnée
			for(int j=0 ; j<n ; j++){ //pour chaque élément de la liste
				centroid.setElement(i, centroid.getElement(i)+vertices.get(j).getVector().getElement(i));
			}
			centroid.setElement(i, centroid.getElement(i)/n);
		}
		return centroid;
	}

	/**
	 * Retourne le centroid du simplexe en excluant le dernier vertex.
	 * 
	 * @return le vertex calculé
	 */
	public Vertex getReducedCentroid(){
		return new Vertex(getReducedCentroidVector(), this.f);
	}


	
	public Vertex getReflectedVertex(double alpha){		
		GVector centroid = this.getReducedCentroidVector();
		centroid.scale(1+alpha);
		
		GVector worst = new GVector(vertices.get(vertices.size()-1).getVector());
		worst.scale(-alpha);

		GVector vReflect = new GVector(centroid);
		vReflect.add(worst);
		
		return (new Vertex(vReflect, f));
	}
	
	public Vertex getExpandedVertex(double alpha, double gamma){		
		GVector vr = new GVector(this.getReflectedVertex(alpha).getVector());
		vr.scale(gamma);
		
		GVector centroid = new GVector(this.getReducedCentroidVector());
		centroid.scale(1-gamma);
		
		GVector vExpanded = new GVector(vr);
		vExpanded.add(centroid);
		
		return (new Vertex(vExpanded, f));
	}

	public Vertex getExpandedVertex(Vertex reflected, double gamma){		
		GVector vr = new GVector(reflected.getVector());
		vr.scale(gamma);
		
		GVector centroid = new GVector(this.getReducedCentroidVector());
		centroid.scale(1-gamma);
		
		GVector vExpanded = new GVector(vr);
		vExpanded.add(centroid);
		
		return (new Vertex(vExpanded, f));
	}
	
	public Vertex getContractedVertex(double alpha, double beta){
		GVector vt;
		
		Vertex vr = this.getReflectedVertex(alpha);
		Vertex vn = this.getLastVertex();
		
		if (vr.getValue() < vn.getValue()) // vt est la meilleure approximation entre vr et vn
			vt = new GVector(vr.getVector());
		else
			vt = new GVector(vn.getVector());
		vt.scale(beta);
		
		GVector centroid = new GVector(this.getReducedCentroidVector());
		centroid.scale(1-beta);
		
		GVector vContracted = new GVector(vt);
		vContracted.add(centroid);
		
		return (new Vertex(vContracted, f));
	}

	public Vertex getContractedVertex(Vertex vertex, double beta){
		GVector vt = new GVector(vertex.getVector());
		
//		Vertex vn = this.getLastVertex();
		
		// FIXME BUg ici !!!!
		
//		if (vertex.getValue() < vn.getValue()) // vt est la meilleure approximation entre vr et vn
//			vt = new GVector(vertex.getVector());
//		else
//			vt = new GVector(vn.getVector());
		vt.scale(beta);
		
		GVector centroid = new GVector(this.getReducedCentroidVector());
		centroid.scale(1-beta);
		
		GVector vContracted = new GVector(vt);
		vContracted.add(centroid);
		
		return (new Vertex(vContracted, f));
	}

	/**
	 * Réduit la taille du simplexe en gardant le premier vertex comme point fixe.
	 * 
	 */
	public void shrink(){
		GVector vec;
		this.sort();
		
		for (int k=1 ; k<verticesCount ; k++){
			vec = new GVector(vertices.get(0).getVector());
			vec.add(vertices.get(k).getVector());
			vec.scale(0.5);
			vertices.set(k, new Vertex(vec, f));
		}
	}

	/**
	 * Réduit la taille du simplexe en gardant le premier vertex comme point
	 * fixe et en excluant le dernier vertex.
	 * 
	 */
	public void reducedShrink(){
		GVector vec;
		this.sort();
		
		for (int k=1 ; k <= dimension-1 ; k++){
			vec = new GVector(vertices.get(0).getVector());
			vec.add(vertices.get(k).getVector());
			vec.scale(0.5);
			vertices.set(k, new Vertex(vec, f));
		}
	}
	
	public boolean isFeasible(OptProb problem){
		if (problem.type == OptProb.Type.UNCONSTRAINED)
			return true;
		for (Vertex v : vertices)
			if (!v.isFeasible(problem))
				return false;

		return true;
	}
	
	public static Simplexe findFeasible(GVector var, OptProb problem) {
		if (problem.type == OptProb.Type.UNCONSTRAINED){
			return new Simplexe(new Vertex(var, problem.objective));
		}
		
		final OptProbConstrained cProblem = (OptProbConstrained)problem;
		
		MultivarFunction f = new MultivarFunction(cProblem.nbVar,
				new MultivarEvaluable() {
					@Override
					public double eval(GVector var) {
						double val = 0.0;
						double tmp;
						for (Constraint c : cProblem.constraints) {
							tmp = c.eval(var);
							if (c.getType() == Constraint.TYPE.LEQ && tmp > 0)
								val = val + Math.pow(tmp, 2.0);
						}
						return val;
					}
				}
		);
		OptProbUnconstrained uncProblem = new OptProbUnconstrained(f, cProblem.nbVar);

		NelderMeadOptimizer nls = new NelderMeadOptimizer(uncProblem, 1E-8);
		nls.init(var);
		Status status = nls.run();

		if(status == Status.TOL_REACHED){
			Simplexe s = new Simplexe(nls.getLastSimplexes(), problem.getObjective());
			
			if (s.isFeasible(problem))
				return s;
		}

		OptimizerLogger.getLogger().log(Level.WARNING, "Can't find feasible simplexe");
		
		return null;
	}


}