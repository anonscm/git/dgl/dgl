/*
** Newton.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.optimization;

import java.util.List;
import java.util.logging.Level;

import org.lgmt.dgl.commons.MaxIterException;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.MultivarFunction;
import org.lgmt.dgl.vecmath.GMatrix;
import org.lgmt.dgl.vecmath.GVector;
import org.lgmt.dgl.vecmath.SingularMatrixException;

/**
 * Un solver rudimentaire.
 * 
 * <p>
 * L'algorithme de résolution est un Newton-Raphson classique avec calcul des
 * dérivées.
 * 
 * Le critère d'arrêt choisi est la somme des résidus au carré.
 * 
 * La phase de résolution peut être monitorée en utilisant un Logger, mais comme
 * cela est assez couteux en termes de temps de calcul, le logger est désactivé
 * par défaut.
 * 
 * @navassoc - - * MultivarFunction
 * @depend - - - Status
 * 
 * @author redonnet
 * 
 */
public class NewtonSolver extends AbstractSolver {
	private List<MultivarFunction> functions;
	private GVector f;
	private GVector var;
	private GMatrix jacobian;
	private GVector delta;

	/**
	 * Construit un nouveau solver.
	 * 
	 * @param functions
	 * @param maxIter   le nombre maximum d'itération allouées à cette résolution
	 * @param epsilon   la tolerance de résolution
	 */
	public NewtonSolver(List<MultivarFunction> functions, int maxIter, double epsilon) {
		this.type = TYPE.NEWTON;
		this.size = 0;
		for (MultivarFunction f : functions)
			if (f.getNbvar() > size)
				size = f.getNbvar();
		if (functions.size() != size)
			throw new IllConditionedProblemException();
		this.maxIter = maxIter;
		this.epsilon = epsilon;
		this.functions = functions;
		f = new GVector(size);
		jacobian = new GMatrix(size, size);
		delta = new GVector(size);
	}

	/**
	 * Construit un nouveau solver.
	 * 
	 * Par défaut la tolérance est fixée à 1E-6
	 * 
	 * @param functions
	 * @param maxIter   le nombre maximum d'itération allouées à cette résolution
	 */
	public NewtonSolver(List<MultivarFunction> functions, int maxIter) {
		this(functions, maxIter, Util.PREC6);
	}

	/**
	 * Construit un nouveau solver.
	 * 
	 * Par défaut la tolérance est fixée à 1E-6 et le nombre maxi d'itérations est
	 * fixé à 100;
	 * 
	 * @param functions
	 */
	public NewtonSolver(List<MultivarFunction> functions) {
		this(functions, 100, Util.PREC6);
	}

	/**
	 * Initialise le solveur en définissant un point de départ.
	 * 
	 * @param var les variables d'initialisation
	 * @return false si la taille du vecteur <b>var</b> est différente de la taille
	 *         du problème, true dans le cas contraire
	 */
	public boolean init(GVector var) {
		if (var.getSize() != size)
			return false;

		this.var = new GVector(var);
		evalF();
		iter = -1;
		return true;
	}

	/**
	 * Lance la résolution.
	 * 
	 * @throws MaxIterException si le nombre maximum d'itérations allouées à cette
	 *                          résolution a été atteint sans que le critère de
	 *                          convergence soit satisfait.
	 */
	public Status run() {
		if (this.functions.size() != this.size)
			return Status.INVALID;
		do {
			if (iter > maxIter)
				return Status.MAXITER_REACHED;
			iter++;
			if (iterate() == Status.SINGULAR_POINT)
				return Status.SINGULAR_POINT;
			var.add(delta);
		} while (f.normSquared() > epsilon);
		return Status.TOL_REACHED;
	}

	private void evalF() {
		for (int i = 0; i < size; i++)
			f.setElement(i, functions.get(i).eval(var));
	}

	private void evalJacobian() {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++)
				jacobian.setElement(i, j, functions.get(i).diff(var, j));
		}
	}

	private Status iterate() {
		evalJacobian();
		GMatrix j1 = new GMatrix(jacobian);
		try {
			j1.invert();
		} catch (SingularMatrixException e) {
			return Status.SINGULAR_POINT;
		}
		evalF();
		GVector f1 = new GVector(f);
		f1.negate();
		delta.mul(j1, f1);
		logger.log(Level.FINE, "X" + iter + " : " + var.toString());
		logger.log(Level.FINE, "F(X" + iter + ") : " + f.toString());
		logger.log(Level.FINER, "jacobian : \n" + jacobian.toString());

		return Status.SUCCESS;
	}

	/**
	 * 
	 * @return les valeurs des fonctions
	 */
	public GVector getF() {
		return f;
	}

	/**
	 * Alias for getF().get(int)
	 * 
	 * @return la valeur d'une fonction
	 */
	public double getF(int index) {
		return f.get(index);
	}

	/**
	 * 
	 * @return les valeurs des variables
	 */
	public GVector getVars() {
		return var;
	}

	/**
	 * 
	 * @return les valeurs des variables
	 * @deprecated Use getVars() instead
	 */
	public GVector getVar() {
		return var;
	}

	/**
	 * Alias for getVar().get(int)
	 * 
	 * @return la valeur d'une variable
	 */
	public double getVar(int index) {
		return var.get(index);
	}
}
