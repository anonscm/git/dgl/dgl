package org.lgmt.dgl.optimization;

import org.lgmt.dgl.vecmath.GVector;

public class NonFeasiblePointException extends RuntimeException {

	private static final long serialVersionUID = 6596804027024175088L;

	/**
	 * Constructeur par défaut.
	 *
	 */
	public NonFeasiblePointException() {
		super();
	}

	/**
	 * Constructeur personnalisé.
	 * 
	 * @param var point invalide
	 */
	public NonFeasiblePointException(GVector var) {
		super(new String("Point "+var+" is not feasible"));
	}

}
