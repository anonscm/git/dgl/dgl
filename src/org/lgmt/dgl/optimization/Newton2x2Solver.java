package org.lgmt.dgl.optimization;

import java.util.logging.Level;

import org.lgmt.dgl.commons.MaxIterException;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.BivarFunction;
import org.lgmt.dgl.vecmath.Matrix2d;
import org.lgmt.dgl.vecmath.SingularMatrixException;
import org.lgmt.dgl.vecmath.Vector2d;

public class Newton2x2Solver extends AbstractSolver {
	private BivarFunction f1;
	private BivarFunction f2;
	private Vector2d f;
	private Vector2d var;
	private Matrix2d jacobian;
	private Vector2d delta;

	/**
	 * Construit un nouveau solver de taille 2.
	 * 
	 * @param maxIter   le nombre maximum d'itération allouées à cette résolution
	 * @param epsilon la tolérance de résolution
	 */
	public Newton2x2Solver(BivarFunction f1, BivarFunction f2, int maxIter, double epsilon) {
		this.type = TYPE.NEWTON2x2;
		this.size = 2;
		this.f1 = f1;
		this.f2 = f2;
		this.maxIter = maxIter;
		this.epsilon = epsilon;
		f = new Vector2d();
		jacobian = new Matrix2d();
		delta = new Vector2d();
	}

	/**
	 * Construit un nouveau solver de taille 2.
	 * 
	 * Par défaut la tolérance est fixée à 1E-6
	 * 
	 * @param maxIter le nombre maximum d'itération allouées à cette résolution
	 */
	public Newton2x2Solver(BivarFunction f1, BivarFunction f2, int maxIter) {
		this(f1, f2, maxIter, Util.PREC6);
	}

	/**
	 * Construit un nouveau solver de taille 2.
	 * 
	 * Par défaut la tolérance est fixée à 1E-6 et le nombre maxi d'itérations est
	 * fixé à 100;
	 * 
	 * @param f1
	 * @param f2
	 */
	public Newton2x2Solver(BivarFunction f1, BivarFunction f2) {
		this(f1, f2, 100, Util.PREC6);
	}

	/**
	 * Initialise le solveur en définissant un point de départ.
	 * 
	 * @param var1 la valeur initiale de la première variable
	 * @param var2 la valeur initiale de la deuxième variable
	 * 
	 * @return true
	 */
	public boolean init(double var1, double var2) {
		this.var = new Vector2d(var1, var2);
		evalF();
		iter = -1;
		return true;
	}

	/**
	 * Lance la résolution.
	 * 
	 * @throws MaxIterException si le nombre maximum d'itérations allouées à cette
	 *                          résolution a été atteint sans que le critère de
	 *                          convergence soit satisfait.
	 */
	public Status run() {
		do {
			if (iter > maxIter)
				return Status.MAXITER_REACHED;
			iter++;
			if (iterate() == Status.SINGULAR_POINT)
				return Status.SINGULAR_POINT;
			var.add(delta);
		} while (f.lengthSquared() > epsilon);
		return Status.TOL_REACHED;
	}

	private void evalF() {
		f.x = f1.eval(var.x, var.y);
		f.y = f2.eval(var.x, var.y);
	}

	private void evalJacobian() {
		jacobian.m00 = f1.diffu(var.x, var.y);
		jacobian.m01 = f1.diffv(var.x, var.y);
		jacobian.m10 = f2.diffu(var.x, var.y);
		jacobian.m11 = f2.diffv(var.x, var.y);
	}

	private Status iterate() {
		evalJacobian();
		Matrix2d j1 = new Matrix2d(jacobian);
		try {
			j1.invert();
		} catch (SingularMatrixException e) {
			return Status.SINGULAR_POINT;
		}
		evalF();
		delta.x = -(j1.m00 * f.x + j1.m01 * f.y);
		delta.y = -(j1.m10 * f.x + j1.m11 * f.y);
		logger.log(Level.FINE, "X" + iter + " : " + var.toString());
		logger.log(Level.FINE, "F(X" + iter + ") : " + f.toString());
		logger.log(Level.FINER, "jacobian : \n" + jacobian.toString());
		return Status.SUCCESS;
	}

	/**
	 * 
	 * @return la valeur des fonctions
	 */
	public Vector2d getF() {
		return f;
	}

	/**
	 * 
	 * @return la valeur des variables
	 */
	public Vector2d getVar() {
		return var;
	}
}
