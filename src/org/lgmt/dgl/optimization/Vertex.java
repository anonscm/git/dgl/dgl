/*
** Vertex.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
/**
 * 
 */
package org.lgmt.dgl.optimization;

import org.lgmt.dgl.functions.MultivarFunction;
import org.lgmt.dgl.vecmath.GVector;
import org.lgmt.dgl.vecmath.MismatchedSizeException;

/**
 * Une classe pour représenter un vertex.
 * 
 * <p>
 * Un vertex est composé d'un ensemble de valeurs des variables et de la valeur
 * de la fonction en ce point.
 * </p><p>
 * Implémente java.lang.Comparable en comparant les valeurs de la fonction entre
 * deux vertex.
 * </p>
 * 
 * @navassoc - - - MultivarFunction
 * 
 * @author redonnet
 * 
 */
public class Vertex implements java.lang.Comparable<Vertex>{
	private int size;
	private GVector vector;
	private MultivarFunction function;
	private double value;

	/**
	 * Crée un Vertex à partir d'un vecteur et d'une fonction de plusieurs
	 * variables.
	 * 
	 * @param v
	 *            le vecteur
	 * @param f
	 *            la fonction
	 * 
	 * @throws MismatchedSizeException
	 *             si la fonction et le vecteur n'ont pas le même nombre de
	 *             variables
	 */
	public Vertex(GVector v, MultivarFunction f){
		if (v.getSize() != f.getNbvar())
			throw (new MismatchedSizeException("Erreur de taille !"));
		this.size = v.getSize();
		this.vector = new GVector(v);
		this.function = f;
		this.value = f.eval(v);
	}

	public Vertex(Vertex v){
		this(v.getVector(), v.getFunction());
	}

	public Vertex(Vertex v, MultivarFunction f){
		this(v.getVector(), f);
	}

	@Override
	public String toString(){
		String str = new String("(");
		for(int i=0 ; i<size ; i++){
				str = str+String.format("% .4f", vector.getElement(i));
				if(i != size-1)
					str = str+", ";
		}
		str = str+") => "+String.format("% .4e", value);
		return str;
	}

	public void set(Vertex v){
		this.size = v.size;
		this.vector = new GVector(v.vector);
		this.function = v.function;
		this.value = v.value;
	}
	
	public void penalize(){
		this.value = Double.MAX_VALUE;
	}
	
	public double getValue(){
		return this.value;
	}
	
	public GVector getVector() {
		return vector;
	}

	public int getSize() {
		return size;
	}

	public MultivarFunction getFunction() {
		return function;
	}

	/**
	 * Compare deux vertex.
	 * 
	 * <p>
	 * Retourne -1, 0 ou 1 selon que other soit (respectivement) supérieur, égal
	 * ou inférieur à this
	 * </p>
	 */
	@Override
	public int compareTo(Vertex other) {
		double value1 = other.getValue();
		double value2 = this.getValue(); 
		if (value1 > value2)  return -1; 
		else if(value1 == value2) return 0; 
		else return 1;
	}

	/**
	 * Teste si deux vertex sont égaux.
	 * 
	 * <p>
	 * Retourne vrai si les deux vertex sont construits à partir du même
	 * vecteur, faux dans le cas contraire.
	 * </p>
	 * 
	 * @param other
	 * @return true or false
	 */
	public boolean equals(Vertex other){
		if(this.vector.equals(other.vector))
			return true;
		
		return false;
	}
	
	public boolean isFeasible(OptProb problem){
		if(problem.type == OptProb.Type.UNCONSTRAINED)
			return true;
		for(Constraint c : problem.constraints)
			if(!c.isValid(this.vector)){
				System.out.println("Vertex "+this.toString()+" not feasible");
				System.out.println("Constraint "+c.toString()+" = "+c.eval(vector));
				return true;
			}
		return true;
	}

}
