/*
** OptProbConstrained.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.optimization;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.lgmt.dgl.functions.MultivarFunction;
import org.lgmt.dgl.vecmath.GVector;

/**
 * Une classe pour définir un problème d'optimisation sous contraintes.
 * 
 * @author redonnet
 *
 */
public class OptProbConstrained extends OptProb{
	/**
	 * Définit un nouveau problème d'optimisation sous contraintes.
	 * 
	 * Les contraintes peuvent être saisies directement ou par la suite en
	 * utilisant addConstraint(). Attention, si constraints est null le problème
	 * est (par souci de cohérence) définit comme un problème sans contraintes.
	 * 
	 * @param objective
	 * @param constraints
	 * @param nbVar
	 */
	public OptProbConstrained(MultivarFunction objective, List<Constraint> constraints, int nbVar) {
		if (constraints == null)
			throw new IllConditionedProblemException(new String("Constrained optimization problem should define at least one constraint"));
		this.type = Type.CONSTRAINED;
		this.objective = objective;
		this.constraints = constraints;
		this.nbVar = nbVar;
	}

	public OptProbConstrained(MultivarFunction objective, Constraint constraint, int nbVar) {
		if (constraint == null)
			throw new IllConditionedProblemException(new String("Constrained optimization problem should define at least one constraint"));
		this.type = Type.CONSTRAINED;
		this.objective = objective;
		this.constraints = new ArrayList<Constraint>(1);
		this.addConstraint(constraint);
		this.nbVar = nbVar;
	}

	/**
	 * Ajoute une contrainte au problème.
	 * 
	 * Si le problème était pécédemment définit sans contrainte, il devient un
	 * problème sous contraintes. La valeur par défaut de la tolérance pour
	 * l'évaluation des contraintes (1E-6) peut alors être adaptée en utilisant
	 * setCtol()
	 * 
	 * @param constraint
	 */
	public void addConstraint(Constraint constraint){
		constraints.add(constraint);
	}

	/**
	 * Teste si le point var est dans le domaine admissible
	 * 
	 * Si le problème n'est pas contraint, retourne systématiquement true
	 * 
	 * @param var
	 * @return true of false
	 * 
	 */
	@Override
	public boolean isFeasible(GVector var){
		for(int i=0 ; i<constraints.size() ; i++)
			if(!constraints.get(i).isValid(var))
				return false;
		return true;
	}
	
	/**
	 * Trouve a point valide en cherchant depuis un point de départ
	 * 
	 */
	public GVector findFeasiblePoint(GVector var) {
		if (this.isFeasible(var))
			return var;
		else {
			GVector feasible = new GVector(Simplexe.findFeasible(var, this).getLastVertex().getVector());
			if (isFeasible(feasible))
				return feasible;
		}
		OptimizerLogger.getLogger().log(Level.WARNING, "Can't find feasible point");
		return null;
	}

	@Override
	public List<Constraint> getConstraints(){
		return constraints;
	}
	
	@Override
	public int getConstraintsCount(){
		return constraints.size();
	}
	
	@Override
	public Constraint getConstraint(int i){
		return constraints.get(i);
	}
}
