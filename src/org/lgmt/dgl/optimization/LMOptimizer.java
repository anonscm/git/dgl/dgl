package org.lgmt.dgl.optimization;

import java.util.logging.Level;

import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.MultivarFunction;
import org.lgmt.dgl.vecmath.GMatrix;
import org.lgmt.dgl.vecmath.GVector;
import org.lgmt.dgl.vecmath.SingularMatrixException;

/**
 * A Levenberg-Marquardt optimizer for best fitting discrete data.
 * 
 * <p>
 * Relaxation factor not integrated yet.
 * <p>
 * Starting point should be close to solution to avoid matrix inversion
 * exception.
 * <p>
 * Solves the following optimization problem
 * <center>Min \sum_{i=0}^n \| y_i - f(x_i,p) * \| ^2 </center>
 * where 
 * <itemize>
 * <li>(x_i,y_i) are discrete data to fit</li>
 * <li>p is the vector of parameters to optimize</li>
 * </itemize>
 * 
 * <p>
 * Starting from initial p vector, at each iteration q vector is added to p and
 * <center>q = (J J^T)^{-1} J^T (y - f(x, p))</center>
 * where
 * <itemize>
 * <li>y is vector of y_i data</li>
 * <li>f(x,p) is vector of f(x_i,p) data</li>
 * <li>J is jacobian of f(x,p) against p</li>
 * </itemize>
 * 
 * @author redonnet
 *
 */
public class LMOptimizer extends AbstractOptimizer {
	/* Data */
	private GVector x;
	private GVector y;

	/* Model */
	private MultivarFunction model; // The first variable is always x_i while others are parameters
	private GVector var; // vector of parameters
	private int n; // model size = nb of parameters

	public LMOptimizer(GVector x, GVector y, MultivarFunction f) {
		this.x = x;
		this.y = y;
		this.model = f;
		this.n = f.getNbvar() - 1;
	}

	public boolean init(GVector var) {
		this.var = new GVector(var);
		OptimizerLogger.getLogger().log(Level.CONFIG, "Starting parameters : " + var.toString());
		return true;
	}

	public Status run() {
		GVector q = new GVector(n);
		GVector q1 = new GVector(n);
		GMatrix j, j1, j2;
		GMatrix jjt = new GMatrix(n, n);
		GVector yp = new GVector(n);

		q.setElement(0, 1); // to be sure to enter in while loop
		while (q.norm() > Util.PREC9) {
			yp = new GVector(y);
			yp.sub(fp(x, var));
			j = jac(x, var);
			j1 = new GMatrix(j);
			j1.transpose();
			q1.mul(j1, yp);
			j2 = new GMatrix(j);
			j2.transpose();
			jjt.mul(j2, j);
			try {
				jjt.invert();
			} catch (SingularMatrixException e) {
				OptimizerLogger.getLogger().log(Level.SEVERE, "Cannot invert matrix");
				return Status.FAILURE;
			}
			q.mul(jjt, q1);
			var.add(q);
		}

		return Status.SUCCESS;
	}

	private GVector getVarX(double xValue, GVector var) {
		double[] x = new double[n + 1];
		x[0] = xValue;
		for (int i = 1; i < n + 1; i++) {
			x[i] = var.get(i - 1);
		}
		return new GVector(x);
	}

	private GVector fp(GVector x, GVector p) {
		GVector res = new GVector(x.getSize());

		for (int i = 0; i < x.getSize(); i++) {
			double[] xp = new double[p.getSize() + 1];
			xp[0] = x.get(i);
			for (int j = 0; j < p.getSize(); j++) {
				xp[j + 1] = p.get(j);
			}
			res.setElement(i, model.eval(new GVector(xp)));
		}
		return res;
	}

	private GMatrix jac(GVector x, GVector p) {
		int nX = x.getSize(); // nombre de lignes = nombre d'échatillons

		double[][] res = new double[nX][n];
		for (int i = 0; i < nX; i++) {
			for (int j = 0; j < n; j++) {
				GVector varX = getVarX(x.get(i), p);
				res[i][j] = model.diff(varX, j + 1);
			}
		}
		return new GMatrix(res);
	}

	public GVector getParameters() {
		return var;
	}

	public GVector getVar() {
		return var;
	}
}
