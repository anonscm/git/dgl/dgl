package org.lgmt.dgl.optimization;

/**
 * Une énumeration pour gérer les valeurs de retour possibles des algorithmes
 * numériques itératifs (solveurs et optimisations).
 * 
 * <p>
 * Les valeurs de retour positives fournissent une solution. La qualité de cette
 * solution dépend de la valeur :
 * <ul>
 * <li>SUCCESS : mention generique de succès</li>
 * <li>TOL_REACHED : la résoution s'est déroulé normalement et la tolérance est
 * atteinte</li>
 * <li>MAXTIME_REACHED : la résolution a atteint le temps maximum imparti</li>
 * <li>MAXITER_REACHED : la résolution a atteint le nombre maximum d'itération
 * allouées</li>
 * </ul>
 * 
 * <p>
 * Les valeurs de retour négatives ne fournissent pas de résultat exploitable :
 * <ul>
 * <li>FAILURE : mention générique d'échec</li>
 * <li>INVALID : problème mal conditionné (par exemple si le nombre de fonctions
 * fournies ne correspond pas à la taille forunie)</li>
 * <li>SINGULAR_POINT : la résolution a échoué sur un point singulier, c'est à
 * dire que la matrice jacobienne n'a pas pu être inversée</li>
 * </ul>
 * 
 * <p>
 * La valeur 0 (CONTINUE) indique que la résolution doit être poursuivie.
 * 
 * @author redonnet
 * 
 */
public enum Status {
	CONTINUE(0), SUCCESS(1), TOL_REACHED(2), MAXTIME_REACHED(3), MAXITER_REACHED(4), FAILURE(-1), INVALID(-2),
	SINGULAR_POINT(-3);

	private int value;

	Status(int val) {
		this.value = val;
	}

	public int getValue() {
		return this.value;
	}
}