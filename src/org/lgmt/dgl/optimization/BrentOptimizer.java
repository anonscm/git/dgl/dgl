package org.lgmt.dgl.optimization;

import static java.lang.Math.abs;
import static java.lang.Math.min;
import static java.lang.Math.pow;

import java.util.ArrayList;

import org.lgmt.dgl.commons.Interval;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.UnivarFunction;

/**
 * Un optimiseur unidimensionnel.
 * 
 * La méthode utilisée est la méthode de Brent. (cf Brent, R. P. (1973),
 * "Chapter 4", Algorithms for Minimization without Derivatives, Englewood
 * Cliffs, NJ: Prentice-Hall, ISBN 0-13-022335-2) L'implémentation est très
 * largement inspirée de la GSL.
 * 
 * @navassoc - - - UnivarFunction
 * 
 * @author redonnet
 */
public class BrentOptimizer extends AbstractOptimizer{
	/**
	 * A fixed size queue to hold memory of function evaluations.
	 * 
	 * This implementation is quite generic and may be moved to more general
	 * context.
	 * 
	 * @author redonnet
	 *
	 * @param <K>
	 */
	public class FixedSizeQueue<K> extends ArrayList<K> {
		private static final long serialVersionUID = 1L;
		private int maxSize;

		public FixedSizeQueue(int size) {
			this.maxSize = size;
		}

		public boolean push(K value) {
			boolean result = super.add(value);
			while (size() > maxSize) {
				remove(0);
			}
			return result;
		}

		public K getLast() {
			return get(size() - 1);
		}

		public K getFirst() {
			return get(0);
		}
	}

	/**
	 * An internal class to store the current state of resolution.
	 * 
	 * @author redonnet
	 *
	 */
	private class State {
		public double d, e, v, w;
		public double f_v, f_w;

		public State(double d, double e, double v, double w, double f_v, double f_w) {
			super();
			this.d = d;
			this.e = e;
			this.v = v;
			this.w = w;
			this.f_v = f_v;
			this.f_w = f_w;
		}
	}

	private final FixedSizeQueue<Double> memory;
	private final double golden = 0.3819660; // = (3 - sqrt(5))/2
	private double epsilon;
	private int maxIter;
	private UnivarFunction f;
	private double x_lower;
	private double x_upper;
	private double f_lower;
	private double f_upper;
	private State state;

	private double x_minimum;
	private double f_minimum;

	/**
	 * Crée un nouvel optimizer.
	 * 
	 * @param function
	 *            la fonction à optimiser
	 * @param lb
	 *            la borne inférieur
	 * @param ub
	 *            la borne supérieure
	 * @param precision
	 *            la précision souhaitée
	 * @param maxIter
	 *            le nombre maximum d'itérations allouées à cette résolution
	 */
	public BrentOptimizer(UnivarFunction function, double lb, double ub, double precision, int maxIter) {
		super();
		if (lb > ub)
			throw new IllConditionedProblemException("Lower bound should be inferior to upper bound");
		this.type = TYPE.BRENT;
		this.memory = new FixedSizeQueue<Double>(3);
		this.f = function;
		this.x_lower = lb;
		this.x_upper = ub;
		this.epsilon = precision;
		this.maxIter = maxIter;

		double v = x_lower + golden * (x_upper - x_lower);
		double w = v;

		this.f_lower = f.eval(x_lower);
		this.f_upper = f.eval(x_upper);
		this.x_minimum = (x_lower + x_upper) / 2.0;
		this.f_minimum = f.eval(x_minimum);
		memory.push(f_minimum);

		double d = 0.0;
		double e = 0.0;
		double f_v = f.eval(v);
		double f_w = f_v;

		this.state = new State(d, e, v, w, f_v, f_w);
	}

	/**
	 * Crée un nouvel optimizer avec 10 itérations maximum
	 * 
	 * @param function
	 *            la fonction à optimiser
	 * @param lb
	 *            la borne inférieur
	 * @param ub
	 *            la borne supérieure
	 * @param precision
	 *            la précision souhaitée
	 */
	public BrentOptimizer(UnivarFunction function, double lb, double ub, double precision) {
		this(function, lb, ub, precision, 10);
	}

	/**
	 * Crée un nouvel optimizer utilisant une précision de 1E(-3)
	 * 
	 * @param function
	 *            la fonction à optimiser
	 * @param lb
	 *            la borne inférieur
	 * @param ub
	 *            la borne supérieure
	 * @param maxIter
	 *            le nombre maximum d'itérations allouées à cette résolution
	 */
	public BrentOptimizer(UnivarFunction function, double lb, double ub, int maxIter) {
		this(function, lb, ub, Util.PREC3, maxIter);
	}

	/**
	 * Crée un nouvel optimizer utilisant une précision de 1E(-3) avec 10
	 * itérations maximum
	 * 
	 * @param function
	 *            la fonction à optimiser
	 * @param lb
	 *            la borne inférieur
	 * @param ub
	 *            la borne supérieure
	 */

	public BrentOptimizer(UnivarFunction function, double lb, double ub) {
		this(function, lb, ub, Util.PREC3, 10);
	}

	/**
	 * Lance la résolution.
	 * 
	 * @return le statut de la résolution (voir {@link Status})
	 */
	public Status run() {
		int iter = 0;
		Status status;

		do {
			iter++;
			status = iterate();
			if (status == Status.SUCCESS) // itération OK => on continue
				status = Status.CONTINUE;
			if (memory.size() > 2)
				status = test_stationary(Util.PREC12);
			if (status != Status.SUCCESS)
				status = test_interval(this.epsilon, 0.0);
		} while (status == Status.CONTINUE && iter < maxIter);
		if (iter >= maxIter)
			status = Status.MAXITER_REACHED;
		return status;
	}

	/**
	 * 
	 * @return la valeur optimale ou sa dernière estimation
	 */
	public double getVar() {
		return x_minimum;
	}

	private Status iterate() {
		double x_left = x_lower;
		double x_right = x_upper;
		double z = x_minimum;

		double d = state.e;
		double e = state.d;
		double u, f_u;
		double v = state.v;
		double w = state.w;
		double f_v = state.f_v;
		double f_w = state.f_w;
		double f_z = f_minimum;

		double w_lower = z - x_left;
		double w_upper = x_right - z;

		double tolerance = Util.PREC6 * abs(z);

		double p = 0, q = 0, r = 0;
		double midpoint = 0.5 * (x_left + x_right);

		if (abs(e) > tolerance) {
			/* fit parabola */

			r = (z - w) * (f_z - f_v);
			q = (z - v) * (f_z - f_w);
			p = (z - v) * q - (z - w) * r;
			q = 2 * (q - r);

			if (q > 0) {
				p = -p;
			} else {
				q = -q;
			}

			r = e;
			e = d;
		}

		if (abs(p) < abs(0.5 * q * r) && p < q * w_lower && p < q * w_upper) {
			double t2 = 2 * tolerance;

			d = p / q;
			u = z + d;

			if ((u - x_left) < t2 || (x_right - u) < t2) {
				d = (z < midpoint) ? tolerance : -tolerance;
			}
		} else {
			e = (z < midpoint) ? x_right - z : -(z - x_left);
			d = golden * e;
		}

		if (abs(d) >= tolerance) {
			u = z + d;
		} else {
			u = z + ((d > 0) ? tolerance : -tolerance);
		}

		state.e = e;
		state.d = d;

		f_u = f.eval(u);

		if (f_u <= f_z) {
			if (u < z) {
				x_upper = z;
				f_upper = f_z;
			} else {
				x_lower = z;
				f_lower = f_z;
			}

			state.v = w;
			state.f_v = f_w;
			state.w = z;
			state.f_w = f_z;
			x_minimum = u;
			f_minimum = f_u;
			memory.push(f_minimum);
			return Status.SUCCESS;
		} else {
			if (u < z) {
				x_lower = u;
				f_lower = f_u;
			} else {
				x_upper = u;
				f_upper = f_u;
			}

			if (f_u <= f_w || w == z) {
				state.v = w;
				state.f_v = f_w;
				state.w = u;
				state.f_w = f_u;
				return Status.SUCCESS;
			} else if (f_u <= f_v || v == z || v == w) {
				state.v = u;
				state.f_v = f_u;
				return Status.SUCCESS;
			}
		}

		return Status.SUCCESS;
	}

	private Status test_stationary(double epsabs) {
		double varsum = 0.0; // somme des variations entre les dernières
								// évaluations de la fonction;
		for (int i = 1; i < memory.size(); i++)
			varsum = varsum + pow((memory.get(i) - memory.get(i - 1)), 2);
		if (varsum < epsabs)
			return Status.SUCCESS;
		return Status.CONTINUE;
	}

	private Status test_interval(double epsabs, double epsrel) {
		double lower = x_lower;
		double upper = x_upper;

		double abs_lower = abs(lower);
		double abs_upper = abs(upper);

		double min_abs, tolerance;

		if (epsrel < 0.0)
			throw new IllegalArgumentException("Relative tolerance is negative");

		if (epsabs < 0.0)
			throw new IllegalArgumentException("Absolute tolerance is negative");

		if (lower > upper)
			throw new IllegalArgumentException("Lower bound larger than upper_bound");

		if ((lower > 0 && upper > 0) || (lower < 0 && upper < 0)) {
			min_abs = min(abs_lower, abs_upper);
		} else {
			min_abs = 0;
		}

		tolerance = epsabs + epsrel * min_abs;

		if (abs(upper - lower) < tolerance)
			return Status.SUCCESS;

		return Status.CONTINUE;
	}

	/**
	 * 
	 * @return un intervalle encadrant la solution.
	 */
	public Interval getXinterval() {
		return new Interval(x_lower, x_upper);
	}

	/**
	 * 
	 * @return un intervalle encadrant le minimum
	 */
	public Interval getFunctionInterval() {
		return new Interval(f_lower, f_upper);
	}
}
