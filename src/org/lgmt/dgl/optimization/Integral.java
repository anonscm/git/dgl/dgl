package org.lgmt.dgl.optimization;

import org.lgmt.dgl.functions.UnivarFunction;

/**
 * Integral of a single variable function
 * 
 * Calculation uses the trapezoidal rule
 * 
 */
public class Integral {
	private static int n = 10000;
	private UnivarFunction function;
	private double lower;
	private double upper;

	public Integral(UnivarFunction function, double lower, double upper) {
		super();
		this.function = function;
		this.lower = lower;
		this.upper = upper;
	}

	public static int getN() {
		return n;
	}

	public static void setN(int n) {
		Integral.n = n;
	}

	public double calc() {
		double h = (upper - lower) / (double) n;
		double sum = function.eval(lower);
		for (double u = lower + h; u < upper; u = u + h) {
			sum = sum + 2 * function.eval(u);
		}
		sum = sum + function.eval(upper);
		sum = sum * h / 2.0;

		return sum;
	}

}
