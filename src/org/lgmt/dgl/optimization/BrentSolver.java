/*
 ** Brent.java
 ** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
 **
 ** This file is part of the DGL software package (DGL stands for
 ** Differential Geometry Library). This software provides mathematic and numeric
 ** tools to work with various curves and surfaces (algebraic, Bézier,
 ** NURBS, ...) with an unified interface.
 **
 ** This software is governed by the CeCILL-C V2 license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 ** 
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-C license and that you accept its terms.
 */
package org.lgmt.dgl.optimization;

import static java.lang.Math.abs;
import static java.lang.Math.signum;

import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.UnivarFunction;

/**
 * Une classe pour résoudre les équations d'une seule variable bornée.
 * 
 * La méthode utilisée est la méthode de Brent. (cf Brent, R. P. (1973),
 * "Chapter 4", Algorithms for Minimization without Derivatives, Englewood
 * Cliffs, NJ: Prentice-Hall, ISBN 0-13-022335-2)
 * 
 * L'implémentation est très largement inspirée de la page Wikipedia sur cette
 * méthode (http://en.wikipedia.org/wiki/Brent%27s_method)
 * 
 * @navassoc - - - UnivarFunction
 * 
 * @author redonnet
 */
public class BrentSolver extends AbstractSolver{
	private UnivarFunction f;
	private double a; // contre-point (f(a)*f(b) < 0 => solution \in [a,b] )
	private double b; // estimation courante
	private double fa;
	private double fb;

	/**
	 * 
	 * @param function
	 *            la fonction dont on cherche la racine
	 * @param lb
	 *            la borne inférieure
	 * @param ub
	 *            la borne supérieure
	 * @param precision
	 *            la précision souhaitée
	 * @param maxIter
	 *            le nombre maximum d'itération que l'on souhaite allouer à
	 *            cette résolution
	 */
	public BrentSolver(UnivarFunction function, double lb, double ub, double precision, int maxIter) {
		super();
		this.type = TYPE.BRENT;
		this.size =1; 
		this.epsilon = precision;
		this.maxIter = maxIter;
		this.a = lb;
		this.b = ub;
		this.f = function;

		fa = f.eval(a);
		fb = f.eval(b);

		if (signum(fa) == signum(fb))
			throw new IllConditionedProblemException("Les deux extrémités de l'intervalle sont de même signe");
		if (abs(fa) < abs(fb)) {
			swapBounds();
		}
	}

	/**
	 * Crée un nouveau solveur.
	 * 
	 * @param lb
	 *            la borne inférieure
	 * @param ub
	 *            la borne supérieure
	 * @param function
	 *            la fonction dont on cherche la racine
	 */
	public BrentSolver(UnivarFunction function, double lb, double ub) {
		this(function, lb, ub, Util.PREC6, 10);
	}

	/**
	 * 
	 * @param lb
	 *            la borne inférieure
	 * @param ub
	 *            la borne supérieure
	 * @param function
	 *            la fonction dont on cherche la racine
	 * @param precision
	 *            la précision souhaitée
	 */
	public BrentSolver(UnivarFunction function, double lb, double ub, double precision) {
		this(function, lb, ub, precision, 10);
	}

	/**
	 * 
	 * @param function
	 *            la fonction dont on cherche la racine
	 * @param lb
	 *            la borne inférieure
	 * @param ub
	 *            la borne supérieure
	 * @param maxIter
	 *            le nombre maximum d'itération que l'on souhaite allouer à
	 *            cette résolution
	 */
	public BrentSolver(UnivarFunction function, double lb, double ub, int maxIter) {
		this(function, lb, ub, Util.PREC6, maxIter);
	}

	/**
	 * Lance la résolution.
	 * 
	 * @return the solver status at the end of the run
	 */
	public Status run() {
		int iter = 0;
		double c = 0, fc; // mémoire iter -1
		double d = Double.MAX_VALUE; // mémoire iter -2
		double s = 0, fs; // point candidat
		boolean mflag = true; // flag de bisection
		double limit;

		c = a;
		fc = fa;

		while ((abs(b - a) > epsilon) && (abs(fb) > Util.PREC12)) {
			if (iter > maxIter)
				return Status.MAXITER_REACHED;
			if ((fa != fc) && (fb != fc)) {
				// interpolation quadratique inverse
				s = ((a * fb * fc) / ((fa - fb) * (fa - fc))) + ((b * fa * fc) / ((fb - fa) * (fb - fc))) + ((c * fa * fb) / ((fc - fa) * (fc - fb)));
			} else {
				// sécante
				s = b - fb * (b - a) / (fb - fa);
			}

			limit = (3.0 * a + b) / 4.0;
			if (!((s > limit && s < b) || (s < limit && s > b)) || (mflag == true && abs(s - b) >= abs(b - c) / 2.0)
					|| (mflag == false && abs(s - b) >= abs(c - d) / 2.0)) {
				// bisection
				s = (a + b) / 2.0;
				mflag = true;
			} else {
				if ((mflag == true && (abs(b - c) < epsilon)) || (mflag == false && (abs(c - d) < epsilon))) {
					s = (a + b) / 2.0;
					mflag = true;
				} else
					mflag = false;
			}
			fs = f.eval(s);
			d = c;
			c = b;
			fc = fb;
			if (signum(fa) != signum(fs)) {
				b = s;
				fb = fs;
			} else {
				a = s;
				fa = fs;
			}
			if (abs(fa) < abs(fb)) {
				swapBounds();
			}
			iter++;
		}
		return Status.TOL_REACHED;
	}

	private void swapBounds() {
		double tmp;

		tmp = a;
		a = b;
		b = tmp;
		tmp = fa;
		fa = fb;
		fb = tmp;
	}

	/**
	 * Permet de récupérer la solution.
	 * 
	 * @return the last value calculated i.e. the solution if the solving
	 *         process terminates normally
	 */
	public double getVar() {
		return b;
	}

	/**
	 * Permet de récupérer la borne inférieure
	 * 
	 * Peut être utile si le solveur ne converge pas
	 * 
	 * @return the lower bound
	 */
	public double getLowerBound() {
		if (a < b)
			return a;
		else
			return b;
	}

	/**
	 * Permet de récupérer la borne supérieure
	 * 
	 * Peut être utile si le solveur ne converge pas
	 * 
	 * @return the upper bound
	 */
	public double getUpperBound() {
		/**
		 * Permet de récupérer la solution.
		 * 
		 * @return
		 */
		if (a > b)
			return a;
		else
			return b;
	}
}
