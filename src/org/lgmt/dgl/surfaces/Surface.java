/*
 ** Surface.java
 ** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
 **
 ** This file is part of the DGL software package (DGL stands for
 ** Differential Geometry Library). This software provides mathematic and numeric
 ** tools to work with various curves and surfaces (algebraic, Bézier,
 ** NURBS, ...) with an unified interface.
 **
 ** This software is governed by the CeCILL-C V2 license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 ** 
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-C license and that you accept its terms.
 */
package org.lgmt.dgl.surfaces;

import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static org.lgmt.dgl.vecmath.Vectors3.addV;
import static org.lgmt.dgl.vecmath.Vectors3.scaleV;

import java.util.ArrayList;
import java.util.List;

import org.lgmt.dgl.commons.Interval;
import org.lgmt.dgl.commons.MaxIterException;
import org.lgmt.dgl.commons.ParameterOutOfBoundsException;
import org.lgmt.dgl.commons.PointNotFoundException;
import org.lgmt.dgl.commons.Transformable;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.functions.BivarEvaluable;
import org.lgmt.dgl.functions.BivarFunction;
import org.lgmt.dgl.functions.BivarVec3Evaluable;
import org.lgmt.dgl.functions.BivarVec3Function;
import org.lgmt.dgl.functions.BivarVec3FunctionFactory;
import org.lgmt.dgl.functions.MultivarEvaluable;
import org.lgmt.dgl.functions.MultivarFunction;
import org.lgmt.dgl.functions.UnivarBoundedFunction;
import org.lgmt.dgl.functions.UnivarEvaluable;
import org.lgmt.dgl.optimization.BrentOptimizer;
import org.lgmt.dgl.optimization.NewtonSolver;
import org.lgmt.dgl.optimization.Status;
import org.lgmt.dgl.vecmath.AxisAngle4d;
import org.lgmt.dgl.vecmath.GVector;
import org.lgmt.dgl.vecmath.Matrix2d;
import org.lgmt.dgl.vecmath.Matrix3d;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Points;
import org.lgmt.dgl.vecmath.Vector2d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.dgl.vecmath.Vectors3;

/**
 * Une surface.
 * 
 * <p>
 * Une surface est définie par une fonction vectorielle de deux variables et les
 * bornes de ces variables.
 * </p>
 * <p>
 * Cette classe devrait être abstraite, mais elle est conservée instanciable
 * pour permettre la duplication de n'importe quel type de surface. (voir
 * {@link #Surface(Surface s) Surface(Surface s)}. Tous les autres constructeurs
 * sont <b>protected</b>.
 * </p>
 * 
 * @navassoc - - - S_TYPE
 * @navassoc - - - BivarVec3Function
 * @depend - - - SurfaceParam
 * @depend - - - Matrix2d
 * 
 * @author redonnet
 * 
 */
public class Surface implements BivarVec3Evaluable, Transformable<Surface> {
	public enum Param {
		U, V
	};

	public enum Limit {
		UMIN, UMAX, VMIN, VMAX
	};

	protected static BivarVec3FunctionFactory functionFactory = new BivarVec3FunctionFactory();
	protected S_TYPE type;
	protected double umin;
	protected double umax;
	protected double vmin;
	protected double vmax;
	protected BivarVec3Function v3f;
	protected BivarVec3Function normalV3f = null;
	protected BivarVec3Function dSu = null;
	protected BivarVec3Function dSv = null;
	protected BivarVec3Function dSuu = null;
	protected BivarVec3Function dSuv = null;
	protected BivarVec3Function dSvv = null;

	/**
	 * Le constructeur par défaut.
	 * 
	 */
	protected Surface() {
		super();
		this.setdSu();
		this.setdSv();
		this.setNV3f();
		this.setdSuu();
		this.setdSuv();
		this.setdSvv();
	}

	/**
	 * Crée une nouvelle surface comme un clone en profondeur d'une surface
	 * existante
	 * 
	 * @param s
	 */
	public Surface(Surface s) {
		this.type = s.type;
		this.umin = s.umin;
		this.umax = s.umax;
		this.vmin = s.vmin;
		this.vmax = s.vmax;
		this.v3f = s.v3f;
	}

	/**
	 * Construit une nouvelle surface comme la transformée d'une autre surface.
	 * 
	 * <p>
	 * Prévu pour être utilisé via
	 * {@link org.lgmt.dgl.surfaces.SurfaceFactory#newSurfaceAsTransformed(Surface surface, Matrix4d transform)}.
	 * 
	 * @param surface   la surface initiale
	 * @param transform la matrice de transformation
	 */
	protected Surface(Surface surface, Matrix4d transform) {
		this.type = surface.type;
		this.umin = surface.umin;
		this.umax = surface.umax;
		this.vmin = surface.vmin;
		this.vmax = surface.vmax;
		this.v3f = new BivarVec3Function(surface.v3f, transform);
	}

	/**
	 * Calcule un point de la surface sans tenir compte des bornes.
	 * 
	 * @param point un point pour stocker le résultat
	 * @param u
	 * @param v
	 */
	public void unboundedEval(Point3d point, double u, double v) {
		v3f.eval(point, u, v);
	}

	/**
	 * Calcule un point de la surface sans tenir compte des bornes.
	 * 
	 * @param u
	 * @param v
	 * @return un nouveau point contenant le résultat
	 */
	public Point3d unboundedEval(double u, double v) {
		Point3d p = new Point3d();
		this.unboundedEval(p, u, v);
		return p;
	}

	/**
	 * Tests si la surface est définie pour les paramètres donnés
	 * 
	 * @param u
	 * @param v
	 * @return true si la surface est définie pour les paramètres u et v
	 */
	public boolean isDefinedFor(double u, double v) {
		if (Math.abs(u - umin) < Util.PREC12)
			u = umin;
		if (Math.abs(u - umax) < Util.PREC12)
			u = umax;
		if (Math.abs(v - vmin) < Util.PREC12)
			v = vmin;
		if (Math.abs(v - vmax) < Util.PREC12)
			v = vmax;
		if (u >= umin && u <= umax && v >= vmin && v <= vmax)
			return true;
		else
			return false;

	}

	/**
	 * Teste si le couple de paramètre (u,v) est sur les limites de la surface.
	 * 
	 * @param u
	 * @param v
	 * @return true si u et/ou v sont sur les limites paramétriques de la surface (à
	 *         10E-12 près)
	 */
	public boolean isOnBounds(double u, double v) {
		if (Math.abs(u - umin) < Util.PREC12)
			return true;
		if (Math.abs(u - umax) < Util.PREC12)
			return true;
		if (Math.abs(v - vmin) < Util.PREC12)
			return true;
		if (Math.abs(v - vmax) < Util.PREC12)
			return true;

		return false;
	}

	/**
	 * Ramène un point sur les limites de la surface s'il est proche des bords.
	 * 
	 * @param p         le point à ramener dans les limites de la surface
	 * 
	 * @param precision la précision à l'intérieur de laquelle on considère que le
	 *                  point est près de la limite. Si la valeur est null ou
	 *                  négative, une précision de 10E-12 est utilisée
	 * 
	 * @return true si le point p est sur les limites paramétriques de la surface à
	 *         la précision près
	 */
	public boolean clampOnBounds(SurfacePoint p, Double precision) {
		boolean flag = false;

		if (precision == null || precision < 0.0)
			precision = Util.PREC12;

		if (Math.abs(p.u - umin) < precision) {
			p.u = umin;
			flag = true;
		}
		if (Math.abs(p.u - umax) < precision) {
			p.u = umax;
			flag = true;
		}
		if (Math.abs(p.v - vmin) < precision) {
			p.v = vmin;
			flag = true;
		}
		if (Math.abs(p.v - vmax) < precision) {
			p.v = vmax;
			flag = true;
		}
		if (flag)
			p.set(this.eval(p.u, p.v));

		return flag;
	}

	/**
	 * Ramène le point p sur les limites de la surface inconditionnellement.
	 * 
	 * @param p le point à ramener dans les limites de la surface
	 * 
	 * @return true si le point p a été ramené sur la limite, false dans le cas
	 *         contraire
	 */
	public boolean clampOnBounds(SurfacePoint p) {
		boolean flag = false;

		if (p.u < umin) {
			p.u = umin;
			flag = true;
		}
		if (p.u > umax) {
			p.u = umax;
			flag = true;
		}
		if (p.v < vmin) {
			p.v = vmin;
			flag = true;
		}
		if (p.v > vmax) {
			p.v = vmax;
			flag = true;
		}
		if (flag)
			p.set(this.eval(p.u, p.v));

		return flag;
	}

	/**
	 * Calcule un point de la surface
	 * 
	 * 
	 * @param point un point pour stocker le résultat
	 * @param u
	 * @param v
	 * @throws ParameterOutOfBoundsException si un paramètre est hors des bornes
	 */
	@Override
	public void eval(Point3d point, double u, double v) {
		if (Math.abs(u - umin) < Util.PREC12)
			u = umin;
		if (Math.abs(u - umax) < Util.PREC12)
			u = umax;
		if (Math.abs(v - vmin) < Util.PREC12)
			v = vmin;
		if (Math.abs(v - vmax) < Util.PREC12)
			v = vmax;
		if (u >= umin && u <= umax && v >= vmin && v <= vmax)
			v3f.eval(point, u, v);
		else {
			if (!(u >= umin && u <= umax))
				throw new ParameterOutOfSurfaceBoundsException(this, u, v);
			if (!(v >= vmin && v <= vmax))
				throw new ParameterOutOfSurfaceBoundsException(this, u, v);
		}
	}

	/**
	 * Calcule un point de la surface
	 * 
	 * 
	 * @param point un point pour stocker le résultat
	 * @param u
	 * @param v
	 * @throws ParameterOutOfBoundsException si un paramètre est hors des bornes
	 */
	public void eval(SurfacePoint point, double u, double v) {
		if (Math.abs(u - umin) < Util.PREC12)
			u = umin;
		if (Math.abs(u - umax) < Util.PREC12)
			u = umax;
		if (Math.abs(v - vmin) < Util.PREC12)
			v = vmin;
		if (Math.abs(v - vmax) < Util.PREC12)
			v = vmax;
		if (u >= umin && u <= umax && v >= vmin && v <= vmax)
			point = new SurfacePoint(this, u, v);
		else {
			if (!(u >= umin && u <= umax))
				throw new ParameterOutOfSurfaceBoundsException(this, u, v);
			if (!(v >= vmin && v <= vmax))
				throw new ParameterOutOfSurfaceBoundsException(this, u, v);
		}
	}

	/**
	 * Calcule un point de la surface
	 * 
	 * 
	 * @param u
	 * @param v
	 * @return un nouveau point contenant le résultat
	 * @throws ParameterOutOfBoundsException si un paramètre est hors des bornes
	 */
	@Override
	public Point3d eval(double u, double v) {
		Point3d p = new Point3d();
		this.eval(p, u, v);
		return p;
	}

	/**
	 * Calcule la coordonnée en x du point de la surface défini par les paramètres u
	 * et v
	 * 
	 * @param u
	 * @param v
	 * @return la valeur calculée
	 */
	public double evalX(double u, double v) {
		return v3f.getFx().eval(u, v);
	}

	/**
	 * Calcule la coordonnée en y du point de la surface défini par les paramètres u
	 * et v
	 * 
	 * @param u
	 * @param v
	 * @return la valeur calculée
	 */
	public double evalY(double u, double v) {
		return v3f.getFy().eval(u, v);
	}

	/**
	 * Calcule la coordonnée en z du point de la surface défini par les paramètres u
	 * et v
	 * 
	 * @param u
	 * @param v
	 * @return la valeur calculée
	 */
	public double evalZ(double u, double v) {
		return v3f.getFz().eval(u, v);
	}

	/**
	 * Définit la fonction dérivée en u.
	 * 
	 */
	private void setdSu() {
		dSu = new BivarVec3Function(new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFx().diffu(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFy().diffu(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFz().diffu(u, v);
			}
		}));
	}

	/**
	 * Définit la fonction dérivée en v.
	 * 
	 */
	private void setdSv() {
		dSv = new BivarVec3Function(new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFx().diffv(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFy().diffv(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFz().diffv(u, v);
			}
		}));
	}

	/**
	 * Définit la fonction normale en (u,v).
	 * 
	 * Calcule dSU x dSv.
	 * 
	 * ATTENTION : les bornes de la surface ne sont pas prises en compte et le
	 * résultat n'est pas normé.
	 * 
	 */
	private void setNV3f() {
		normalV3f = new BivarVec3Function(new BivarFunction(new BivarEvaluable() {
			public double eval(double u, double v) {
				return dSu.getFy().eval(u, v) * dSv.getFz().eval(u, v)
						- dSu.getFz().eval(u, v) * dSv.getFy().eval(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			public double eval(double u, double v) {
				return dSu.getFz().eval(u, v) * dSv.getFx().eval(u, v)
						- dSu.getFx().eval(u, v) * dSv.getFz().eval(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			public double eval(double u, double v) {
				return dSu.getFx().eval(u, v) * dSv.getFy().eval(u, v)
						- dSu.getFy().eval(u, v) * dSv.getFx().eval(u, v);
			}
		}));
	}

	/**
	 * Définit la fonction dérivée seconde en uu.
	 * 
	 */
	private void setdSuu() {
		dSuu = new BivarVec3Function(new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFx().diffuu(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFy().diffuu(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFz().diffuu(u, v);
			}
		}));
	}

	/**
	 * Définit la fonction dérivée seconde en uv.
	 * 
	 */
	private void setdSuv() {
		dSuv = new BivarVec3Function(new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFx().diffuv(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFy().diffuv(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFz().diffuv(u, v);
			}
		}));
	}

	/**
	 * Définit la fonction dérivée seconde en vv.
	 * 
	 */
	private void setdSvv() {
		dSvv = new BivarVec3Function(new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFx().diffvv(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFy().diffvv(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFz().diffvv(u, v);
			}
		}));
	}

	/**
	 * Calcule la tangente en u à la surface en un point.
	 * 
	 * @param result
	 * @param u
	 * @param v
	 * @throws ParameterOutOfBoundsException si un paramètre est hors des bornes
	 */
	public void tanU(Vector3d result, double u, double v) {
		if (u < umin || u > umax)
			throw new ParameterOutOfBoundsException(u);
		if (v < vmin || v > vmax)
			throw new ParameterOutOfBoundsException(v);

		if (u < umin + Util.PREC6)
			v3f.diffuFw(result, u, v);
		else if (u > umax - Util.PREC6)
			v3f.diffuBw(result, u, v);
		else
			v3f.diffu(result, u, v);
	}

	/**
	 * Calcule la tangente en u à la surface en un point.
	 * 
	 * @param u
	 * @param v
	 * @return le vecteur tangent nouvellement calculé
	 * @throws ParameterOutOfBoundsException si un paramètre est hors des bornes
	 */
	public Vector3d tanU(double u, double v) {
		Vector3d tgu = new Vector3d();
		this.tanU(tgu, u, v);
		return tgu;
	}

	/**
	 * Calcule la tangente en v à la surface en un point.
	 * 
	 * @param result
	 * @param u
	 * @param v
	 */
	public void tanV(Vector3d result, double u, double v) {
		if (u < umin || u > umax)
			throw new ParameterOutOfBoundsException(u);
		if (v < vmin || v > vmax)
			throw new ParameterOutOfBoundsException(v);

		if (v < vmin + Util.PREC6)
			v3f.diffvFw(result, u, v);
		else if (v > vmax - Util.PREC6)
			v3f.diffvBw(result, u, v);
		else
			v3f.diffv(result, u, v);
	}

	/**
	 * Calcule la tangente en v à la surface en un point.
	 * 
	 * @param u
	 * @param v
	 * @return le vecteur tangent nouvellement calculé
	 */
	public Vector3d tanV(double u, double v) {
		Vector3d tgv = new Vector3d();
		this.tanV(tgv, u, v);
		return tgv;
	}

	/**
	 * Calcule la normale à la surface en un point.
	 * 
	 * Le résultat est normé.
	 * 
	 * @param result un vecteur pour stocker le résultat
	 * @param u
	 * @param v
	 */
	public void normal(Vector3d result, double u, double v) {
		if (abs(u - umax) < Util.PREC12)
			u = umax;
		if (abs(u - umin) < Util.PREC12)
			u = umin;
		if (abs(v - vmax) < Util.PREC12)
			v = vmax;
		if (abs(v - vmin) < Util.PREC12)
			v = vmin;
		if (u < umin || u > umax)
			throw new ParameterOutOfSurfaceBoundsException(this, u, v);
		if (v < vmin || v > vmax)
			throw new ParameterOutOfSurfaceBoundsException(this, u, v);

		Vector3d tgu = new Vector3d();
		Vector3d tgv = new Vector3d();

		if (u < umin + Util.PREC6)
			v3f.diffuFw(tgu, u, v);
		else if (u > umax - Util.PREC6)
			v3f.diffuBw(tgu, u, v);
		else
			v3f.diffu(tgu, u, v);

		if (v < vmin + Util.PREC6)
			v3f.diffvFw(tgv, u, v);
		else if (v > vmax - Util.PREC6)
			v3f.diffvBw(tgv, u, v);
		else
			v3f.diffv(tgv, u, v);

		/* ************************** */
		/* Cas des carreaux dégénérés */
		/* ************************** */

		// Vecteur tangent nul
		if (tgu.length() < Util.PREC6) {
			result.set(this.normal_tgu0(u, v, tgv));
			result.normalize();
			return;
		}
		if (tgv.length() < Util.PREC6) {
			result.set(this.normal_tgv0(u, v, tgu));
			result.normalize();
			return;
		}

		// Vecteurs tangents alignés
		if (Vectors3.epsilonAligned(tgu, tgv, Util.PREC9)) {
			result.set(this.normal_align(u, v, tgu, tgv));
			result.normalize();
			return;
		}

		result.cross(new Vector3d(tgu), new Vector3d(tgv));

		if (result.length() < Util.PREC6) {
			double h = 1E-6;
			if (abs(tgu.length()) < Util.PREC9) {
				if (v < (umax - umin) / 2.0)
					this.tanU(tgu, u, v + h);
				else
					this.tanU(tgu, u, v - h);
			}
			if (abs(tgv.length()) < Util.PREC9) {
				if (u < (umax - umin) / 2.0)
					this.tanV(tgv, u + h, v);
				else
					this.tanV(tgv, u - h, v);
			}
			// TODO : Gérer le cas où tgu et tgv sont alignés
		}

		result.normalize();
	}

	private Vector3d normal_tgu0(double u, double v, Vector3d tgv) {
		Vector3d n = new Vector3d();

		return n;
	}

	private Vector3d normal_tgv0(double u, double v, Vector3d tgu) {
		Vector3d n = new Vector3d();

		return n;
	}

	/*
	 * Calcule la normale à la surface quand tgu et tgv sont alignés
	 * 
	 * Principe cherche le point O, centre du cercle passant par trois points dans
	 * le plan contenant tgu et tgv Soit B = S(u,v), A = S(u+du,v) et C = S(u,v+dv)
	 * On a, par définition : OA = OB = OC On pose arbitrairement : OB = alpha AB +
	 * beta BC (0)
	 *
	 * OB.OB = OC.OC = (OB + BC).(OB + BC) = OB.OB + BC.BC + 2 OB.BC
	 *
	 * ==> BC.BC + 2 OB.BC = 0 (1)
	 *
	 * OB.OB = OA.OA = (OB + BA).(OB + BA) = OB.OB + BA.BA + 2 OB.BA
	 *
	 * ==> BA.BA + 2 OB.BA = 0 <=> AB.AB - 2 OB.AB = 0 (2)
	 *
	 * On injecte (0) dans (1) et (2)
	 * 
	 * -BC.BC / 2 = alpha AB.BC + beta BC.BC (1') AB.AB / 2 = alpha AB.AB + beta
	 * BC.AB (2')
	 * 
	 * La résolution analytique du système linéaire en (alpha,beta) donne :
	 * 
	 * (BC.AB + AB.AB) * BC.BC alpha = ------------------------------------- 2 *
	 * AB.AB * BC.BC - 2 * AB.BC * BC.AB
	 * 
	 * et
	 * 
	 * - (BC.BC + AB.BC) * AB.AB beta = ------------------------------------- 2 *
	 * AB.AB * BC.BC - 2 * AB.BC * BC.AB
	 * 
	 * On calcule ensuite OB = alpha AB + beta BC qui est normal à la surface en B
	 */
	private Vector3d normal_align(double u, double v, Vector3d tgu, Vector3d tgv) {
		Vector3d n = new Vector3d();
		double du = Util.PREC6;
		double dv = Util.PREC6;
		Point3d A, B, C;
		B = this.eval(u, v);
		if (u > umax - du)
			A = this.eval(u - du, v);
		else
			A = this.eval(u + du, v);
		if (v > vmax - dv)
			C = this.eval(u, v - dv);
		else
			C = this.eval(u, v + dv);
		Vector3d AB = new Vector3d(A, B);
		Vector3d BC = new Vector3d(B, C);

		double alpha = (BC.dot(AB) + AB.dot(AB)) * BC.dot(BC)
				/ (2 * AB.dot(AB) * BC.dot(BC) - 2 * AB.dot(BC) * BC.dot(AB));
		double beta = -(BC.dot(BC) + AB.dot(BC)) * AB.dot(AB)
				/ (2 * AB.dot(AB) * BC.dot(BC) - 2 * AB.dot(BC) * BC.dot(AB));

		n.set(addV(scaleV(alpha, AB), scaleV(beta, BC)));

		return n;
	}

	/**
	 * Calcule la normale à la surface en un point.
	 * 
	 * Le résultat est normé.
	 * 
	 * @param u
	 * @param v
	 * @return un nouveau vecteur contenant le résultat
	 */
	public Vector3d normal(double u, double v) {
		Vector3d n = new Vector3d();
		this.normal(n, u, v);
		return n;
	}

	/**
	 * Calcule la première matrice fondamentale de la surface en un point.
	 * 
	 * @param u
	 * @param v
	 * 
	 * @return la première matrice fondamentale de la surface au point (u,v)
	 */
	public Matrix2d evalGmatrix(double u, double v) {
		double m00, m01, m10, m11;
		Vector3d dSu = new Vector3d();
		Vector3d dSv = new Vector3d();
		this.v3f.diffu(dSu, u, v);
		this.v3f.diffv(dSv, u, v);

		m00 = dSu.dot(dSu);
		m01 = dSu.dot(dSv);
		m10 = dSv.dot(dSu);
		m11 = dSv.dot(dSv);

		return new Matrix2d(m00, m01, m10, m11);
	}

	/**
	 * Calcule la deuxième matrice fondamentale de la surface en un point.
	 *
	 * @param u
	 * @param v
	 * 
	 * @return la deuxième matrice fondamentale de la surface au point (u,v)
	 */
	public Matrix2d evalDmatrix(double u, double v) {
		double m00, m01, m10, m11;
		BivarVec3Function dSuu = new BivarVec3Function(new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFx().diffuu(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFy().diffuu(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFz().diffuu(u, v);
			}
		}));
		BivarVec3Function dSuv = new BivarVec3Function(new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFx().diffuv(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFy().diffuv(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFz().diffuv(u, v);
			}
		}));
		BivarVec3Function dSvu = new BivarVec3Function(new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFx().diffvu(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFy().diffvu(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFz().diffvu(u, v);
			}
		}));

		BivarVec3Function dSvv = new BivarVec3Function(new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFx().diffvv(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFy().diffvv(u, v);
			}
		}), new BivarFunction(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				return v3f.getFz().diffvv(u, v);
			}
		}));
		Vector3d n = this.normal(u, v);

		Vector3d v00 = new Vector3d(dSuu.eval(u, v));
		Vector3d v01 = new Vector3d(dSuv.eval(u, v));
		Vector3d v10 = new Vector3d(dSvu.eval(u, v));
		Vector3d v11 = new Vector3d(dSvv.eval(u, v));

		m00 = n.dot(v00);
		m01 = n.dot(v01);
		m10 = n.dot(v10);
		m11 = n.dot(v11);

		return new Matrix2d(m00, m01, m10, m11);
	}

	/**
	 * Calcule la courbure gaussienne de la surface en un point.
	 *
	 * @param u
	 * @param v
	 * 
	 * @return la courbure gaussienne de la surface au point (u,v)
	 */
	public double evalGaussianCurvature(double u, double v) {
		double d = evalDmatrix(u, v).determinant();
		double g = evalGmatrix(u, v).determinant();

		return d / g;
	}

	/**
	 * Calcule la courbure moyenne de la surface en un point.
	 *
	 * @param u
	 * @param v
	 * 
	 * @return la courbure gaussienne de la surface au point (u,v)
	 */
	public double evalMedianCurvature(double u, double v) {
		Matrix2d d = evalDmatrix(u, v);
		Matrix2d g = evalGmatrix(u, v);

		return (g.m00 * d.m11 + d.m00 * g.m11 - 2 * g.m01 * d.m01) / (2 * g.determinant());
	}

	/**
	 * Calcule une des courbures principales de la surface en un point.
	 * 
	 * H+sqrt(H^2-K)
	 *
	 * @param u
	 * @param v
	 * 
	 * @return Calcule une des courbures principales de la surface au point (u,v)
	 */
	public double evalMainCurvature1(double u, double v) {
		double k = evalGaussianCurvature(u, v);
		double h = evalMedianCurvature(u, v);

		return h + Math.sqrt(h * h - k);
	}

	/**
	 * Calcule une des courbures principales de la surface en un point.
	 * 
	 * H-sqrt(H^2-K)
	 * 
	 * @param u
	 * @param v
	 * 
	 * @return Calcule une des courbures principales de la surface au point (u,v)
	 */
	public double evalMainCurvature2(double u, double v) {
		double k = evalGaussianCurvature(u, v);
		double h = evalMedianCurvature(u, v);

		return h - Math.sqrt(h * h - k);
	}

	/**
	 * Calcule la courbure normale en un point de la surface.
	 * 
	 * La direction dans laquelle est calculée la courbure est définie par l'angle
	 * que forme cette direction avec la tangente en u.
	 * 
	 * L'abscisse curviligne dans cette direction est exprimée sous la la forme q =
	 * (du dv). On a alors
	 * 
	 * Kn = (q*D*transpose(q))/(q*G*transpose(q))
	 * 
	 * @param u
	 * @param v
	 * @param angle (en radians)
	 * 
	 * @return the normal curvature
	 */
	public double evalNormalCurvature(double u, double v, double angle) {
		double kn = 0;

		Vector3d tgu = tanU(u, v);
		tgu.normalize();
		Vector3d tgv = tanV(u, v);
		tgv.normalize();
		Vector3d n = normal(u, v);

		Matrix3d m = new Matrix3d(tgu.x, tgu.y, tgu.z, tgv.x, tgv.y, tgv.z, n.x, n.y, n.z);

		Vector3d vec = new Vector3d();
		AxisAngle4d aa = new AxisAngle4d(n, angle);
		Matrix3d r = new Matrix3d();
		r.set(aa);
		r.transform(tgu, vec);

		m.transform(vec);

		double du = vec.x * Util.PREC9;
		double dv = vec.y * Util.PREC9;

		Matrix2d d = evalDmatrix(u, v);
		Matrix2d g = evalGmatrix(u, v);

		kn = (d.m00 * du * du + 2 * d.m01 * du * dv + d.m11 * dv * dv)
				/ (g.m00 * du * du + 2 * g.m01 * du * dv + g.m11 * dv * dv);

		return kn;
	}

	/**
	 * Calcule la distance entre la surface et une autre surface en un point.
	 * 
	 * <p>
	 * La distance est calculée selon la normale à <b>this</b> en <b>(u,v)</b>
	 * 
	 * @param surface la surface distante
	 * @param u
	 * @param v
	 * 
	 * @return la distance calculée
	 * @throws MaxIterException si une solution n'a pas été trouvée en 10 itérations
	 */
	public double distanceTo(final Surface surface, double u, double v) {
		final Vector3d n = new Vector3d();
		normal(n, u, v);
		final Point3d p = new Point3d();
		eval(p, u, v);
		MultivarFunction f1 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector var) {
				return (p.x + var.getElement(0) * n.x) - surface.v3f.getFx().eval(var.getElement(1), var.getElement(2));
			}
		});
		MultivarFunction f2 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector var) {
				return (p.y + var.getElement(0) * n.y) - surface.v3f.getFy().eval(var.getElement(1), var.getElement(2));
			}
		});
		MultivarFunction f3 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector var) {
				return (p.z + var.getElement(0) * n.z) - surface.v3f.getFz().eval(var.getElement(1), var.getElement(2));
			}
		});
		List<MultivarFunction> functions = new ArrayList<MultivarFunction>();
		functions.add(f1);
		functions.add(f2);
		functions.add(f3);
		NewtonSolver solver = new NewtonSolver(functions, 10);
		solver.init(new GVector(new double[] { 0.0D, u, v }));
		try {
			solver.run();
			return solver.getVars().getElement(0);
		} catch (MaxIterException e) {
			throw e;
		}
	}

	/**
	 * Calcule la distance entre un point de la surface et une autre surface selon
	 * une direction donnée.
	 * 
	 * @param surface   la surface distante
	 * @param u
	 * @param v
	 * @param direction la direction de recherche
	 * 
	 * @return la distance calculée ou -1 en cas d'échec
	 */
	public double distanceToAlong(final Surface surface, double u, double v, final Vector3d direction) {
		final Point3d p = new Point3d();
		eval(p, u, v);
		MultivarFunction f1 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector var) {
				return (p.x + var.getElement(0) * direction.x)
						- surface.v3f.getFx().eval(var.getElement(1), var.getElement(2));
			}
		});
		MultivarFunction f2 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector var) {
				return (p.y + var.getElement(0) * direction.y)
						- surface.v3f.getFy().eval(var.getElement(1), var.getElement(2));
			}
		});
		MultivarFunction f3 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector var) {
				return (p.z + var.getElement(0) * direction.z)
						- surface.v3f.getFz().eval(var.getElement(1), var.getElement(2));
			}
		});
		List<MultivarFunction> functions = new ArrayList<MultivarFunction>();
		functions.add(f1);
		functions.add(f2);
		functions.add(f3);
		NewtonSolver solver = new NewtonSolver(functions, 10);
		solver.init(new GVector(new double[] { 0.0D, 0.5D, 0.5D }));

		if (solver.run() == Status.TOL_REACHED)
			return solver.getVars().getElement(0);
		else
			return -1.0;
	}

	/**
	 * Calcule la projection d'un point sur la surface.
	 *
	 * Résout S(u,v) + k*direction = P
	 * 
	 * variables var = [k, u, v]
	 * 
	 * @param p         point à projeter
	 * @param u         point de départ de la résolution
	 * @param v         point de départ de la résolution
	 * @param direction direction de la projection
	 *
	 * @return la projection de p sur la surface selon la direction donnée. null si
	 *         aucune solution n'a été trouvée
	 */
	public SurfacePoint getPointProjection(final Point3d p, double u, double v, final Vector3d direction) {
		MultivarFunction f1 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector var) {
				return evalX(var.getElement(1), var.getElement(2)) + var.getElement(0) * direction.x - p.x;
			}
		});
		MultivarFunction f2 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector var) {
				return evalY(var.getElement(1), var.getElement(2)) + var.getElement(0) * direction.y - p.y;
			}
		});
		MultivarFunction f3 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector var) {
				return evalZ(var.getElement(1), var.getElement(2)) + var.getElement(0) * direction.z - p.z;
			}
		});
		List<MultivarFunction> functions = new ArrayList<MultivarFunction>();
		functions.add(f1);
		functions.add(f2);
		functions.add(f3);
		NewtonSolver solver = new NewtonSolver(functions, 10);
		solver.init(new GVector(new double[] { 1.0, u, v }));

		SurfacePoint result = null;
		if (solver.run() == Status.TOL_REACHED) {
			double utmp = solver.getVars().getElement(1);
			double vtmp = solver.getVars().getElement(2);
			if (isDefinedFor(utmp, vtmp))
				result = new SurfacePoint(this, utmp, vtmp);
			else
				result = null;
		}

		return result;
	}

	/**
	 * Calcule la projection normale d'un point sur la surface.
	 *
	 * Méthode principale : On résout S(u,v) + k n(u,v) = P. Système 3x3 en (u,v,k)
	 * résolu avec Newton-Raphson.
	 * 
	 * Méthode de secours : Au point (u,v) solution, on doit avoir (S(u,v)-p) normal
	 * à la surface, donc (S(u,v)-p).tgU = 0 et (S(u,v)-p).tgV = 0 où tgU et tgV
	 * sont les tangentes à la surface en u et v respectivement
	 * 
	 * @param p  point à projeter
	 * @param u0 point de départ de la résolution
	 * @param v0 point de départ de la résolution
	 * @return la projection normale de p sur la surface. null si aucune solution
	 *         n'a été trouvée
	 */
	public SurfacePoint getNormalPointProjection(final Point3d p, double u0, double v0) {
		SurfacePoint result = null;

		MultivarFunction f1 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector var) {
				return (v3f.getFx().eval(var.getElement(0), var.getElement(1))
						+ var.getElement(2) * normalV3f.getFx().eval(var.getElement(0), var.getElement(1)) - p.x);
			}
		});
		MultivarFunction f2 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector var) {
				return (v3f.getFy().eval(var.getElement(0), var.getElement(1))
						+ var.getElement(2) * normalV3f.getFy().eval(var.getElement(0), var.getElement(1)) - p.y);
			}
		});
		MultivarFunction f3 = new MultivarFunction(3, new MultivarEvaluable() {
			@Override
			public double eval(GVector var) {
				return (v3f.getFz().eval(var.getElement(0), var.getElement(1))
						+ var.getElement(2) * normalV3f.getFz().eval(var.getElement(0), var.getElement(1)) - p.z);
			}
		});
		List<MultivarFunction> functions = new ArrayList<MultivarFunction>();
		functions.add(f1);
		functions.add(f2);
		functions.add(f3);
		NewtonSolver solver = new NewtonSolver(functions, 10);
		solver.init(new GVector(new double[] { u0, v0, 0.0 }));

		if (solver.run() == Status.TOL_REACHED
				&& isDefinedFor(solver.getVars().getElement(0), solver.getVars().getElement(1))) {
			result = new SurfacePoint(this, solver.getVars().getElement(0), solver.getVars().getElement(1));

			return result;
		}

		// Fallback
		// (S(u,v)-p).tgU = 0 et (S(u,v)-p).tgV = 0
		// En décomposant S(u,v) en S(u0,v0) + du*tgU + dv*tgV
		// on peut calculer les valeurs de du et dv à chaque itération

		double u = u0;
		double v = v0;

		Vector3d tgU, tgV, vs;
		Vector3d vp = new Vector3d(p);

		double du = Double.MAX_VALUE;
		double dv = Double.MAX_VALUE;
		int maxIter = 20;
		int iter = 0;
		while (du * du + dv * dv > Util.PREC12 && iter < maxIter) {
			iter++;
			tgU = this.tanU(u, v);
			tgV = this.tanV(u, v);
			vs = new Vector3d(this.eval(u, v));

			du = ((vp.dot(tgV) - vs.dot(tgV)) * tgV.dot(tgU) + (vs.dot(tgU) - vp.dot(tgU)) * tgV.dot(tgV))
					/ (pow(tgU.dot(tgV), 2) - tgU.dot(tgU) * tgV.dot(tgV));
			dv = ((vp.dot(tgU) - vs.dot(tgU)) * tgU.dot(tgV) + (vs.dot(tgV) - vp.dot(tgV)) * tgU.dot(tgU))
					/ (pow(tgU.dot(tgV), 2) - tgU.dot(tgU) * tgV.dot(tgV));

			u = u + du;
			u = u < umin ? umin : u;
			u = u > umax ? umax : u;
			v = v + dv;
			v = v < vmin ? vmin : v;
			v = v > vmax ? vmax : v;
		}

		// On vérifie que le point trouvé correspond bien à la normale
		Vector3d n = new Vector3d(this.eval(u, v), p);
		double err = pow(abs(n.dot(this.tanU(u, v))), 2) + pow(abs(n.dot(this.tanV(u, v))), 2);
		if (err < Util.PREC9)
			result = new SurfacePoint(this, u, v);

		if (result == null) {
			// TODO: log Warning
			// System.out.println("result null");
		}

		return result;
	}

	/**
	 * Calcule la distance d'un point à la surface
	 * 
	 * @param p  point à projeter
	 * @param u0 point de départ de la résolution
	 * @param v0 point de départ de la résolution
	 * @return la distance de p à sa projection normale sur <b>this</b> ou -1 si
	 *         aucune solution n'a été trouvée
	 */
	public double distanceTo(Point3d p, double u0, double v0) {
		SurfacePoint sp = this.getNormalPointProjection(p, u0, v0);
		if (sp != null)
			return sp.distance(p);
		return -1;
	}

	public double maxLineToUVpathGap(SurfacePoint p0, SurfacePoint p1) {
		double value = maxLineToUVpathGap(p0, p0, p1, p1);
		return value;
	}

	private double maxLineToUVpathGap(Point3d p0, SurfacePoint sp0, Point3d p1, SurfacePoint sp1) {
		if (p0.distance(p1) < Util.PREC6)
			return Math.max(p0.distance(sp0), p1.distance(sp1));
		Point3d[] p = new Point3d[5];
		p[0] = p0;
		p[4] = p1;
		SurfacePoint[] sp = new SurfacePoint[5];
		sp[0] = sp0;
		sp[4] = sp1;

		for (int i = 1; i < 4; i++) {
			double k = 0.25 * i;
			p[i] = Points.add(Points.scale(1 - k, p0), Points.scale(k, p1));
			sp[i] = new SurfacePoint(this, (1 - k) * sp0.u + k * sp1.u, (1 - k) * sp0.v + k * sp1.v);
		}

		double d = 0.0;
		int iSol = 0;

		for (int i = 0; i < 5; i++) {
			double dTest = p[i].distance(sp[i]);
			if (dTest > d) {
				d = dTest;
				iSol = i;
			}
		}
		if (iSol == 0)
			return maxLineToUVpathGap(p[0], sp[0], p[1], sp[1]);
		if (iSol == 4)
			return maxLineToUVpathGap(p[3], sp[3], p[4], sp[4]);

		return maxLineToUVpathGap(p[iSol - 1], sp[iSol - 1], p[iSol + 1], sp[iSol + 1]);

	}

	/**
	 * 
	 * @return La fonction vectorielle
	 */
	public BivarVec3Function getFunction() {
		return v3f;
	}

	/**
	 * 
	 * @return la valeur mini du paramètre u
	 */
	public double getUmin() {
		return umin;
	}

	/**
	 * 
	 * @return la valeur maxi du paramètre u
	 */
	public double getUmax() {
		return umax;
	}

	/**
	 * 
	 * @return la valeur mini du paramètre v
	 */
	public double getVmin() {
		return vmin;
	}

	/**
	 * 
	 * @return la valeur maxi du paramètre v
	 */
	public double getVmax() {
		return vmax;
	}

	/**
	 * 
	 * @return la fonction vectorielle de la surface
	 */
	public BivarVec3Function getV3f() {
		return v3f;
	}

	/**
	 * 
	 * @return la fonction vectorielle de la normale à la surface
	 */
	public BivarVec3Function getNormalV3f() {
		return normalV3f;
	}

	public BivarVec3Function getdSu() {
		return dSu;
	}

	public BivarVec3Function getdSv() {
		return dSv;
	}

	public BivarVec3Function getdSuu() {
		return dSuu;
	}

	public BivarVec3Function getdSuv() {
		return dSuv;
	}

	public BivarVec3Function getdSvv() {
		return dSvv;
	}

	public S_TYPE getType() {
		return type;
	}

	/**
	 * Calculate the normal vectorial function to the surface n(u,v)
	 * 
	 * @return the normal function
	 * 
	 * @deprecated use getNormalV3f instead
	 */
	@Deprecated
	public BivarVec3Function getNormalFunction() {
		return Surface.getNormalFunctionStatic(this);
	}

	/**
	 * @deprecated use getNormalV3f instead
	 */
	@Deprecated
	private static BivarVec3Function getNormalFunctionStatic(final Surface s) {
		BivarVec3Function result = new BivarVec3Function(new BivarEvaluable() {
			public double eval(double u, double v) {
				Vector3d n = s.normal(u, v);
				return n.x;
			}
		}, new BivarEvaluable() {
			public double eval(double u, double v) {
				Vector3d n = s.normal(u, v);
				return n.y;
			}
		}, new BivarEvaluable() {
			public double eval(double u, double v) {
				Vector3d n = s.normal(u, v);
				return n.z;
			}
		});
		return result;
	}

	/**
	 * Return the closest limit of the given (u,v) point
	 * 
	 * @param u
	 * @param v
	 * 
	 * @return the closest limit to the given parameters in parametric space.
	 */
	public Limit getClosestLimit(double u, double v) {
		SurfacePoint p = new SurfacePoint(this, u, v);

		return p.getClosestLimit();
	}

	/**
	 * Return the closest limit of the given (u,v) point along the parametric
	 * direction given by the vector vec
	 * 
	 * @param u
	 * @param v
	 * @param vec
	 * 
	 * @return the closest limit along the given parameters in parametric space.
	 */
	public Limit getClosestLimitAlongDirection(double u, double v, Vector2d vec) {
		double kumin, kvmin, kumax, kvmax;
		if (abs(vec.x) > Util.PREC12) {
			kumin = (this.umin - u) / vec.x;
			kumax = (this.umax - u) / vec.x;
		} else {
			kumin = Double.MAX_VALUE;
			kumax = Double.MAX_VALUE;
		}
		if (abs(vec.y) > Util.PREC12) {
			kvmin = (this.vmin - v) / vec.y;
			kvmax = (this.vmax - v) / vec.y;
		} else {
			kvmin = Double.MAX_VALUE;
			kvmax = Double.MAX_VALUE;
		}

		Limit limitU;
		double ku;
		if (kumin < 0) {
			limitU = Limit.UMAX;
			ku = kumax;
		} else {
			limitU = Limit.UMIN;
			ku = kumin;
		}

		Limit limitV;
		double kv;
		if (kvmin < 0) {
			limitV = Limit.VMAX;
			kv = kvmax;
		} else {
			limitV = Limit.VMIN;
			kv = kvmin;
		}

		if (ku < kv)
			return limitU;
		else
			return limitV;

	}

	/**
	 * Return the chord error between two points of the surface.
	 * 
	 * Returned value is positive on a convex point and negative on a concave point.
	 * 
	 * WARNING: The complex cases with several curvature variations between the two
	 * points are not handled. Results may be deeply erroneous.
	 * 
	 * @param u1
	 * @param v1
	 * @param u2
	 * @param v2
	 * 
	 * @return the value of the chord
	 */
	public double evalChordError(double u1, double v1, double u2, double v2) {
		SurfacePoint p1 = new SurfacePoint(this, u1, v1);
		SurfacePoint p2 = new SurfacePoint(this, u2, v2);
		return evalChordError(p1, p2);
	}

	public double evalChordError(SurfacePoint p1, SurfacePoint p2) {
		double err = 0.0;
		double u1 = p1.u;
		double v1 = p1.v;
		double u2 = p2.u;
		double v2 = p2.v;

		if (p1.distance(p2) < Util.PREC12) // les deux points sont confondus
			return 0.0;

		// variable d'optimisation k, avec p =(1-k)*p1 + k*p2 et 0<k<1
		UnivarBoundedFunction chorderror = new UnivarBoundedFunction(new UnivarEvaluable() {
			@Override
			public double eval(double k) {
				if (k < Util.PREC12 || abs(k - 1) < Util.PREC12)
					return 0.0;
				Point3d p = Points.interpolate(p1, p2, k);
				SurfacePoint sp = getNormalPointProjection(p, u1 + k * (u2 - u1), v1 + k * (v2 - v1));
				if (sp != null)
					// on cherche la distance maximale => on minimise -distance
					return -p.distance(sp);
				else // projection point cannot be found
					throw new PointNotFoundException();
			}
		}, new Interval(0.0, 1.0));

		BrentOptimizer solver = new BrentOptimizer(chorderror, 0.0, 1.0);
		solver.setMaxIter(10);
		double k; // the k value of the solution
		try {
			Status status = solver.run();
			if (status == Status.SUCCESS) {
				k = solver.getVar();
				err = chorderror.eval(k);
				return -err;
			} else {
				return maxLineToUVpathGap(p1, p2);
			}
		} catch (PointNotFoundException e) {
			return maxLineToUVpathGap(p1, p2);
		}
	}

	@SuppressWarnings("unused")
	private double diffu(BivarFunction f, double u, double v) {
		if (u < umin || u > umax)
			throw new ParameterOutOfBoundsException(u);
		if (v < vmin || v > vmax)
			throw new ParameterOutOfBoundsException(v);

		if (u < umin + Util.PREC6)
			return f.diffuFw(u, v);
		else if (u > umax - Util.PREC6)
			return f.diffuBw(u, v);
		else
			return f.diffu(u, v);
	}

	@SuppressWarnings("unused")
	private double diffv(BivarFunction f, double u, double v) {
		if (u < umin || u > umax)
			throw new ParameterOutOfBoundsException(u);
		if (v < vmin || v > vmax)
			throw new ParameterOutOfBoundsException(v);

		if (v < vmin + Util.PREC6)
			return f.diffvFw(u, v);
		else if (v > vmax - Util.PREC6)
			return f.diffvBw(u, v);
		else
			return f.diffv(u, v);
	}

	/**
	 * Déplace la surface par une matrice de translation/rotation
	 * 
	 * @param m la matrice de transformation
	 */
	public void transform(Matrix4d m) {
		BivarVec3Function newFunc = this.v3f.transformClone(m);
		this.v3f = newFunc;
	}

	/**
	 * Déplace le clone en profondeur d'une surface en utilisant une matrice de
	 * translation/rotation
	 * 
	 * @param m la matrice de transformation
	 */
	public Surface transformClone(Matrix4d m) {
		Surface clone = new Surface(this);
		BivarVec3Function newV3f = clone.v3f.transformClone(m);
		clone.v3f = newV3f;
		return clone;
	}
}
