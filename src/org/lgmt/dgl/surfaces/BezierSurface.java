/*
 ** BezierSurface.java
 ** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
 **
 ** This file is part of the DGL software package (DGL stands for
 ** Differential Geometry Library). This software provides mathematic and numeric
 ** tools to work with various curves and surfaces (algebraic, Bézier,
 ** NURBS, ...) with an unified interface.
 **
 ** This software is governed by the CeCILL-C V2 license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 ** 
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-C license and that you accept its terms.
 */
package org.lgmt.dgl.surfaces;

import static java.lang.Math.abs;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.curves.BezierCurve;
import org.lgmt.dgl.functions.BivarEvaluable;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Points;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.dgl.vecmath.Vectors3;

/**
 * Une surface de Bézier
 * 
 * [uBasis] * [Mu] * [Pij] * [Mv] * [vBasis]
 * 
 * [hornerCoeffsX] [Mu] * [Pij] * [Mv] = [hornerCoeffsY] [hornerCoeffsZ]
 *
 * @navassoc - hodographU - BezierSurface
 * @navassoc - hodographV - BezierSurface
 * 
 * @author redonnet
 * 
 */
public class BezierSurface extends Surface {
	protected int npu;
	protected int npv;
	protected int dgu;
	protected int dgv;
	protected Point3d[][] points;
	protected Point3d[][] hornerCoeffs;
	protected BezierSurface hodographU = null;
	protected BezierSurface hodographV = null;
	protected BezierSurface normalVecSurf = null;

	/**
	 * Construit une nouvelle courbe de Bézier à partir d'un tableau de points de
	 * contrôle.
	 * 
	 * @param points les points de contrôle
	 */
	public BezierSurface(Point3d[][] points) {
		init(points);
	}

	/**
	 * Construit une nouvelle courbe de Bézier à partir d'une liste de liste de
	 * points de contrôle.
	 * 
	 * @param points les points de contrôle
	 */
	public BezierSurface(ArrayList<ArrayList<Point3d>> points) {
		int sizeU = points.size();
		int sizeV = points.get(0).size();

		Point3d[][] pts = new Point3d[sizeU][sizeV];

		for (int i = 0; i < sizeU; i++)
			for (int j = 0; j < sizeV; j++)
				pts[i][j] = new Point3d(points.get(i).get(j));

		init(pts);
	}

	/**
	 * Construit une nouvelle surface de Bézier, clone de la surface passée en
	 * paramètre.
	 * 
	 * NOTE : Deep copy
	 * 
	 * @param surface
	 */
	public BezierSurface(BezierSurface surface) {
		this.npu = surface.npu;
		this.npv = surface.npv;
		Point3d[][] points = new Point3d[npu][npv];
		for (int i = 0; i < npu; i++)
			for (int j = 0; j < npv; j++)
				points[i][j] = new Point3d(surface.points[i][j]);
		init(points);
	}

	private void init(Point3d[][] points) {
		this.type = S_TYPE.S_BEZIER;
		this.points = points;
		this.npu = Array.getLength(points);
		this.npv = Array.getLength(points[0]);
		this.dgu = npu - 1;
		this.dgv = npv - 1;
		this.umin = 0.0;
		this.umax = 1.0;
		this.vmin = 0.0;
		this.vmax = 1.0;
		this.hodographU = null;
		this.hodographV = null;
		this.normalVecSurf = null;

		calcHornerCoeffs();

		setV3f();
	}

	private void calcHornerCoeffs() {
		double valX;
		double valY;
		double valZ;
		double elU, elV;
		double sign;
		Point3d[][] pTmp = new Point3d[npu][npv];

		hornerCoeffs = new Point3d[npu][npv];
		for (int i = 0; i < npu; i++)
			for (int j = 0; j < npv; j++) {
				pTmp[i][j] = new Point3d(0.0, 0.0, 0.0);
			}

		for (int h = 0; h < npu; h++)
			for (int j = 0; j < npv; j++) {
				valX = 0;
				valY = 0;
				valZ = 0;
				sign = npu % 2 == 0 ? -1.0 : 1.0;
				for (int i = 0; i < npu; i++) {
					elU = i < npu - h
							? sign * Math.pow(-1, h - i) * Util.binomial(dgu, dgu - h) * Util.binomial(dgu - h, i)
							: 0;
					valX = valX + elU * points[i][j].x;
					valY = valY + elU * points[i][j].y;
					valZ = valZ + elU * points[i][j].z;
				}
				pTmp[h][j].x = valX;
				pTmp[h][j].y = valY;
				pTmp[h][j].z = valZ;
			}
		for (int h = 0; h < npu; h++)
			for (int k = 0; k < npv; k++) {
				valX = 0;
				valY = 0;
				valZ = 0;
				sign = npv % 2 == 0 ? -1.0 : 1.0;
				for (int j = 0; j < npv; j++) {
					elV = k < npv - j
							? sign * Math.pow(-1, j - k) * Util.binomial(dgv, dgv - j) * Util.binomial(dgv - j, k)
							: 0;
					valX = valX + pTmp[h][j].x * elV;
					valY = valY + pTmp[h][j].y * elV;
					valZ = valZ + pTmp[h][j].z * elV;
				}
				hornerCoeffs[h][k] = new Point3d(valX, valY, valZ);
			}
	}

	/**
	 * Calcule l'hodographe en u de la surface.
	 * 
	 * L'hodographe d'une surface est une autre surface constituée par l'ensemble
	 * des vecteurs tangents selon un paramètres donné à la surface originale
	 * ramenés sur une même origine. Pour une surface de Bézier, on définit un
	 * hodographe pour chaque paramère. L'hodographe d'une surface de Bézier est une
	 * surface de Bézier.
	 * 
	 * NOTA : Par rapport à la méthode de calcul direct des tangentes par les
	 * dérivées, le temps nécessaire pour calculer l'hodographe est largement
	 * compensé par le temps de calcul effectif des tangentes
	 */
	private void calcHodographU() {
		int npuh = this.npu - 1;

		Point3d[][] hpoints = new Point3d[npuh][npv];
		Point3d ptmp = new Point3d();
		for (int j = 0; j < npv; j++) {
			for (int i = 0; i < npuh; i++) {
				ptmp.scaleAdd(-1, points[i][j], points[i + 1][j]);
				ptmp.scale(npuh);
				hpoints[i][j] = new Point3d(ptmp);
			}
		}
		this.hodographU = new BezierSurface(hpoints);
	}

	/**
	 * Calcule l'hodographe en v de la surface.
	 * 
	 * L'hodographe d'une surface est une autre surface constituée par l'ensemble
	 * des vecteurs tangents selon un paramètres donné à la surface originale
	 * ramenés sur une même origine. Pour une surface de Bézier, on définit un
	 * hodographe pour chaque paramère. L'hodographe d'une surface de Bézier est une
	 * surface de Bézier.
	 * 
	 * NOTA : Par rapport à la méthode de calcul direct des tangentes par les
	 * dérivées, le temps nécessaire pour calculer l'hodographe est largement
	 * compensé par le temps de calcul effectif des tangentes
	 */
	private void calcHodographV() {
		int npvh = this.npv - 1;

		Point3d[][] hpoints = new Point3d[npu][npvh];
		Point3d ptmp = new Point3d();
		for (int j = 0; j < npvh; j++) {
			for (int i = 0; i < npu; i++) {
				ptmp.scaleAdd(-1, points[i][j], points[i][j + 1]);
				ptmp.scale((double) npvh);
				hpoints[i][j] = new Point3d(ptmp);
			}
		}
		this.hodographV = new BezierSurface(hpoints);
	}

	@Override
	public void tanU(Vector3d result, double u, double v) {
		if (this.hodographU == null)
			calcHodographU();

		result.set(hodographU.eval(u, v));
	}

	@Override
	public Vector3d tanU(double u, double v) {
		if (this.hodographU == null)
			calcHodographU();
		Vector3d tgu = new Vector3d();
		tgu.set(hodographU.eval(u, v));
		return tgu;
	}

	@Override
	public void tanV(Vector3d result, double u, double v) {
		if (this.hodographV == null)
			calcHodographV();

		result.set(hodographV.eval(u, v));
	}

	@Override
	public Vector3d tanV(double u, double v) {
		if (this.hodographV == null)
			calcHodographV();
		Vector3d tgv = new Vector3d();
		tgv.set(hodographV.eval(u, v));
		return tgv;
	}

	/**
	 * Calculates the normal vector surface of this surface. The normal vector
	 * surface is the equivalent of the hodograph, but for normals instead of
	 * tangents.
	 * 
	 * see "Bézier Normal Vector Surface and Its Applications", in "Proceedings of
	 * 1997 International Conference on Shape Modeling and Applications", Yamagushi,
	 * 1997
	 * 
	 * Note: some variables are capitalized to conform to the notations used in this
	 * document.
	 */
	private Point3d[][] calcNormalVecSurfPoints() {
		Point3d[][] pointsQ = new Point3d[dgu][dgv + 1];
		for (int K = 0; K < dgu; K++)
			for (int L = 0; L < dgv + 1; L++) {
				pointsQ[K][L] = new Point3d(Points.sub(points[K + 1][L], points[K][L]));
			}
		Point3d[][] pointsR = new Point3d[dgu + 1][dgv];
		for (int M = 0; M < dgu + 1; M++)
			for (int N = 0; N < dgv; N++) {
				pointsR[M][N] = new Point3d(Points.sub(points[M][N + 1], points[M][N]));
			}
		Point3d[][] pointsN = new Point3d[2 * dgu][2 * dgv];
		for (int i = 0; i < 2 * dgu; i++)
			for (int j = 0; j < 2 * dgv; j++) {
				Vector3d nIJ = new Vector3d();

				for (int K = 0; K <= dgu - 1; K++) {
					for (int M = 0; M <= dgu; M++) {
						if (K + M != i)
							continue;
						for (int L = 0; L <= dgv; L++) {
							for (int N = 0; N <= dgv - 1; N++) {
								if (L + N != j)
									continue;
								Vector3d qKL = new Vector3d(pointsQ[K][L]);
								Vector3d rMN = new Vector3d(pointsR[M][N]);
								Vector3d tmp = Vectors3.crossV(qKL, rMN);
								if (tmp.length() < Util.PREC3 || qKL.angle(rMN) < Util.PREC3
										|| abs(Math.PI - qKL.angle(rMN)) < Util.PREC3) { // May be a singular point
									Logger.getGlobal().log(Level.FINER,
											"Singular point: N[i][j] point fixed to (0,0,0)");
									pointsN[i][j] = new Point3d(0.0, 0.0, 0.0);
									continue;
								}
								tmp.scale(Util.binomial(dgv - 1, N));
								tmp.scale(Util.binomial(dgv, L));
								tmp.scale(Util.binomial(dgu, M));
								tmp.scale(Util.binomial(dgu - 1, K));
								nIJ.add(tmp);
							}
						}
					}
				}

				double tmp = (dgu * dgv) / (Util.binomial(2 * dgu - 1, i) * Util.binomial(2 * dgv - 1, j));
				nIJ.scale(tmp);
				pointsN[i][j] = new Point3d(nIJ);
			}
		return pointsN;
	}

	/**
	 * Calculates the normal vector on singular points based on the Taylor's series
	 * expansion up to the second order.
	 * 
	 * N(du,dv) = N + Nu.du + Nv.dv + 0.5(Nuu.du^2 * + 2Nuv.du.dv + Nvv.dv^2)
	 * 
	 * By construction, on a singular point the normal vector surface N(u,v) is
	 * zero. As a result, the calculation is based on the other terms only.
	 * Consequently, the result will be false for a non-singular point. Should not
	 * be used for non singular points.
	 * 
	 * The final result is the average of the valid normal vectors in the vicinity
	 * of the singular point.
	 * 
	 * @param u
	 * @param v
	 * @return
	 */
	private Vector3d singularNormal(double u, double v) {
		Point3d Nu = normalVecSurf.dSu.eval(u, v);
		Point3d Nv = normalVecSurf.dSv.eval(u, v);
		Point3d Nuu = normalVecSurf.dSuu.eval(u, v);
		Point3d Nuv = normalVecSurf.dSuv.eval(u, v);
		Point3d Nvv = normalVecSurf.dSvv.eval(u, v);

		double h = 1E-6;
		double du, dv;

		List<Vector3d> vList = new ArrayList<Vector3d>();
		for (int i = 0; i < 3; i++) {
			du = (i - 1) * h;
			for (int j = 0; j < 3; j++) {
				dv = (j - 1) * h;
				if (du == 0.0 && dv == 0.0)
					continue;
				if (isDefinedFor(u + du, v + dv)) {
					Point3d nPu = new Point3d(Nu);
					nPu.scale(du);
					Point3d nPv = new Point3d(Nv);
					nPv.scale(dv);
					Point3d nPuu = new Point3d(Nuu);
					nPuu.scale(0.5 * du * du);
					Point3d nPuv = new Point3d(Nuv);
					nPuv.scale(du * dv);
					Point3d nPvv = new Point3d(Nvv);
					nPvv.scale(0.5 * dv * dv);

					Point3d nP = Points.add(nPu, nPv);
					nP.add(nPuu);
					nP.add(nPuv);
					nP.add(nPvv);

					vList.add(new Vector3d(nP));
				}
			}
		}
		Vector3d result = new Vector3d();
		for (Vector3d vec : vList) {
			result.add(vec);
		}
		result.scale(1.0 / vList.size());
		result.normalize();
		return result;
	}

	/**
	 * Calcule la normale à la surface en un point.
	 * 
	 * @param u
	 * @param v
	 * @return un nouveau vecteur contenant le résultat
	 */
	@Override
	public Vector3d normal(double u, double v) {
		if (this.normalVecSurf == null) {
			Point3d[][] pointsN = calcNormalVecSurfPoints();
			this.normalVecSurf = new BezierSurface(pointsN);
		}
		if (abs(u - umax) < Util.PREC12)
			u = umax;
		if (abs(u - umin) < Util.PREC12)
			u = umin;
		if (abs(v - vmax) < Util.PREC12)
			v = vmax;
		if (abs(v - vmin) < Util.PREC12)
			v = vmin;
		if (u < umin || u > umax)
			throw new ParameterOutOfSurfaceBoundsException(this, u, v);
		if (v < vmin || v > vmax)
			throw new ParameterOutOfSurfaceBoundsException(this, u, v);

		Vector3d result = new Vector3d(normalVecSurf.eval(u, v));

		/*
		 * Singularity detection.
		 * 
		 * Singular points should be identified by a result vector length equal to zero.
		 * However, due to numerical approximations, this length may differ slightly
		 * from zero. The result vector commonly has a length of several thousand. A
		 * length of less than one is assumed to be zero.
		 */
		if (result.lengthSquared() < 2) {
			// System.err.println("Squared normal length = "+result.lengthSquared());
			result = singularNormal(u, v);
		}

		result.normalize();
		return result;

	}

	/**
	 * Calcule la normale à la surface en un point.
	 * 
	 * @param result un vecteur pour stocker le résultat
	 * @param u
	 * @param v
	 */
	@Override
	public void normal(Vector3d result, double u, double v) {
		result.set(normal(u, v));
	}

	private void setV3f() {
		this.v3f = functionFactory.newVec3Function(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				double val;
				double[] cu = new double[npu];
				for (int i = 0; i < npu; i++) {
					cu[i] = hornerCoeffs[i][0].x;
					for (int j = 1; j < npv; j++) {
						cu[i] = cu[i] * v + hornerCoeffs[i][j].x;
					}
				}
				val = cu[0];
				for (int i = 1; i < npu; i++) {
					val = val * u + cu[i];
				}
				return val;
			}
		}, new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				double val;
				double[] cu = new double[npu];
				for (int i = 0; i < npu; i++) {
					cu[i] = hornerCoeffs[i][0].y;
					for (int j = 1; j < npv; j++) {
						cu[i] = cu[i] * v + hornerCoeffs[i][j].y;
					}
				}
				val = cu[0];
				for (int i = 1; i < npu; i++) {
					val = val * u + cu[i];
				}
				return val;
			}

		}, new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				double val;
				double[] cu = new double[npu];
				for (int i = 0; i < npu; i++) {
					cu[i] = hornerCoeffs[i][0].z;
					for (int j = 1; j < npv; j++) {
						cu[i] = cu[i] * v + hornerCoeffs[i][j].z;
					}
				}
				val = cu[0];
				for (int i = 1; i < npu; i++) {
					val = val * u + cu[i];
				}
				return val;
			}
		});

	}

	public int getDgu() {
		return dgu;
	}

	public int getDgv() {
		return dgv;
	}

	public int getNpu() {
		return npu;
	}

	public int getNpv() {
		return npv;
	}

	public Point3d[][] getPoints() {
		return points;
	}

	public Point3d getPoint(int indexU, int indexV) {
		return points[indexU][indexV];
	}

	/**
	 * Returns a list of Bézier curves defining the 4 borders of this Bézier patch.
	 * 
	 * This list is organized as follows: umin, umax, vmin, vmax
	 * 
	 * @return the borders of this surface
	 */
	public List<BezierCurve> getBorders() {
		List<BezierCurve> list = new ArrayList<BezierCurve>();
		List<Point3d> pts;

		pts = new ArrayList<Point3d>();
		for (int i = 0; i < this.getNpu(); i++)
			pts.add(points[i][0]);
		list.add(new BezierCurve(pts));

		pts = new ArrayList<Point3d>();
		for (int j = 0; j < this.getNpv(); j++)
			pts.add(points[0][j]);
		list.add(new BezierCurve(pts));

		pts = new ArrayList<Point3d>();
		for (int i = 0; i < this.getNpu(); i++)
			pts.add(points[i][this.getNpv() - 1]);
		list.add(new BezierCurve(pts));

		pts = new ArrayList<Point3d>();
		for (int j = 0; j < this.getNpv(); j++)
			pts.add(points[this.getNpu() - 1][j]);
		list.add(new BezierCurve(pts));

		return list;
	}

	public BezierSurface getHodographU() {
		return this.hodographU;
	}

	public BezierSurface getHodographV() {
		return this.hodographV;
	}

	public BezierSurface getNormalVecSurf() {
		if (this.normalVecSurf == null) {
			Point3d[][] pointsN = calcNormalVecSurfPoints();
			this.normalVecSurf = new BezierSurface(pointsN);
		}
		return normalVecSurf;
	}

	/**
	 * Rédéfinit la surface en intervertissant les paramètres u et v et les indices
	 * des points de contrôle.
	 * 
	 * Cette opération n'a aucune influence sur le calcul des points, mais influe
	 * sur la direction de la normale.
	 */
	public void swapUV() {
		Point3d pts[][] = new Point3d[this.getNpv()][this.getNpu()];
		for (int i = 0; i < this.getNpv(); i++)
			for (int j = 0; j < this.getNpu(); j++) {
				pts[i][j] = new Point3d(this.getPoint(j, i));
			}
		this.points = pts;

		init(points);
	}

	/**
	 * Déplace la surface par une matrice de translation/rotation
	 * 
	 * @param m la matrice de transformation
	 */
	@Override
	public void transform(Matrix4d m) {
		Point3d[][] newPoints = new Point3d[npu][npv];
		for (int i = 0; i < npu; i++) {
			for (int j = 0; j < npv; j++) {
				newPoints[i][j] = new Point3d();
				m.transform(this.points[i][j], newPoints[i][j]);
			}
		}
		this.points = newPoints;

		init(points);
	}

	@Override
	public BezierSurface transformClone(Matrix4d m) {
		BezierSurface clone = new BezierSurface(this);
		clone.transform(m);
		return clone;
	}

}
