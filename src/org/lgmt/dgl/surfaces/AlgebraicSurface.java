/*
** AlgebraicSurface.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.surfaces;

import org.lgmt.dgl.functions.BivarVec3Function;
import org.lgmt.dgl.vecmath.Matrix4d;

/**
 * Une surface définie algébriquement, c'est à dire par une fonction vectorielle
 * paramétrique f(u,v).
 * 
 * @author redonnet
 * 
 */
public class AlgebraicSurface extends Surface {

	/**
	 * Contruit une nouvelle surface algébrique.
	 * 
	 * @param v3f  la fonction vectorielle f(u,v)
	 * @param umin la valeur mini pour le paramètre u
	 * @param umax la valeur maxi pour le paramètre u
	 * @param vmin la valeur mini pour le paramètre v
	 * @param vmax la valeur maxi pour le paramètre v
	 */
	public AlgebraicSurface(BivarVec3Function v3f, double umin, double umax, double vmin, double vmax) {
		this.type = S_TYPE.S_ALGEBRAIC;
		this.umin = umin;
		this.umax = umax;
		this.vmin = vmin;
		this.vmax = vmax;
		this.v3f = v3f;
	}

	/**
	 * Contruit une nouvelle surface algébrique à partir d'une fonction vectorielle
	 * et d'une matrice de transformation.
	 * 
	 * @param v3f       la fonction vectorielle f(u,v)
	 * @param umin      la valeur mini pour le paramètre u
	 * @param umax      la valeur maxi pour le paramètre v
	 * @param vmin      la valeur mini pour le paramètre u
	 * @param vmax      la valeur maxi pour le paramètre v
	 * @param transform la matrice de transformation à appliquer à la fonction
	 *                  vectorielle
	 */
	public AlgebraicSurface(BivarVec3Function v3f, double umin, double umax, double vmin, double vmax,
			Matrix4d transform) {
		this.type = S_TYPE.S_ALGEBRAIC;
		this.umin = umin;
		this.umax = umax;
		this.vmin = vmin;
		this.vmax = vmax;
		this.v3f = new BivarVec3Function(v3f, transform);
	}

	/**
	 * Construit une nouvelle surface algébrique comme la transformée d'une autre
	 * surface algébrique.
	 * 
	 * <p>
	 * Prévu pour être utilisé via
	 * {@link org.lgmt.dgl.surfaces.SurfaceFactory#newSurfaceAsTransformed(Surface surface, Matrix4d transform)}.
	 * 
	 * @param surface   la surface initiale
	 * @param transform la matrice de transformation
	 */
	public AlgebraicSurface(AlgebraicSurface surface, Matrix4d transform) {
		super(surface, transform);
	}

	/**
	 * Déplace la surface par une matrice de translation/rotation
	 * 
	 * @param m la matrice de transformation
	 */
	@Override
	public void transform(Matrix4d m) {
		BivarVec3Function newV3f = this.v3f.transformClone(m);
		this.v3f = newV3f;
	}

}
