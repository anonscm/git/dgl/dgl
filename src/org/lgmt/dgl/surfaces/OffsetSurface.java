/*
 ** OffsetSurface.java
 ** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
 **
 ** This file is part of the DGL software package (DGL stands for
 ** Differential Geometry Library). This software provides mathematic and numeric
 ** tools to work with various curves and surfaces (algebraic, Bézier,
 ** NURBS, ...) with an unified interface.
 **
 ** This software is governed by the CeCILL-C V2 license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 ** 
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-C license and that you accept its terms.
 */
package org.lgmt.dgl.surfaces;

import org.lgmt.dgl.functions.BivarEvaluable;
import org.lgmt.dgl.functions.BivarVec3Function;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;

/**
 * Une surface offset.
 * 
 * @navassoc - reference - Surface
 * 
 * @author redonnet
 *
 */
public class OffsetSurface extends Surface {
	private Surface surfaceBase;

	private double offset;

	private Cache cache = null;

	/**
	 * Construit une nouvelle surface offset à partir d'une surface existante et
	 * d'une valeur d'offset
	 * 
	 * @param surface la surface de base à utiliser
	 * @param offset  la valeur d'offset à utiliser
	 */
	public OffsetSurface(final Surface surface, final double offset) {
		this.type = S_TYPE.S_OFFSET;
		this.umin = surface.umin;
		this.umax = surface.umax;
		this.vmin = surface.vmin;
		this.vmax = surface.vmax;
		this.surfaceBase = surface;
		this.offset = offset;
		this.cache = new Cache(surface, offset);

		this.v3f = new BivarVec3Function(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Point3d point = cache.getPoint(u, v);
				return point.x;
			}
		}, new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Point3d point = cache.getPoint(u, v);
				return point.y;
			}
		}, new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Point3d point = cache.getPoint(u, v);
				return point.z;
			}
		});

	}

	/**
	 * 
	 * @return la valeur de l'offset
	 */
	public double getOffset() {
		return offset;
	}

	/**
	 * 
	 * @return la surface de base
	 */
	public Surface getSurfaceBase() {
		return surfaceBase;
	}

	/**
	 * La normale à une surface offset est égale à la normale à la surface de base.
	 */
	@Override
	public void normal(Vector3d result, double u, double v) {
		result.set(surfaceBase.normal(u, v));
	}

	/**
	 * La normale à une surface offset est égale à la normale à la surface de base.
	 */
	@Override
	public Vector3d normal(double u, double v) {
		return surfaceBase.normal(u, v);
	}

	/**
	 * Déplace la surface par une matrice de translation/rotation
	 * 
	 * @param m la matrice de transformation
	 */
	@Override
	public void transform(Matrix4d m) {
		this.v3f = functionFactory.newVec3Function(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Point3d point = cache.getPoint(u, v);
				m.transform(point);
				return point.x;
			}
		}, new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Point3d point = cache.getPoint(u, v);
				m.transform(point);
				return point.y;
			}
		}, new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Point3d point = cache.getPoint(u, v);
				m.transform(point);
				return point.z;
			}
		});
	}

	private class Cache {
		private double u;
		private double v;
		private Surface s = null;
		private Double offset = null;

		private Vector3d n = new Vector3d();
		private Point3d point = new Point3d();

		private Cache(Surface s, double offset) {
			super();
			this.s = s;
			this.offset = offset;

			/* initialisation pour éviter une incohérence fortuite lors du premier calcul */
			this.u = s.umin;
			this.v = s.vmin;
			s.eval(point, u, v);
			s.normal(n, u, v);
			n.scale(offset);
			point.add(n);
		}

		public Point3d getPoint(double u, double v) {
			if (u == this.u && v == this.v)
				return point;
			this.u = u;
			this.v = v;
			s.eval(point, u, v);
			s.normal(n, u, v);
			n.scale(offset);
			point.add(n);

			return point;
		}
	}
}
