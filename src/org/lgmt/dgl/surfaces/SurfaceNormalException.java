package org.lgmt.dgl.surfaces;

/**
 * Une exception levée quand on rencontre un problème de normale à une surface.
 * 
 * Utile en particulier quand on n'arrive pas à calculer la normale à une
 * surface en un point donné (points singuliers exotiques, etc)
 * 
 * @author redonnet
 *
 */
public class SurfaceNormalException extends RuntimeException {
	private static final long serialVersionUID = 1592508850034829002L;

	public Surface s;
	public double u;
	public double v;
	public String m;

	/**
	 * Constructeur par défaut.
	 *
	 */
	public SurfaceNormalException() {
		super();
	}

	/**
	 * Constructeur personnalisé.
	 * 
	 * @param s la Surface
	 * @param u
	 * @param v
	 */
	public SurfaceNormalException(Surface s, double u, double v) {
		super(new String("Cannot compute normal in point (" + u + "," + v + ")"));

		this.s = s;
		this.u = u;
		this.v = v;
	}

	/**
	 * Constructeur personnalisé.
	 * 
	 * @param s la surface
	 * @param u
	 * @param v
	 * @param m a message
	 */
	public SurfaceNormalException(Surface s, double u, double v, String m) {
		super(new String("Cannot compute normal in point (" + u + "," + v + ")"));

		this.s = s;
		this.u = u;
		this.v = v;
		this.m = m;
	}

}
