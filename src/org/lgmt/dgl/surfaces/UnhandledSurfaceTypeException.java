package org.lgmt.dgl.surfaces;

/**
 * Une exception levée quand on rencontre une surface d'un type non géré.
 * 
 * Utile en particulier pour signaler les surfaces B-Splines dont les séquences
 * nodales n'ont pas une multiplicité égale au degré + 1 à leurs extrémités.
 * 
 * @author redonnet
 *
 */
public class UnhandledSurfaceTypeException extends RuntimeException {
	private static final long serialVersionUID = 6732284549461422238L;

	public Surface s;
	public String m;

	/**
	 * Constructeur par défaut.
	 *
	 */
	public UnhandledSurfaceTypeException() {
		super();
	}

	/**
	 * Constructeur personnalisé.
	 * 
	 * @param s
	 *            la Surface
	 */
	public UnhandledSurfaceTypeException(Surface s) {
		super(new String("Unhandled surface type."));

		this.s = s;
	}

	/**
	 * Constructeur personnalisé.
	 * 
	 * @param s
	 *            la surface
	 * @param m
	 *            a message
	 */
	public UnhandledSurfaceTypeException(Surface s, String m) {
		super(new String("Unhandled surface type."));

		this.s = s;
		this.m = m;
	}

}
