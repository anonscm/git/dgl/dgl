package org.lgmt.dgl.surfaces;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import org.lgmt.dgl.commons.Index2;
import org.lgmt.dgl.commons.Interval;
import org.lgmt.dgl.commons.Interval2;
import org.lgmt.dgl.functions.BivarEvaluable;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.MismatchedSizeException;

/**
 * Surface définie par morceaux.
 * 
 * Définie sur l'espace paramétrique [0,1]x[0,1]
 * 
 * Les index des maps sont définis en u, puis en v en commençant à 0. Par
 * exemple le carreau (1,2) est le 2èmme carreau en u sur la 3ème rangée de
 * carreaux en v.
 * 
 * ATTENTION : Aucune vérification n'est faite sur la continuité de la surface,
 * ni dans l'espace 3D, ni dans l'espace paramétrique.
 * 
 * ATTENTION : L'utilisation de mappings trop hétérogènes peut conduire à des
 * comportements inattendus, notamment lors du calcul de dérivées
 * 
 * @author redonnet
 *
 */
public class PiecewiseSurface extends Surface {
	private List<Double> uLimits;
	private List<Double> vLimits;

	private int nu; // nombre de carreaux en u
	private int nv; // nombre de carreaux en v

	private Interval2[][] iTab; // tableau des intervalles
	private Surface[][] sTab; // tableau des surfaces

	/**
	 * 
	 * @param ulimits
	 *            les limites en u (en ordre croissant), en incluant les bornes
	 *            0.0 et 1.0
	 * @param vlimits
	 *            les limites en v (en ordre croissant), en incluant les bornes
	 *            0.0 et 1.0
	 * @param surfaces
	 *            la liste des carreaux composant la surface
	 */
	public PiecewiseSurface(double[] ulimits, double[] vlimits, List<Surface> surfaces) {
		this.umin = 0.0;
		this.umax = 1.0;
		this.vmin = 0.0;
		this.vmax = 1.0;

		this.nu = Array.getLength(ulimits) - 1; // nb de carreaux en u
		this.nv = Array.getLength(vlimits) - 1; // nb de carreaux en v

		if (surfaces.size() != nu * nv)
			throw new MismatchedSizeException();

		uLimits = new ArrayList<Double>();
		for (double u : ulimits)
			if (u < 0.0 || u > 1.0)
				throw new ParameterOutOfSurfaceBoundsException(this, u, 0.0);
			else
				uLimits.add(u);
		vLimits = new ArrayList<Double>();
		for (double v : vlimits)
			if (v < 0.0 || v > 1.0)
				throw new ParameterOutOfSurfaceBoundsException(this, 0.0, v);
			else
				vLimits.add(v);
		// TODO : check params order

		iTab = new Interval2[nu][nv];
		for (int i = 0; i < nu; i++) {
			for (int j = 0; j < nv; j++) {
				iTab[i][j] = new Interval2(new Interval(uLimits.get(i), uLimits.get(i + 1)),
						new Interval(vLimits.get(j), vLimits.get(j + 1)));
			}
		}

		sTab = new Surface[nu][nv];

		for (int k = 0; k < surfaces.size(); k++) {
			int i; // indice de la surface en u
			int j; // indice de la surface en v
			i = Math.floorDiv(k, nv);
			j = Math.floorMod(k, nv);

			sTab[i][j] = surfaces.get(k);
		}

		this.v3f = functionFactory.newVec3Function(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Index2 idx = getIndex2(u, v);
				Interval2 it = iTab[idx.i][idx.j];
				Surface s = sTab[idx.i][idx.j];
				double ul = (u - it.getLeftValue1()) / (it.getRightValue1() - it.getLeftValue1());
				double vl = (v - it.getLeftValue2()) / (it.getRightValue2() - it.getLeftValue2());
				return s.v3f.getFx().eval(ul, vl);
			}
		}, new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Index2 idx = getIndex2(u, v);
				Interval2 it = iTab[idx.i][idx.j];
				Surface s = sTab[idx.i][idx.j];
				double ul = (u - it.getLeftValue1()) / (it.getRightValue1() - it.getLeftValue1());
				double vl = (v - it.getLeftValue2()) / (it.getRightValue2() - it.getLeftValue2());
				return s.v3f.getFy().eval(ul, vl);
			}
		}, new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Index2 idx = getIndex2(u, v);
				Interval2 it = iTab[idx.i][idx.j];
				Surface s = sTab[idx.i][idx.j];
				double ul = (u - it.getLeftValue1()) / (it.getRightValue1() - it.getLeftValue1());
				double vl = (v - it.getLeftValue2()) / (it.getRightValue2() - it.getLeftValue2());
				return s.v3f.getFz().eval(ul, vl);
			}
		});

	}

	private Index2 getIndex2(double u, double v) {
		for (int i = 0; i < nu; i++)
			for (int j = 0; j < nv; j++)
				if (iTab[i][j].includes(u, v))
					return new Index2(i, j);
		return null;
	}
	
	public Surface getUnderlyingSurface(int i, int j){
		return sTab[i][j];
	}

	@Override
	public void transform(Matrix4d transform) {
		System.err.println("Pas encore implémenté !");
	}
}
