/*
 ** SurfacePoint.java
 ** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
 **
 ** This file is part of the DGL software package (DGL stands for
 ** Differential Geometry Library). This software provides mathematic and numeric
 ** tools to work with various curves and surfaces (algebraic, Bézier,
 ** NURBS, ...) with an unified interface.
 **
 ** This software is governed by the CeCILL-C V2 license under French law
 ** and abiding by the rules of distribution of free software. You can
 ** use, modify and/or redistribute the software under the terms of the
 ** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
 ** URL: "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to copy,
 ** modify and redistribute granted by the license, users are provided
 ** only with a limited warranty and the software's author, the holder of
 ** the economic rights, and the successive licensors have only limited
 ** liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks associated
 ** with loading, using, modifying and/or developing or reproducing the
 ** software by the user in light of its specific status of free software,
 ** that may mean that it is complicated to manipulate, and that also
 ** therefore means that it is reserved for developers and experienced
 ** professionals having in-depth computer knowledge. Users are therefore
 ** encouraged to load and test the software's suitability as regards
 ** their requirements in conditions enabling the security of their
 ** systems and/or data to be ensured and, more generally, to use and
 ** operate it in the same conditions as regards security.
 ** 
 ** The fact that you are presently reading this means that you have had
 ** knowledge of the CeCILL-C license and that you accept its terms.
 */
package org.lgmt.dgl.surfaces;

import org.lgmt.dgl.commons.Frame;
import org.lgmt.dgl.commons.FrameSet;
import org.lgmt.dgl.commons.Transformable;
import org.lgmt.dgl.commons.Util;
import org.lgmt.dgl.surfaces.Surface.Limit;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point2d;
import org.lgmt.dgl.vecmath.Point3d;

/**
 * Un point appartenant à une surface
 * 
 * Pour simplifier l'utilisation les valeurs x,y,z,u et v sont en public.
 * 
 * @navassoc - - - Surface
 * 
 * @author redonnet
 * 
 */
public class SurfacePoint extends Point3d implements Cloneable, Transformable<SurfacePoint> {
	private static final long serialVersionUID = 6066373347383281151L;
	private static double epsilon = Util.PREC6;

	private Surface surface = null;
	public double u;
	public double v;

	public SurfacePoint() {
		super();
		this.u = 0.0;
		this.v = 0.0;
	}

	public SurfacePoint(double x, double y, double z, double u, double v) {
		super(x, y, z);
		this.u = u;
		this.v = v;
	}

	public SurfacePoint(Point3d p, double u, double v) {
		super(p.x, p.y, p.z);
		this.u = u;
		this.v = v;
	}

	public SurfacePoint(Surface s, double u, double v) {
		super(s.eval(u, v));
		this.surface = s;
		this.u = u;
		this.v = v;
	}

	/**
	 * Construit un nouveau point sur une surface, clone du point passé en paramètre
	 * 
	 * @param p
	 */
	public SurfacePoint(SurfacePoint p) {
		super(p.x, p.y, p.z);
		this.surface = p.surface;
		u = p.u;
		v = p.v;
	}

	public Point3d getPoint() {
		return new Point3d(x, y, z);
	}

	public Point2d getParam() {
		return new Point2d(u, v);
	}

	/**
	 * Useful to quickly build comparators
	 * 
	 * @return u
	 */
	public double getU() {
		return u;
	}

	/**
	 * Useful to quickly build comparators
	 * 
	 * @return v
	 */
	public double getV() {
		return v;
	}

	public Surface getSurface() {
		return surface;
	}

	/**
	 * Define the surface of <b>this</b>.
	 * 
	 * WARNING: No check is performed. 3D point (x,y,z) may or may not belong to
	 * <b>surface</b> and 2D parameters (u,v) may or may not be part of
	 * <b>surface</b> parametric space. Use with caution.
	 * 
	 * @param surface
	 */
	public void setSurface(Surface surface) {
		this.surface = surface;
	}

	public void set(SurfacePoint p) {
		surface = p.surface;
		x = p.x;
		y = p.y;
		z = p.z;
		u = p.u;
		v = p.v;
	}

	public void set(Surface s, double u, double v) {
		surface = s;
		setPoint(surface.eval(u, v));
		this.u = u;
		this.v = v;
	}

	private void setPoint(Point3d p) {
		x = p.x;
		y = p.y;
		z = p.z;
	}

	protected static void setEpsilon(double epsilon) {
		SurfacePoint.epsilon = epsilon;
	}

	public boolean equals(SurfacePoint p) {
		if (p.getPoint().epsilonEquals(this.getPoint(), epsilon)
				&& p.getParam().epsilonEquals(this.getParam(), epsilon))
			return true;
		return false;
	}

	/**
	 * Teste si le couple de paramètre (u,v) est sur les limites de la surface.
	 * 
	 * @return true si u et/ou v sont sur les limites paramétriques de la surface (à
	 *         10E-12 près)
	 */
	public boolean isOnBounds() {
		if (Math.abs(u - surface.umin) < Util.PREC12)
			return true;
		if (Math.abs(u - surface.umax) < Util.PREC12)
			return true;
		if (Math.abs(v - surface.vmin) < Util.PREC12)
			return true;
		if (Math.abs(v - surface.vmax) < Util.PREC12)
			return true;

		return false;
	}

	@Override
	public String toString() {
		return new String("(" + String.format(Util.format, this.x) + ", " + String.format(Util.format, this.y) + ", "
				+ String.format(Util.format, this.z) + "), (" + String.format(Util.format, this.u) + ", "
				+ String.format(Util.format, this.v) + ")");
	}

	/**
	 * Exprime this dans un repère donné
	 * 
	 * @param fs         le système de repère à utilisé
	 * @param initFrame  le repère où sont exprimées les coordonnées de this
	 * @param finalFrame le repère dans lequel on veut exprimer les coordonnées de
	 *                   this
	 * @return un Point6d nouvelle créé qui contient les coordonnées de this dans
	 *         finalFrame
	 */
	public SurfacePoint transform(FrameSet fs, Frame initFrame, Frame finalFrame) {
		SurfacePoint result = new SurfacePoint();

		result.setPoint(fs.transform(this.getPoint(), initFrame, finalFrame));
		result.u = this.u;
		result.v = this.v;

		return result;
	}

	/**
	 * Exprime un Point6d dans un repère donné
	 * 
	 * Le résultat est stocké dans result qui doit être préalablement créé
	 * 
	 * @param fs         le système de repère à utilisé
	 * @param initFrame  le repère où sont exprimées les coordonnées de this
	 * @param finalFrame le repère dans lequel on veut exprimer les coordonnées de
	 *                   this
	 * @param result     un Point6d pour stocker le résultat
	 */

	public void transform(FrameSet fs, Frame initFrame, Frame finalFrame, SurfacePoint result) {
		result.setPoint(fs.transform(this.getPoint(), initFrame, finalFrame));
		result.u = this.u;
		result.v = this.v;
	}

	/**
	 * 
	 * @return la limite de la surface (umin, umax, vmin ou vmax) la plus proche de
	 *         this
	 */

	public Surface.Limit getClosestLimit() {
		double tmp;
		double delta = Math.abs(surface.getUmin() - u);
		Limit l = Limit.UMIN;
		tmp = Math.abs(surface.getUmax() - u);
		if (tmp < delta) {
			delta = tmp;
			l = Limit.UMAX;
		}
		tmp = Math.abs(surface.getVmin() - v);
		if (tmp < delta) {
			delta = tmp;
			l = Limit.VMIN;
		}
		tmp = Math.abs(surface.getVmax() - v);
		if (tmp < delta) {
			delta = tmp;
			l = Limit.VMAX;
		}

		return l;
	}

	/**
	 * 
	 * @return les limites de la surface (umin, umax, vmin et vmax) ordonnées dans
	 *         un tableau allant de la plus proche de this à la plus éloignée
	 */
	public Limit[] getClosestLimitsOrdered() {
		Limit[] tab = { Limit.UMIN, Limit.UMAX, Limit.VMIN, Limit.VMAX };

		int inner, outer;

		for (outer = 0; outer < 4; outer++) {
			Limit temp = tab[outer];
			inner = outer;
			while (inner > 0 && getDistanceToLimit(tab[inner - 1]) >= getDistanceToLimit(temp)) {
				tab[inner] = tab[inner - 1];
				inner--;
			}
			tab[inner] = temp;
		}

		return tab;
	}

	/**
	 * 
	 * @param l une limite de surface
	 * 
	 * @return la distance de this à la limite passée en paramètre
	 */
	public double getDistanceToLimit(Limit l) {
		double d = 0;
		switch (l) {
		case UMIN:
			d = Math.abs(surface.getUmin() - u);
			break;
		case UMAX:
			d = Math.abs(surface.getUmax() - u);
			break;
		case VMIN:
			d = Math.abs(surface.getVmin() - v);
			break;
		case VMAX:
			d = Math.abs(surface.getVmax() - v);
			break;
		default:
			break;
		}
		return d;
	}

	public void transform(Matrix4d m) {
		m.transform(this);
		Surface newSurf = surface.transformClone(m);
		this.surface = newSurf;
	}

	@Override
	public SurfacePoint transformClone(Matrix4d m) {
		SurfacePoint clone = new SurfacePoint(this);
		clone.transform(m);
		return clone;
	}
}
