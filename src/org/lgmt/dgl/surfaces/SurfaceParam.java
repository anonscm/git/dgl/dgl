package org.lgmt.dgl.surfaces;

/**
 * Une enum des paramètres de la surface. Utile pour passer en arguments à
 * certaines méthodes. utilisé également par les courbes isoparamétriques.
 * 
 * @author redonnet
 *
 */
public enum SurfaceParam {
	U, V
}