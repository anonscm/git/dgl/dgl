/*
** RuledSurface.java
** Copyright 2013 Jean-Max Redonnet, Institut Clément Ader Toulouse
**
** This file is part of the DGL software package (DGL stands for
** Differential Geometry Library). This software provides mathematic and numeric
** tools to work with various curves and surfaces (algebraic, Bézier,
** NURBS, ...) with an unified interface.
**
** This software is governed by the CeCILL-C V2 license under French law
** and abiding by the rules of distribution of free software. You can
** use, modify and/or redistribute the software under the terms of the
** CeCILL-C license as circulated by CEA, CNRS and INRIA at the following
** URL: "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to copy,
** modify and redistribute granted by the license, users are provided
** only with a limited warranty and the software's author, the holder of
** the economic rights, and the successive licensors have only limited
** liability.
** 
** In this respect, the user's attention is drawn to the risks associated
** with loading, using, modifying and/or developing or reproducing the
** software by the user in light of its specific status of free software,
** that may mean that it is complicated to manipulate, and that also
** therefore means that it is reserved for developers and experienced
** professionals having in-depth computer knowledge. Users are therefore
** encouraged to load and test the software's suitability as regards
** their requirements in conditions enabling the security of their
** systems and/or data to be ensured and, more generally, to use and
** operate it in the same conditions as regards security.
** 
** The fact that you are presently reading this means that you have had
** knowledge of the CeCILL-C license and that you accept its terms.
*/
package org.lgmt.dgl.surfaces;

import org.lgmt.dgl.curves.Curve;
import org.lgmt.dgl.functions.BivarEvaluable;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;

/**
 * Une surface réglée.
 * 
 * @navassoc - - 2 Curve
 * 
 * @author redonnet
 *
 */
public class RuledSurface extends Surface {
	private Curve c0;
	private Curve c1;

	/**
	 * Construit une nouvelle surface réglée à partir de deux courbes.
	 * 
	 * <p>
	 * La nouvelle surface est définie par : (1-v)*c0(u) + v*c1(u).
	 *
	 * <p>
	 * Les deux courbes doivent être défine pour un paramètre u allant de 0 à 1
	 * 
	 * <p>
	 * TODO: Gérer les limites paramétriques des courbes
	 * 
	 * @param c0
	 * @param c1
	 */
	public RuledSurface(final Curve c0, final Curve c1) {
		this.c0 = c0;
		this.c1 = c1;
		this.umin = 0.0;
		this.umax = 1.0;
		this.vmin = 0.0;
		this.vmax = 1.0;

		setV3f();
	}

	private void setV3f() {
		this.v3f = functionFactory.newVec3Function(new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Point3d point0 = new Point3d();
				Point3d point1 = new Point3d();
				c0.eval(point0, u);
				c1.eval(point1, u);
				return (1 - v) * point0.x + v * point1.x;
			}
		}, new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Point3d point0 = new Point3d();
				Point3d point1 = new Point3d();
				c0.eval(point0, u);
				c1.eval(point1, u);
				return (1 - v) * point0.y + v * point1.y;
			}
		}, new BivarEvaluable() {
			@Override
			public double eval(double u, double v) {
				Point3d point0 = new Point3d();
				Point3d point1 = new Point3d();
				c0.eval(point0, u);
				c1.eval(point1, u);
				return (1 - v) * point0.z + v * point1.z;
			}
		});
	}

	/**
	 * Construit une nouvelle surface réglée comme la transformée d'une autre
	 * surface réglée.
	 * 
	 * <p>
	 * Prévu pour être utilisé via
	 * {@link org.lgmt.dgl.surfaces.SurfaceFactory#newSurfaceAsTransformed(Surface surface, Matrix4d transform)}.
	 * 
	 * @param surface   la surface initiale
	 * @param transform la matrice de transformation
	 */
	public RuledSurface(RuledSurface surface, Matrix4d transform) {
		super(surface, transform);
	}

	/**
	 * 
	 * @return la courbe c0
	 */
	public Curve getC0() {
		return c0;
	}

	/**
	 * 
	 * @return la courbe c1
	 */
	public Curve getC1() {
		return c1;
	}

	/**
	 * Déplace la surface par une matrice de translation/rotation
	 * 
	 * <p>
	 * En pratique cette méthode déplace les deux génératrice de la surface.
	 * 
	 * @param m la matrice de transformation
	 */
	@Override
	public void transform(Matrix4d m) {
		Curve newC0 = new Curve(c0);
		Curve newC1 = new Curve(c1);
		newC0.transform(m);
		newC1.transform(m);

		setV3f();
	}
	
}
