package org.lgmt.dgl.shapes;

import java.util.ArrayList;

import org.lgmt.dgl.commons.Transformable;
import org.lgmt.dgl.vecmath.Matrix3d;
import org.lgmt.dgl.vecmath.Matrix4d;
import org.lgmt.dgl.vecmath.Point3d;
import org.lgmt.dgl.vecmath.Vector3d;
import org.lgmt.dgl.vecmath.Vectors3;

public class Rectangle implements Transformable<Rectangle> {
	private Point3d position;
	private Vector3d normal;
	private Vector3d orientation; // always along the largest (length) dimension
	private double length;
	private double width;
	private Matrix4d transform;
	/**
	 * 
	 * <pre>
	 * p1 --- p2
	 * |      |   --> orientation
	 * p0 --- p3
	 * </pre>
	 * 
	 * Order define normal in counterclock
	 */
	private ArrayList<Point3d> angles;

	/**
	 * Creates a new rectangle centered at <b>position</b> witch normal is
	 * <b>normal</b> and orientation is defined by <b>orientation</b>.
	 * 
	 * Size of the rectangle is provided by <b>length</b> on the largest side and
	 * <b>width</b> on the narrower.
	 * 
	 * In case orientation and normal are not perpendicular, real orientation is
	 * computed as follows:
	 * 
	 * real_orientation = (normal x orientation) x normal
	 * 
	 * @param position
	 * @param normal
	 * @param orientation
	 */
	public Rectangle(Point3d position, Vector3d normal, Vector3d orientation, double length, double width) {
		this.position = position;
		this.normal = normal;
		// TODO: Handle case crossprod is zero
		this.orientation = Vectors3.crossV(Vectors3.crossV(normal, orientation), normal);
		if (length > width) {
			this.length = length;
			this.width = width;
		} else {
			this.width = length;
			this.length = width;
		}
		commonBuild();
	}

	public Rectangle(Rectangle r) {
		this.position = new Point3d(r.position);
		this.normal = new Vector3d(r.normal);
		this.orientation = new Vector3d(r.orientation);
		this.length = r.length;
		this.width = r.width;
		this.transform = new Matrix4d(r.transform);
		this.angles = new ArrayList<Point3d>();
		for (Point3d p : r.getAngles())
			this.angles.add(new Point3d(p));
	}

	private void commonBuild() {
		Matrix4d m = new Matrix4d();
		Matrix3d rotation = new Matrix3d();
		Vector3d eI = new Vector3d(this.orientation);
		eI.normalize();
		Vector3d eIII = new Vector3d(this.normal);
		eIII.normalize();
		Vector3d eII = Vectors3.crossV(eIII, eI);
		eII.normalize();
		rotation.setColumn(0, eI);
		rotation.setColumn(1, eII);
		rotation.setColumn(2, eIII);
		m.setRotation(rotation);
		Vector3d translation = new Vector3d(this.position);
		m.setTranslation(translation);

		this.transform = m;

		this.angles = new ArrayList<Point3d>();
		Vector3d dir1 = new Vector3d(eI);
		dir1.scale(length / 2.0);
		Vector3d dir2 = new Vector3d(eII);
		dir2.scale(width / 2.0);
		Point3d p0 = new Point3d(position);
		p0.sub(dir1);
		p0.sub(dir2);
		Point3d p1 = new Point3d(position);
		p1.add(dir1);
		p1.sub(dir2);
		Point3d p2 = new Point3d(position);
		p2.add(dir1);
		p2.add(dir2);
		Point3d p3 = new Point3d(position);
		p3.sub(dir1);
		p3.add(dir2);
		angles.add(p0);
		angles.add(p1);
		angles.add(p2);
		angles.add(p3);
	}

	/**
	 * Return the transformation matrix that set position and orientation of
	 * <b>this</b> from a same-size rectangle, centered at origin, with normal along
	 * z-axis and largest side along x-axis
	 * 
	 * @return
	 */
	public Matrix4d getTransformMatrix() {
		return transform;
	}

	public ArrayList<Point3d> getAngles() {
		return angles;
	}

	public double getLength() {
		return length;
	}

	public double getWidth() {
		return width;
	}

	public Vector3d getNormal() {
		return normal;
	}

	public Vector3d getOrientation() {
		return orientation;
	}

	@Override
	public void transform(Matrix4d m) {
		m.transform(position);
		m.transform(normal);
		m.transform(orientation);
		for (Point3d p : angles)
			m.transform(p);

		Matrix4d mat = new Matrix4d(m);
		mat.mul(this.transform);
		this.transform = mat;
	}

	@Override
	public Rectangle transformClone(Matrix4d m) {
		Rectangle clone = new Rectangle(this);
		clone.transform(m);
		return clone;
	}

}
