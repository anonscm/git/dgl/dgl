#!/bin/bash

JAVA_HOME=$(readlink -f /usr/bin/javac | sed "s:/bin/javac::")

#echo $JAVA_HOME

CLASSPATH=../lib/umlgraph-5.6.5.jar:../lib/vecmath.jar:$JAVA_HOME/lib/tools.jar

#echo $CLASSPATH

BASEDIR=../src

for dir in `find $BASEDIR -type d` ; do
    for file in `ls $dir` ; do
        if [[ $file == *.java ]] ; then
            CLASSLIST=$CLASSLIST\ $dir/$file
        fi
    done
done

echo $CLASSLIST

if [ ! -d "uml" ];then
    mkdir uml
fi

OUTPUT=./uml/classdiagram

java -classpath $CLASSPATH org.umlgraph.doclet.UmlGraph -enumerations -enumconstants -hide Exception -hide Logger -hide Factory -output $OUTPUT.dot $CLASSLIST

dot -Tpng -Elabeldistance=1.5 -o$OUTPUT.png $OUTPUT.dot
dot -Tsvg -Elabeldistance=1.5 -o$OUTPUT.svg $OUTPUT.dot
