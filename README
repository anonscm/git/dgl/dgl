Differential Geometry Library
=============================

version 0.8 alpha

Objective: Provide all the tools necessary for handling 3D curves and surfaces.

Scope: Limited to mathematical aspects. For business-oriented aspects see other 
libraries.

Description:
  - Includes the management of functions of one variable (Univar), of two  
variables (Bivar) or of more variables (Multivar)
  - Includes the management of vector functions (size 3) of one variable 
(UnivarVec3) and two variables (BivarVec3)
  - Provides a common interface for managing geometric objects of various types. 
For now curves types supported are Bézier, BSplines, algebraic, sequence of
points and isocurves lying on a surface. Surfaces types supported are Béziers,
algebraic, offsets and ruled surfaces. BSplines are under developement. NURBS
are planned.
  - Includes few tools for solving equations (NewTon-Raphson and Brent) and 
proceeding to optimizations (Nelder-Mead)

Requirements:
  - DGL uses vecmath wich was initially part of java3d (see https://java3d.java.net/).
    Java3d seems to be discontinued, so vecmath is now integrated (Nov. 9th 2017).
  - For its documentation, DGL uses umlgraph (see http://www.umlgraph.org/) which
relies upon Graphviz (http://www.graphviz.org/)

Conventions:
  - In the code u and v are used to designate the parameters while x, y and z  
are used to designate the coordinates. Many methods are built using this 
convention; eg diffux denotes the u derivative of the x coordinate (of a  
vectorial function)
